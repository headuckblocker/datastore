package com.headuck.datastore.formats.legacy;

import com.sun.jndi.toolkit.url.Uri;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Utility to import legacy format
 * Created on 1/2/16.
 */
public class ImportLegacy {
    static final int MAX_LEGACY_IMPORT_SIZE = 102400;
    static final int MAX_PHONENUM_LEN = 16;

    static public final int ROAM_TYPE_ALL = 0;
    static public final int ROAM_TYPE_NON_ROAMING = 1;
    static public final int ROAM_TYPE_ROAMING = 2;

    static public final int ERROR_NOT_FOUND = -1;
    static public final int ERROR_SECURITY = -2;
    static public final int ERROR_FILE_TOO_LARGE = -3;
    static public final int ERROR_IO = -4;
    static public final int ERROR_FILE_FORMAT = -5;
    static public final int ERROR_UNSUPPORTED_ENCODING = -6;

    static Pattern legacyRegex = Pattern.compile("(-\\d)\\t([^\\t\\x00]*)\\t([^\\t\\x00]*)$");

    final static Logger log = LoggerFactory.getLogger("ImportLegacy");

    public interface Callback {
        boolean addBlackList(String phoneNum, String name, int roamType);
        boolean addWhiteList(String phoneNum, String name, int roamType);
    }

    public static int runImportLegacy (String filename, InputStream inputStream, Callback callback) {

        BufferedReader in = null;
        InputStream is = null;
        int errCount = 0;
        int okCount = 0;

        try
        {
            if (inputStream == null) {
                File file = new File(filename);
                if (!file.exists()) {
                    log.error("File {} not found", filename);
                    return ERROR_NOT_FOUND;
                }
                // Sanity check
                if (file.length() > MAX_LEGACY_IMPORT_SIZE) {
                    log.error("file size > {}", MAX_LEGACY_IMPORT_SIZE);
                    return ERROR_FILE_TOO_LARGE;
                }
                is = new FileInputStream(file);
            } else {
                is = new BufferedInputStream(inputStream);
            }
            // Check BOM
            byte[] bom = new byte[2];
            int bytesRead = is.read(bom);
            if (bytesRead != 2) {
                return ERROR_FILE_FORMAT;
            }
            if (!((bom[0]==-1) && (bom[1]==-2))) {
                return ERROR_FILE_FORMAT;
            }
            // Read using buffered reader

            try {
                in = new BufferedReader(new InputStreamReader(is, "UTF-16LE"));
                String recordLine;

                while ((recordLine = in.readLine())!= null) {
                    // Lines are in format
                    // num \t phone
                    Matcher legacyMatcher = legacyRegex.matcher(recordLine);
                    if (!legacyMatcher.matches()) {
                        // warn error, skip line
                        errCount ++;
                        continue;
                    } else {

                        int listId = 0;
                        try {
                            listId = Integer.parseInt(legacyMatcher.group(1));
                        } catch (NumberFormatException e) {
                            errCount ++;
                            continue;
                        }
                        String phoneNum = stripInvalidChar(legacyMatcher.group(2));
                        String name = legacyMatcher.group(3).trim();
                        if (phoneNum.length() > 0) {
                            boolean ret = true;
                            switch (listId) {
                                case -1: // Black list
                                    ret = callback.addBlackList(phoneNum, name, ROAM_TYPE_ALL);
                                    break;
                                case -2: // White list
                                    ret = callback.addWhiteList(phoneNum, name, ROAM_TYPE_ALL);
                                    break;
                                case -3: // Roaming white list
                                    ret = callback.addWhiteList(phoneNum, name, ROAM_TYPE_ROAMING);
                                    break;
                                default:
                                    errCount++;
                                    continue;
                            }
                            okCount++;
                            if (!ret) {
                                System.out.println("Loading cancel requested");
                                break;
                            }
                        }
                    }
                }
                System.out.format("Error %d Success %d%n", errCount, okCount);
            } catch (UnsupportedEncodingException e) {
                log.error("Unsupported encoding");
                return ERROR_UNSUPPORTED_ENCODING;
            } finally {
                if (in != null) in.close();
            }

        } catch (SecurityException e) {
            log.error("Security error", e);
            return ERROR_SECURITY;
        } catch (FileNotFoundException e) {
            log.error("File not found exception", e);
            return ERROR_NOT_FOUND;
        } catch (IOException e) {
            log.error("IO exception", e);
            return ERROR_IO;
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    // ignore
                }
            }
        }
        return errCount;

    }

    /**
     * Adopted from PhoneNumberUtils but stricter - only allow +
     *
     * Strips separators from a phone number string.
     * @param phoneNumber phone number to strip.
     * @return phone string stripped of separators.
     */
    public static String stripInvalidChar(String phoneNumber) {
        if (phoneNumber == null) {
            return null;
        }
        int len = phoneNumber.length();
        StringBuilder ret = new StringBuilder(len);
        int strLen = 0;
        for (int i = 0; i < len; i++) {
            char c = phoneNumber.charAt(i);
            if (isValidChar(c)) {
                if (c == '+') {
                    if (strLen == 0) {
                        ret.append(c);
                        strLen ++;
                    }
                } else {
                    if (strLen >= MAX_PHONENUM_LEN) {
                        // No more char allowed, change the last char to * and break.
                        ret.deleteCharAt(strLen - 1);
                        ret.append('*');
                        break;
                    }
                    ret.append(c);
                    strLen ++;
                    if (c == '*') {
                        break;
                    }
                }
            }
        }
        return ret.toString();
    }

    public final static boolean
    isDigit(char c) {
        return (c >= '0' && c <= '9');
    }

    public final static boolean
    isValidChar(char c) {
        return (isDigit(c) || (c == '*') || (c == '+') );
    }


}
