package com.headuck.datastore.formats.vcard.textio;

import com.headuck.datastore.formats.vcard.VCard;
import com.headuck.datastore.formats.vcard.VCardParam;
import com.headuck.datastore.formats.vcard.VCardProperty;
import com.headuck.datastore.formats.vcard.codec.DecoderException;
import com.headuck.datastore.formats.vcard.codec.QuotedPrintableCodec;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.nio.charset.IllegalCharsetNameException;
import java.nio.charset.UnsupportedCharsetException;
import java.util.ArrayList;
import java.util.List;

import static com.headuck.datastore.formats.vcard.textio.StringUtils.NEWLINE;

/*
 Copyright (c) 2012-2015, Michael Angstadt
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met: 

 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution. 

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 The views and conclusions contained in the software and documentation are those
 of the authors and should not be interpreted as representing official policies, 
 either expressed or implied, of the FreeBSD Project.
 */

/**
 * Parses the components out of each line in a plain-text vCard data stream.
 * @author Michael Angstadt
 * @see <a href="http://www.imc.org/pdi/vcard-21.rtf">vCard 2.1</a>
 * @see <a href="http://tools.ietf.org/html/rfc2426">RFC 2426 (3.0)</a>
 * @see <a href="http://tools.ietf.org/html/rfc6350">RFC 6350 (4.0)</a>
 */
public class VCardRawReader implements Closeable {

    final static Logger log = LoggerFactory.getLogger("VCardRawReader");

	private final Reader reader;
	private final ClearableStringBuilder buffer = new ClearableStringBuilder();
	private final ClearableStringBuilder unfoldedLine = new ClearableStringBuilder();

	private boolean eos = false;
	private boolean caretDecodingEnabled = true;
    private Charset defaultQuotedPrintableCharset;
	private int version = VCard.VC_VER_2_1; //initialize to 2.1, since the VERSION property can exist anywhere in the file in this version
	private int prevChar = -1;
	private int propertyLineNum = 1;
	private int lineNum = 1;

	/**
	 * @param reader the reader to read from
	 */
	public VCardRawReader(Reader reader) {
		this.reader = reader;
	}

	/**
	 * Gets the line number of the line that was just read. If the line was
	 * folded, this will be the line number of the first line.
	 * @return the line number
	 */
	public int getLineNumber() {
		return propertyLineNum;
	}

	/**
	 * Gets the vCard version that the reader is currently parsing with.
	 * @return the vCard version
	 */
	public int getVersion() {
		return version;
	}

	/**
	 * Reads the next line from the input stream. Folded lines are automatically
	 * unfolded.
	 * @return the parsed components of the line
	 * @throws VCardParseException if a line cannot be parsed
	 * @throws IOException if there's a problem reading from the stream
	 */
	public VCardRawLine readLine() throws IOException {
		if (eos) {
			return null;
		}

		propertyLineNum = lineNum;
		buffer.clear();
		unfoldedLine.clear();

		/*
		 * The raw line builder.
		 */
		VCardRawLine.Builder builder = new VCardRawLine.Builder();

		/*
		 * The property's group.
		 */
		String group = null;

		/*
		 * The property's name.
		 */
		String propertyName = null;

		/*
		 * The name of the parameter we're currently inside of.
		 */
		String curParamName = null;

		/*
		 * The property's parameters.
		 */
		// List<VCardParam> parameters = null;

		/*
		 * The property encoding = quoted-printable has been encountered
		 */
		boolean isQuotedPrintable = false;

		/*
		 * The character that used to escape the current character (for
		 * parameter values).
		 */
		char escapeChar = 0;

		/*
		 * Are we currently inside a parameter value that is surrounded with
		 * double-quotes?
		 */
		boolean inQuotes = false;

		/*
		 * Are we currently inside the property value?
		 */
		boolean inValue = false;

		/*
		 * Does the line use quoted-printable encoding, and does it end all of
		 * its folded lines with a "=" character?
		 */
		boolean quotedPrintableLine = false;


        /*
         * Charset parameter if set
         */
        String Charset = null;


		/*
		 * The current character.
		 */
		char ch = 0;

		/*
		 * The previous character.
		 */
		char prevChar;

		while (true) {
			prevChar = ch;

			int read = nextChar();
			if (read < 0) {
				eos = true;
				break;
			}

			ch = (char) read;

			if (prevChar == '\r' && ch == '\n') {
				/*
				 * The newline was already processed when the "\r" character was
				 * encountered, so ignore the accompanying "\n" character.
				 */
				continue;
			}

			if (isNewline(ch)) {
				quotedPrintableLine = (inValue && prevChar == '=' && isQuotedPrintable);
				if (quotedPrintableLine) {
					/*
					 * Remove the "=" character that some vCards put at the end
					 * of quoted-printable lines that are followed by a folded
					 * line.
					 */
					buffer.chop();
					unfoldedLine.chop();
				}

				//keep track of the current line number
				lineNum++;

				continue;
			}

			if (isNewline(prevChar)) {
				if (isWhitespace(ch)) {
					/*
					 * This line is a continuation of the previous line (the
					 * line is folded).
					 */
					continue;
				}

				if (quotedPrintableLine) {
					/*
					 * The property's parameters indicate that the property
					 * value is quoted-printable. And the previous line ended
					 * with an equals sign. This means that folding whitespace
					 * may not be prepended to folded lines like it should...
					 */
				} else {
					/*
					 * We're reached the end of the property.
					 */
					this.prevChar = ch;
					break;
				}
			}

			unfoldedLine.append(ch);

			if (inValue) {
				buffer.append(ch);
				continue;
			}

			if (escapeChar != 0) {
				//this character was escaped
				if (escapeChar == '\\') {
					if (ch == '\\') {
						buffer.append(ch);
					} else if (ch == 'n' || ch == 'N') {
						//newlines appear as "\n" or "\N" (see RFC 2426 p.7)
						buffer.append(NEWLINE);
					} else if (ch == '"' && version != VCard.VC_VER_2_1) {
						//double quotes don't need to be escaped in 2.1 parameter values because they have no special meaning
						buffer.append(ch);
					} else if (ch == ';' && version == VCard.VC_VER_2_1) {
						//semi-colons can only be escaped in 2.1 parameter values (see section 2 of specs)
						//if a 3.0/4.0 param value has semi-colons, the value should be surrounded in double quotes
						buffer.append(ch);
					} else {
						//treat the escape character as a normal character because it's not a valid escape sequence
						buffer.append(escapeChar).append(ch);
					}
				} else if (escapeChar == '^') {
					if (ch == '^') {
						buffer.append(ch);
					} else if (ch == 'n') {
						buffer.append(NEWLINE);
					} else if (ch == '\'') {
						buffer.append('"');
					} else {
						//treat the escape character as a normal character because it's not a valid escape sequence
						buffer.append(escapeChar).append(ch);
					}
				}
				escapeChar = 0;
				continue;
			}

			if (ch == '\\' || (ch == '^' && version != VCard.VC_VER_2_1 && caretDecodingEnabled)) {
				//an escape character was read
				escapeChar = ch;
				continue;
			}

			if (ch == '.' && group == null && propertyName == null) {
				//set the group
				group = buffer.getAndClear();
				builder.group(group);
				continue;
			}

			if ((ch == ';' || ch == ':') && !inQuotes) {
				if (propertyName == null) {
					//property name
					propertyName = buffer.getAndClear();
					builder.name(propertyName);
				} else {
					//parameter value
					String paramValue = buffer.getAndClear();
					if (version == VCard.VC_VER_2_1) {
						//2.1 allows whitespace to surround the "=", so remove it
						paramValue = StringUtils.ltrim(paramValue);
					}
                    List<String> valueList = null;
                    if ((curParamName != null) && (VCardParam.VC_TYPE_TOKEN.equals(curParamName.toUpperCase()))) {
                        valueList = processQuotedMultivaluedTypeParams(paramValue);
                    }
                    if (valueList == null) {
                        builder.param(curParamName, paramValue);
                        if (!isQuotedPrintable) {
                            isQuotedPrintable = isQuotedPrintable(curParamName, paramValue);
                        }
                        if (Charset == null) {
                            Charset = checkCharset(curParamName, paramValue);
                        }
                    } else {
                        for (String val : valueList) {
                            builder.param(curParamName, val);
                        }
                    }
					// parameters.put(curParamName, paramValue);
					curParamName = null;
				}

				if (ch == ':') {
					//the rest of the line is the property value
					inValue = true;
				}
				continue;
			}

			if (ch == ',' && !inQuotes && version != VCard.VC_VER_2_1) {
				//multi-valued parameter
				// parameters.put(curParamName, buffer.getAndClear());
				String paramValue = buffer.getAndClear();
				builder.param(curParamName, paramValue);
				if (!isQuotedPrintable) {
					isQuotedPrintable = isQuotedPrintable(curParamName, paramValue);
				}
				continue;
			}

			if (ch == '=' && curParamName == null) {
				//parameter name
				String paramName = buffer.getAndClear();
				if (version == VCard.VC_VER_2_1) {
					//2.1 allows whitespace to surround the "=", so remove it
					paramName = StringUtils.rtrim(paramName);
				}
				curParamName = paramName;
				continue;
			}

			if (ch == '"' && version != VCard.VC_VER_2_1) {
				//2.1 doesn't use the quoting mechanism
				inQuotes = !inQuotes;
				continue;
			}

			buffer.append(ch);
		}

		if (unfoldedLine.length() == 0) {
			//input stream was empty
			return null;
		}

		if (propertyName == null) {
			throw new VCardParseException(unfoldedLine.get(), propertyLineNum, "Null property name");
		}

		String value = buffer.getAndClear();

		if (VCardProperty.VC_VERSION.equalsIgnoreCase(propertyName)) {
			int version = VCard.getVersion(value);
			if (version == -1) {
				throw new VCardParseException(unfoldedLine.get(), propertyLineNum, "Invalid VCard version: " + value);
			}
			this.version = version;
		}

        //decode property value from quoted-printable
        if (isQuotedPrintable) {
            try {
                value = decodeQuotedPrintableValue(propertyName, value, Charset);
            } catch (DecoderException e) {
                throw new VCardParseException(unfoldedLine.get(), propertyLineNum, "Error decoding quoted printable value: " + value);
            }
        }
        builder.value(value);

        return builder.build();

	}

	/**
	 * <p>
	 * Gets whether the reader will decode parameter values that use circumflex
	 * accent encoding (enabled by default). This escaping mechanism allows
	 * newlines and double quotes to be included in parameter values.
	 * </p>
	 * 
	 * <table class="simpleTable">
	 * <tr>
	 * <th>Raw Character</th>
	 * <th>Encoded Character</th>
	 * </tr>
	 * <tr>
	 * <td>{@code "}</td>
	 * <td>{@code ^'}</td>
	 * </tr>
	 * <tr>
	 * <td><i>newline</i></td>
	 * <td>{@code ^n}</td>
	 * </tr>
	 * <tr>
	 * <td>{@code ^}</td>
	 * <td>{@code ^^}</td>
	 * </tr>
	 * </table>
	 * 
	 * <p>
	 * Example:
	 * </p>
	 * 
	 * <pre>
	 * GEO;X-ADDRESS="Pittsburgh Pirates^n115 Federal St^nPitt
	 *  sburgh, PA 15212":40.446816;80.00566
	 * </pre>
	 * 
	 * @return true if circumflex accent decoding is enabled, false if not
	 * @see <a href="http://tools.ietf.org/html/rfc6868">RFC 6868</a>
	 */
	public boolean isCaretDecodingEnabled() {
		return caretDecodingEnabled;
	}

	/**
	 * <p>
	 * Sets whether the reader will decode parameter values that use circumflex
	 * accent encoding (enabled by default). This escaping mechanism allows
	 * newlines and double quotes to be included in parameter values.
	 * </p>
	 * 
	 * <table class="simpleTable">
	 * <tr>
	 * <th>Raw Character</th>
	 * <th>Encoded Character</th>
	 * </tr>
	 * <tr>
	 * <td>{@code "}</td>
	 * <td>{@code ^'}</td>
	 * </tr>
	 * <tr>
	 * <td><i>newline</i></td>
	 * <td>{@code ^n}</td>
	 * </tr>
	 * <tr>
	 * <td>{@code ^}</td>
	 * <td>{@code ^^}</td>
	 * </tr>
	 * </table>
	 * 
	 * <p>
	 * Example:
	 * </p>
	 * 
	 * <pre>
	 * GEO;X-ADDRESS="Pittsburgh Pirates^n115 Federal St^nPitt
	 *  sburgh, PA 15212":geo:40.446816,-80.00566
	 * </pre>
	 * 
	 * @param enable true to use circumflex accent decoding, false not to
	 * @see <a href="http://tools.ietf.org/html/rfc6868">RFC 6868</a>
	 */
	public void setCaretDecodingEnabled(boolean enable) {
		caretDecodingEnabled = enable;
	}

	/**
	 * Gets the character encoding of the reader.
	 * @return the character encoding or null if none is defined
	 */
	public Charset getEncoding() {
		if (reader instanceof InputStreamReader) {
			InputStreamReader isr = (InputStreamReader) reader;
			String charsetStr = isr.getEncoding();
			return (charsetStr == null) ? null : Charset.forName(charsetStr);
		}

		return null;
	}

	private int nextChar() throws IOException {
		if (prevChar >= 0) {
			/*
			 * Use the character that was left over from the previous invocation
			 * of "readLine()".
			 */
			int ch = prevChar;
			prevChar = -1;
			return ch;
		}

		return reader.read();
	}

	private boolean isNewline(char ch) {
		return ch == '\n' || ch == '\r';
	}

	private boolean isWhitespace(char ch) {
		return ch == ' ' || ch == '\t';
	}

	/**
	 * Determines if a name-value pair represents quoted-printable encoding. This must be
	 * checked in order to account for the fact that some vCards fold
	 * quoted-printed lines in a non-standard way.
	 * @param name the property's name
	 * @param value the property's value
	 * @return true if the property is quoted-printable, false if not
	 */
	private boolean isQuotedPrintable(String name, String value) {
		if (VCardParam.VC_ENCODING_TOKEN.equalsIgnoreCase(name)) {
			if ("QUOTED-PRINTABLE".equalsIgnoreCase(value)) {
				return true;
			}
		}
		return false;
	}

    /**
     * If a name-value pair represents charset, return the charset String.
     * Otherwise return null.
     * @param name the property's name
     * @param value the property's value
     * @return String of the charset if the property is charset, null if not
     */
    private String checkCharset(String name, String value) {
        if (VCardParam.VC_CHARSET_TOKEN.equalsIgnoreCase(name)) {
            return value;
        }
        return null;
    }

	/**
	 * Closes the underlying {@link Reader} object.
	 */
	public void close() throws IOException {
		reader.close();
	}

    /**
     * <p>
     * Accounts for multi-valued TYPE parameters being enclosed entirely in
     * double quotes (for example: ADR;TYPE="home,work").
     * </p>
     * <p>
     * Many examples throughout the 4.0 specs show TYPE parameters being encoded
     * in this way. This conflicts with the ABNF and is noted in the errata.
     * This method will parse these incorrectly-formatted TYPE parameters as if
     * they were multi-valued, even though, technically, they are not.
     * </p>
     * @param value the value string of quoted printable
     */
    private List<String> processQuotedMultivaluedTypeParams(String value) {

        int comma = value.indexOf(',');
        if (comma < 0) {
            return null;
        }

        int prevComma = -1;
        List<String> valueList = new ArrayList<String>();
        while (true) {
            valueList.add(value.substring(prevComma + 1, comma));

            prevComma = comma;
            comma = value.indexOf(',', prevComma + 1);
            if (comma < 0) {
                valueList.add(value.substring(prevComma + 1));
                break;
            }
        }
        return valueList;
    }

    public void setDefaultQuotedPrintableCharset(Charset defaultQuotedPrintableCharset) {
        this.defaultQuotedPrintableCharset = defaultQuotedPrintableCharset;
    }

    /**
     * Checks to see if a property's value is encoded in quoted-printable
     * encoding and decodes it if it is.
     * @param name the property name
     * @param value the property value
     * @param charsetStr charset string from the parameters
     * @return the decoded property value or the untouched property value if it
     * is not encoded in quoted-printable encoding
     * @throws DecoderException if the value couldn't be decoded
     */
     private String decodeQuotedPrintableValue(String name, String value, String charsetStr) throws DecoderException {

        //determine the character set
        Charset charset = null;
        if (charsetStr == null) {
            charset = defaultQuotedPrintableCharset;
        } else {
            try {
                charset = Charset.forName(charsetStr);
            } catch (IllegalCharsetNameException e) {
                //bad charset name
            } catch (UnsupportedCharsetException e) {
                //bad charset name
            }

            if (charset == null) {
                charset = defaultQuotedPrintableCharset;
                //the given charset was invalid, so add a warning
                log.warn ("Invalid charset: {}", charsetStr);
            }
        }

        boolean escapeVCard = VCardProperty.isStructuredPropertyValue(name);
        QuotedPrintableCodec codec = new QuotedPrintableCodec(charset.name());
        return codec.decode(value, escapeVCard);
    }

	/**
	 * Wraps a {@link StringBuilder} object, providing utility methods for clearing
	 * it.
	 */
	private static class ClearableStringBuilder {
		private final StringBuilder sb = new StringBuilder();

		/**
		 * Clears the buffer.
		 * @return this
		 */
		public ClearableStringBuilder clear() {
			sb.setLength(0);
			return this;
		}

		/**
		 * Gets the buffer's contents.
		 * @return the buffer's contents
		 */
		public String get() {
			return sb.toString();
		}

		/**
		 * Gets the buffer's contents, then clears it.
		 * @return the buffer's contents
		 */
		public String getAndClear() {
			String string = get();
			clear();
			return string;
		}

		/**
		 * Appends a character to the buffer.
		 * @param ch the character to append
		 * @return this
		 */
		public ClearableStringBuilder append(char ch) {
			sb.append(ch);
			return this;
		}

		/**
		 * Appends a character sequence to the buffer.
		 * @param string the character sequence to append
		 * @return this
		 */
		public ClearableStringBuilder append(CharSequence string) {
			sb.append(string);
			return this;
		}

		/**
		 * Appends a character array to the buffer.
		 * @param buffer the characters to append
		 * @param start the index of the first char to append
		 * @param length the number of chars to append
		 * @return this
		 */
		public ClearableStringBuilder append(char[] buffer, int start, int length) {
			sb.append(buffer, start, length);
			return this;
		}

		/**
		 * Removes the last character from the buffer.
		 * @return this
		 */
		public ClearableStringBuilder chop() {
			sb.setLength(sb.length() - 1);
			return this;
		}

		/**
		 * Gets the length of the buffer.
		 * @return the buffer's length
		 */
		public int length() {
			return sb.length();
		}
	}
}
