package com.headuck.datastore.formats.vcard;

import com.headuck.datastore.formats.vcard.textio.CharacterBitSet;
import com.headuck.datastore.formats.vcard.textio.StringUtils;
import com.headuck.datastore.formats.vcard.textio.VCardRawWriter;
import com.headuck.datastore.formats.vcard.textio.VCardWriteException;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/*
 * Created by headuck
 * Reference made to libvcard library
 * https://github.com/pol51/libvcard/tree/master/libvcard
 */

/**
 * Representation of a vCard property
 */
public class VCardProperty {
    public static final String VC_FORMATTED_NAME="FN";
    public static final String VC_NAME="N";
    public static final String VC_TELEPHONE="TEL";
    public static final String VC_VERSION="VERSION";
    public static final String VC_PRODID_2="X-PRODID";  // For Vcard 2
    public static final String VC_PRODID_3="PRODID";  // For Vcard 3
    public static final String VC_HEADUCK_LISTTYPE="X-HEADUCK-LISTTYPE";

    // Generic fields
    public static final int DefaultValue = 0;

    // Name fields
    public static final int Lastname = 0;
    public static final int Firstname = 1;
    public static final int Additional = 2;
    public static final int Prefix = 3;
    public static final int Suffix = 4;

    protected String group;
    protected String name;
    protected String value;
    protected List<VCardParam> params;

    public VCardProperty() {
        group = null;
        name = null;
        value = null;
        params = new ArrayList<VCardParam> ();
    }

    public VCardProperty(String group, String name, String value, List<VCardParam> params) {
        this.group = group;
        this.name = name;
        this.value = value;
        this.params = params;
    }

    public String getGroup() {
        return group;
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }

    public List<VCardParam> getParams () {
        return params;
    }

    public boolean isValid() {
        if ((name == null) || (value == null)) {
            return false;
        }
        if (params != null) {
            for (VCardParam param : params) {
                if (!param.isValid()) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public boolean equals(Object otherObj) {
        if (!(otherObj instanceof VCardProperty)) return false;
        final VCardProperty other = (VCardProperty)otherObj;

        return (((name != null) && (other.getName() != null) && (name.equals(other.getName()))) &&
                (((value != null) && (other.getValue() != null) && (value.equals(other.getValue())) )) &&
                (((group == null) ? (other.getGroup() == null) : group.equals(other.getGroup()))) &&
                (((params == null) ? (other.getParams() == null) : paramsEqual (params, other.getParams()))) );
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        Set<VCardParam> paramsSet = new HashSet<VCardParam>();
        paramsSet.addAll(params);

        result = prime * result + ((group == null) ? 0 : group.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((params == null) ? 0 : paramsSet.hashCode());
        result = prime * result + ((value == null) ? 0 : value.hashCode());
        return result;
    }

    /**
     * List of characters which would break the syntax of the vCard if used
     * inside a property name. The list of characters permitted by the
     * specification is much more strict, but the goal here is to be as lenient
     * as possible.
     */
    private final CharacterBitSet invalidPropertyNameCharacters = new CharacterBitSet(".;:\n\r");

    /**
     * List of characters which would break the syntax of the vCard if used
     * inside a group name. The list of characters permitted by the
     * specification is much more strict, but the goal here is to be as lenient
     * as possible.
     */
    private final CharacterBitSet invalidGroupNameCharacters = invalidPropertyNameCharacters;

    private final CharacterBitSet newlineBitSet = new CharacterBitSet("\r\n");

    /**
     * Writes a property to the vCard data stream.
     * @param writer VCardRawWriter to be used
     * @throws IOException if there's a problem writing to the data stream
     */

    public void writeProperty (VCardRawWriter writer) throws IOException {
        switch (writer.getVersion()) {
            case VCard.VC_VER_2_1:
            case VCard.VC_VER_3_0:

                //validate the group name
                if (group != null) {
                    if (invalidGroupNameCharacters.containsAny(group)) {
                        throw new VCardWriteException("Parameter group name with invalid characters");
                    }
                    if (StringUtils.beginsWithWhitespace(group)) {
                        throw new VCardWriteException("Parameter group name begins with white space");
                    }
                }

                //validate the property name
                if (invalidPropertyNameCharacters.containsAny(name)) {
                    throw new VCardWriteException("Property name with invalid characters");
                }
                if (StringUtils.beginsWithWhitespace(name)) {
                    throw new VCardWriteException("Property name begins with white space");
                }

                // Sanitize property value

                String santizedValue;
                boolean useQuotedPrintable = false;

                if (value == null) {
                    santizedValue = "";
                } else if (writer.getVersion() == VCard.VC_VER_2_1 && newlineBitSet.containsAny(value)) {
                    /*
                     * 2.1 does not support the "\n" escape sequence (see "Delimiters"
                     * sub-section in section 2 of the specs) so encode the value in
                     * quoted-printable encoding if any newline characters exist.
                     */
                    useQuotedPrintable = true;
                    santizedValue = value;
                } else {
                    santizedValue = StringUtils.escapeNewlines(value);
                }

                if (!useQuotedPrintable) {
                    if ("QUOTED-PRINTABLE".equalsIgnoreCase(VCardParam.getEncoding(params))) {
                        useQuotedPrintable = true;
                    }
                }

                /*
                 * If the property value must be encoded in quoted printable
                 * encoding, determine what charset to use for the encoding.
                 */

                Charset quotedPrintableCharset = null;
                if (useQuotedPrintable) {
                    if (params == null) {
                        quotedPrintableCharset = Charset.forName("UTF-8");
                    } else {
                        quotedPrintableCharset = VCardParam.getCharset(params);
                    }
                }

                //write the group
                if (group != null) {
                    writer.getFoldedLineWriter().append(group).append('.');
                }

                //write the property name
                writer.getFoldedLineWriter().append(name);

                if (useQuotedPrintable) {
                    params = VCardParam.setEncoding(params, "QUOTED-PRINTABLE");
                    params = VCardParam.setCharset(params, quotedPrintableCharset);
                }

                if ((params!= null) && params.size()>0) {
                    VCardParam.writeParams (writer, params);
                }

                writer.getFoldedLineWriter().append(VCard.VC_ASSIGNMENT_TOKEN);
                writer.getFoldedLineWriter().append(santizedValue, useQuotedPrintable, quotedPrintableCharset);
                writer.getFoldedLineWriter().append(writer.getFoldedLineWriter().getNewline());

                break;
            default:
                break;
        }
    }

    /**
     * Sanitizes the property value for safe inclusion in a vCard.
     * @return the sanitized value
     */


    public static boolean paramsEqual(List<VCardParam> params1, List<VCardParam> params2) {
        if (params1 == params2) return true;
        // Check equality independent of order, discard duplicates
        Set<VCardParam> set1 = new HashSet<VCardParam>();
        if (params1 != null) {
            set1.addAll(params1);
        }
        Set<VCardParam> set2 = new HashSet<VCardParam>();
        if (params2 != null) {
            set2.addAll(params2);
        }
        return set1.equals(set2);
    }

    /**
     * Return if the property with value name is a structured value requiring escape of , and ; for values
     * This only process defined property in the library
     * @param name property name
     * @return true if yes
     */
    public static boolean isStructuredPropertyValue(String name) {
        if (VC_FORMATTED_NAME.equals(name) || VC_NAME.equals(name)) return true;
        return false;
    }

    public StructuredName getStructuredNames () {
        StructuredName structuredValue = new StructuredName();
        StringUtils.StructuredIterator it = StringUtils.structured(value);

        structuredValue.family = it.nextString();
        structuredValue.given = it.nextString();
        structuredValue.additional.addAll(it.nextComponent());
        structuredValue.prefixes.addAll(it.nextComponent());
        structuredValue.suffixes.addAll(it.nextComponent());
        return structuredValue;
    }

    public static class StructuredName {
        private String family;
        private String given;
        private List<String> additional;
        private List<String> prefixes;
        private List<String> suffixes;

        public StructuredName() {
            additional = new ArrayList<String>();
            prefixes = new ArrayList<String>();
            suffixes = new ArrayList<String>();
        }
    }
}

