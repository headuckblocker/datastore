package com.headuck.datastore.formats.vcard;

import com.headuck.datastore.formats.legacy.ImportLegacy;
import com.headuck.datastore.formats.vcard.textio.StringUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 *
 * Utility to import vcf format
 * Created on 1/30/16.
 */
public class ImportVcf implements HeaduckVcf {
    static final int MAX_LEGACY_IMPORT_SIZE = 409600;

    static public final int ERROR_NOT_FOUND = -1;
    static public final int ERROR_SECURITY = -2;
    static public final int ERROR_FILE_TOO_LARGE = -3;
    static public final int ERROR_IO = -4;
    static public final int ERROR_FILE_FORMAT = -5;
    static public final int ERROR_UNSUPPORTED_ENCODING = -6;

    final static Logger log = LoggerFactory.getLogger("ImportVcf");

    static public final int LOC_NONE=0;
    static public final int LOC_HOME=1;
    static public final int LOC_WORK=2;

    static public final int TYPE_NONE=0;
    static public final int TYPE_FAX=1;
    static public final int TYPE_PAGER=2;
    static public final int TYPE_CELL=3;


    public interface Callback {
        boolean addVcfList(String phoneNum, String name, int type, int loc);
        boolean addVchList(String phoneNum, String name, int listType, int roam, int ccat);
    }

    public static int runImportVCard(String filename, InputStream is, Callback callback, boolean isVch) {

        VCardReader reader = null;
        try {
            if (is == null) {
                File file = new File(filename);
                if (!file.exists()) {
                    return ERROR_NOT_FOUND;
                }
                // Sanity check
                if (file.length() > MAX_LEGACY_IMPORT_SIZE) {
                    return ERROR_FILE_TOO_LARGE;
                }

                reader = new VCardReader(file);
            } else {
                reader = new VCardReader(is);
            }

            VCard vcard;
            boolean ret;
            while ((vcard = reader.readNext()) != null) {
                ret = ProcessVCard(vcard, reader.getLastVersion(), callback, isVch);
                if (!ret) {
                    break;
                }
            }
        } catch (SecurityException e) {
            return ERROR_SECURITY;
        } catch (FileNotFoundException e) {
            return ERROR_NOT_FOUND;
        } catch (IllegalArgumentException e) {
            return ERROR_FILE_FORMAT;
        } catch (IOException e) {
            log.error("Error reading vcard", e);
            return ERROR_IO;
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    // ignored
                }
            }
        }
        return 0;
    }

    private static boolean ProcessVCard (VCard vcard, int version, Callback callback, boolean isVch) {
        // Get version
        boolean cancelled = false;
        // Get the formatted name
        VCardProperty fnameProperty = vcard.getProperty(VCardProperty.VC_FORMATTED_NAME, null, false);
        String name;
        if (fnameProperty != null) {
            name = fnameProperty.getValue();
            if (name == null) name = "";
            if (version == VCard.VC_VER_2_1) {
                // Probably from Android which requires unescape of FN
                name = StringUtils.unescape(name).trim();
            }
        } else {
            name = "";
        }

        // Get parameters from .vch file if applicable

        int listTypeInt = -1;
        int roamInt = -1;
        int ccatInt = -1;
        if (isVch) {
            // also check for
            // HeaduckVcf.VC_HEADUCK_LISTTYPE
            // HeaduckVcf.VC_HEADUCK_CCAT
            // HeaduckVcf.VC_HEADUCK_ROAM
            VCardProperty listTypeProperty = vcard.getProperty(HeaduckVcf.VC_HEADUCK_LISTTYPE, null, false);

            if (listTypeProperty != null) {
                final String listType = listTypeProperty.getValue();
                if (HeaduckVcf.VALUE_LISTTYPE_USER_BLACK.equalsIgnoreCase(listType)) {
                    listTypeInt = HeaduckVcf.LISTTYPE_USER_BLACK;
                } else if (HeaduckVcf.VALUE_LISTTYPE_USER_WHITE.equalsIgnoreCase(listType)) {
                    listTypeInt = HeaduckVcf.LISTTYPE_USER_WHITE;
                } else if (HeaduckVcf.VALUE_LISTTYPE_USER_JUNKWHITE.equalsIgnoreCase(listType)) {
                    listTypeInt = HeaduckVcf.LISTTYPE_USER_JUNKWHITE;
                } else if (HeaduckVcf.VALUE_LISTTYPE_USER_JUNKNAMEWHITE.equalsIgnoreCase(listType)) {
                    listTypeInt = HeaduckVcf.LISTTYPE_USER_JUNKNAMEWHITE;
                }
            }

            if (listTypeInt == -1) {
                // Can't process if type missing / unknown
                return true;
            }

            // Read roam property if applicable

            if ((listTypeInt == HeaduckVcf.LISTTYPE_USER_BLACK) || (listTypeInt == HeaduckVcf.LISTTYPE_USER_WHITE)) {
                VCardProperty roamProperty = vcard.getProperty(HeaduckVcf.VC_HEADUCK_ROAM, null, false);
                if (roamProperty != null) {
                    final String roam = roamProperty.getValue();
                    if (HeaduckVcf.VALUE_ROAM_TYPE_ROAMING.equals(roam)) {
                        roamInt = HeaduckVcf.ROAM_TYPE_ROAMING;
                    } else if (HeaduckVcf.VALUE_ROAM_TYPE_NON_ROAMING.equals(roam)) {
                        roamInt = HeaduckVcf.ROAM_TYPE_NON_ROAMING;
                    } else {
                        roamInt = HeaduckVcf.ROAM_TYPE_ALL;
                    }
                } else {
                    roamInt = HeaduckVcf.ROAM_TYPE_ALL;
                }
            }


            if (listTypeInt == HeaduckVcf.LISTTYPE_USER_JUNKNAMEWHITE) {
                VCardProperty ccatProperty = vcard.getProperty(HeaduckVcf.VC_HEADUCK_CCAT, null, false);
                if (ccatProperty != null) {
                    final String ccat = ccatProperty.getValue();
                    if ((ccat != null) && (ccat.length() > 0)) {
                        try {
                            ccatInt = Integer.parseInt(ccat);
                        } catch (NumberFormatException e) {
                            log.warn("Invalid ccat value : {}", ccat);
                            ccatInt = -1;
                        }
                    }
                }
            }

        }

        if (listTypeInt == HeaduckVcf.LISTTYPE_USER_JUNKNAMEWHITE) {
            boolean ret = callback.addVchList(null, name, listTypeInt, roamInt, ccatInt);
            if (!ret) {
                System.out.println("Loading cancel requested");
                cancelled = true;
            }
        } else {
            // iterate through tel, extract types
            for (VCardProperty property : vcard.getProperties()) {
                String currentName = property.getName();
                if (currentName == null || !currentName.equals(VCardProperty.VC_TELEPHONE)) {
                    continue;
                }
                int numberLoc = LOC_NONE;
                int numberType = TYPE_NONE;
                if (!isVch) {
                    List<VCardParam> params = property.getParams();
                    for (VCardParam param : params) {
                        if (param.isValid() && (param.getGroup() == VCardParam.Type)) {
                            final String value = param.getValue().toLowerCase();
                            // For conflicting values the last one wins
                            if (value.contains("fax")) {
                                numberType = TYPE_FAX;
                            } else if (value.contains("pager")) {
                                numberType = TYPE_PAGER;
                            } else if (value.equals("cell")) {
                                numberType = TYPE_CELL;
                            } else if (value.equals("home")) {
                                numberLoc = LOC_HOME;
                            } else if (value.equals("work")) {
                                numberLoc = LOC_WORK;
                            }
                        }
                    }
                }
                String phoneNum = ImportLegacy.stripInvalidChar(property.getValue());
                if ((phoneNum != null) && (phoneNum.length() > 0)) {
                    final boolean ret;
                    if (!isVch) {
                        // Import from .vcf file
                        ret = callback.addVcfList(phoneNum, name, numberType, numberLoc);
                    } else {
                        // Restore from .vch file
                        ret = callback.addVchList(phoneNum, name, listTypeInt, roamInt, ccatInt);
                    }
                    if (!ret) {
                        System.out.println("Loading cancel requested");
                        cancelled = true;
                        break;  // operation cancelled
                    }
                }
            }
        }
        return (!cancelled);
    }


}
