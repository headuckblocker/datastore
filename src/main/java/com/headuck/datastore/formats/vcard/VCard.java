package com.headuck.datastore.formats.vcard;

import com.headuck.datastore.formats.vcard.textio.VCardWriteException;

import java.util.ArrayList;
import java.util.List;

/*
 * Created by headuck
 * Reference made to libvcard library
 * https://github.com/pol51/libvcard/tree/master/libvcard
 */

/**
 * Representation of a vCard
 */
public class VCard {
    // Global constant
    static public final String VC_SEPARATOR_TOKEN=";";
    static public final String VC_ASSIGNMENT_TOKEN=":";
    static public final String VC_BEGIN_TOKEN="BEGIN:VCARD";
    static public final String VC_END_TOKEN="END:VCARD";

    // Version
    static public final int VC_VER_2_1 = 0;
    static public final int VC_VER_3_0 = 1;
    static public final int VC_VER_4_0 = 2;

    // Version strings
    static public String [] versionStrings = {"2.1", "3.0", "4.0"};


    // member
    List<VCardProperty> properties;

    public VCard() {
        properties = new ArrayList<VCardProperty>();
    }

    public VCard(List<VCardProperty> properties) {
        this.properties = properties;
    }

    /**
     * Add property to vcard
     * @param property property to add
     */
    public void addProperty(VCardProperty property) {
        this.properties.add(property);
    }

    /**
     * Return the list of properties in the VCard
     * @return list of properties
     */
    public List<VCardProperty> getProperties() {
        return properties;
    }

    /**
     * Remove the specified property from the vcard
     * @param property property to remove
     * @return true if success
     */
    public boolean removeProperty(VCardProperty property) {
        return properties.remove(property);
    }

    /**
     * Check if the vcard is valid
     * @return true if valid, false otherwise
     */
    public boolean isValid() {
        if (properties.size() == 0) return false;
        for (VCardProperty property : properties) {
            if (!property.isValid()) {
                return false;
            }
        }
        return true;
    }

    /**
     * Get the property with the specified name and parameters in the vcard
     * @param name name of property
     * @param params specified parameters to match
     * @param strict requires that the property only contains the specified parameters
     * @return property found, or null if no matches
     */
    public VCardProperty getProperty (String name, List<VCardParam> params, boolean strict) {
        for (VCardProperty property : properties) {
            String currentName = property.getName();
            if (currentName == null || !currentName.equalsIgnoreCase(name)) {
                continue;
            }

            if (strict) {
                if (VCardProperty.paramsEqual(params, property.getParams())) {
                    return property;
                }
            }

            if (params == null) return property;

            if ((property.getParams() != null) && property.getParams().containsAll(params)) {
                return property;
            }
        }
        return null;
    }

    /**
     * Checks for exact match of property in vcard
     * @param property
     * @return true if exact match found, false otherwise
     */
    public boolean contains (final VCardProperty property) {
        return this.properties.contains(property);
    }

    static String versionToString (int version) throws VCardWriteException {
        if ((version >=0) && (version < versionStrings.length)) {
            return versionStrings[version];
        } else {
            throw new VCardWriteException("Version code invalid: " + Integer.toString(version));
        }
    }

    public static int getVersion (String value) {
        for (int i = 0; i< versionStrings.length; i++) {
            if (versionStrings[i].equals(value)) {
                return i;
            }
        }
        return -1;
    }

}
