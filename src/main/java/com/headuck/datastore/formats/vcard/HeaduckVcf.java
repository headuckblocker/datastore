package com.headuck.datastore.formats.vcard;

import com.headuck.datastore.formats.legacy.ImportLegacy;

/**
 * Created by hhclaw on 8/31/16.
 */

public interface HeaduckVcf {
    int ROAM_TYPE_ALL = 0;
    int ROAM_TYPE_NON_ROAMING = ImportLegacy.ROAM_TYPE_NON_ROAMING;
    int ROAM_TYPE_ROAMING = ImportLegacy.ROAM_TYPE_ROAMING;

    // int value representing list dbs for use views routines
    int LISTTYPE_USER_BLACK = 0;
    int LISTTYPE_USER_WHITE = 1;
    int LISTTYPE_USER_JUNKWHITE = 2;
    int LISTTYPE_USER_JUNKNAMEWHITE = 3;
    int LISTTYPE_USER_JUNK = 4;
    int LISTTYPE_USER_ORG = 5;

    int DEFAULT_VCARD_VERSION = VCard.VC_VER_2_1;

    // Property
    String VC_HEADUCK_LISTTYPE = "X-HEADUCK-LISTTYPE";
    String VC_HEADUCK_ROAM = "X-HEADUCK-ROAM";
    String VC_HEADUCK_CCAT = "X-HEADUCK-CCAT";
    // Values
    // List Type Strings
    String VALUE_LISTTYPE_USER_BLACK = "BLACKLIST";
    String VALUE_LISTTYPE_USER_WHITE = "WHITELIST";
    String VALUE_LISTTYPE_USER_JUNKWHITE = "ALLOWNO";
    String VALUE_LISTTYPE_USER_JUNKNAMEWHITE = "ALLOWNAME";
    String VALUE_LISTTYPE_USER_JUNK = "TEMPJUNK";
    String VALUE_LISTTYPE_USER_ORG = "ORGLIST";

    // Roam Type Strings
    String VALUE_ROAM_TYPE_NON_ROAMING = "NONROAM";
    String VALUE_ROAM_TYPE_ROAMING = "ROAM";
}
