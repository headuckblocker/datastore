package com.headuck.datastore.formats.vcard;

/*
 * Writer of vCard contacts
 * based on and simplified from the ez-vcard library
 * https://github.com/mangstadt/ez-vcard
 * Original copy right notice below
 * Vcard 4.0 support, writing of binary data (and outlook compatibility for binary data),
 * nested vcard, etc removed
 *
 */

import com.headuck.datastore.DataStore;
import com.headuck.datastore.formats.vcard.textio.VCardRawWriter;

import java.io.File;
import java.io.FileWriter;
import java.io.Flushable;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


/*
 Copyright (c) 2012-2015, Michael Angstadt
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:

 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 The views and conclusions contained in the software and documentation are those
 of the authors and should not be interpreted as representing official policies,
 either expressed or implied, of the FreeBSD Project.
 */

/**
 * <p>
 * Writes {@link VCard} objects to a plain-text vCard data stream.
 * </p>
 * <p>
 * <b>Example:</b>
 *
 * <pre class="brush:java">
 * VCard vcard1 = ...
 * VCard vcard2 = ...
 * File file = new File("vcard.vcf");
 * VCardWriter writer = null;
 * try {
 *   writer = new VCardWriter(file, VCardVersion.V3_0);
 *   writer.write(vcard1);
 *   writer.write(vcard2);
 * } finally {
 *   if (writer != null) writer.close();
 * }
 * </pre>
 *
 * </p>
 *
 * <p>
 * <b>Changing the line folding settings:</b>
 *
 * <pre class="brush:java">
 * VCardWriter writer = new VCardWriter(...);
 *
 * //disable line folding
 * writer.getRawWriter().getFoldedLineWriter().setLineLength(null);
 *
 * //change line length
 * writer.getRawWriter().getFoldedLineWriter().setLineLength(50);
 *
 * //change folded line indent string
 * writer.getRawWriter().getFoldedLineWriter().setIndent("\t");
 *
 * //change newline character
 * writer.getRawWriter().getFoldedLineWriter().setNewline("**");
 * </pre>
 *
 * </p>
 * @author Michael Angstadt
 * @see <a href="http://www.imc.org/pdi/vcard-21.rtf">vCard 2.1</a>
 * @see <a href="http://tools.ietf.org/html/rfc2426">RFC 2426 (3.0)</a>
 * @see <a href="http://tools.ietf.org/html/rfc6350">RFC 6350 (4.0)</a>
 */
public class VCardWriter implements Flushable {
    private final VCardRawWriter writer;
    private final LinkedList<Boolean> prodIdStack = new LinkedList<Boolean>();

    /**
     * @param out the output stream to write to
     * @param targetVersion the version that the vCards should conform to (if
     * set to "4.0", vCards will be written in UTF-8 encoding)
     */
    public VCardWriter(OutputStream out, int targetVersion) {
        this( //(targetVersion == VCard.VC_VER_4_0) ? utf8Writer(out) :
             new OutputStreamWriter(out), targetVersion);
    }

    /**
     * @param file the file to write to
     * @param targetVersion the version that the vCards should conform to (if
     * set to "4.0", vCards will be written in UTF-8 encoding)
     * @throws IOException if there's a problem opening the file
     */
    public VCardWriter(File file, int targetVersion) throws IOException {
        this(file, false, targetVersion);
    }

    /**
     * @param file the file to write to
     * @param append true to append to the end of the file, false to overwrite
     * it
     * @param targetVersion the version that the vCards should conform to
     *                      supports VCard.VC_VER_2_0 / VCard.VC_VER_3_0 only.
     * @throws IOException if there's a problem opening the file
     */
    public VCardWriter(File file, boolean append, int targetVersion) throws IOException {
        this( // (targetVersion == VCard.VC_VER_4_0) ? utf8Writer(file, append) :
              new FileWriter(file, append), targetVersion);
    }

    /**
     * @param writer the writer to write to
     * @param targetVersion the version that the vCards should conform to
     */
    public VCardWriter(Writer writer, int targetVersion) {
        this.writer = new VCardRawWriter(writer, targetVersion);
    }

    /**
     * Gets the writer that this object wraps.
     * @return the raw writer
     */
    public VCardRawWriter getRawWriter() {
        return writer;
    }

    /**
     * Gets the version that the vCards should adhere to.
     * @return the vCard version
     */
    public int getTargetVersion() {
        return writer.getVersion();
    }

    /**
     * Sets the version that the vCards should adhere to.
     * @param targetVersion the vCard version
     */
    public void setTargetVersion(int targetVersion) {
        writer.setVersion(targetVersion);
    }

    /**
     * <p>
     * Gets whether the writer will apply circumflex accent encoding on
     * parameter values (disabled by default, only applies to 3.0 and 4.0
     * vCards). This escaping mechanism allows for newlines and double quotes to
     * be included in parameter values.
     * </p>
     *
     * <p>
     * Note that this encoding mechanism is defined separately from the
     * iCalendar specification and may not be supported by the vCard consumer.
     * </p>
     * @return true if circumflex accent encoding is enabled, false if not
     * @see VCardRawWriter#isCaretEncodingEnabled()
     */
    public boolean isCaretEncodingEnabled() {
        return writer.isCaretEncodingEnabled();
    }

    /**
     * <p>
     * Sets whether the writer will apply circumflex accent encoding on
     * parameter values (disabled by default, only applies to 3.0 and 4.0
     * vCards). This escaping mechanism allows for newlines and double quotes to
     * be included in parameter values.
     * </p>
     *
     * <p>
     * Note that this encoding mechanism is defined separately from the
     * iCalendar specification and may not be supported by the vCard consumer.
     * </p>
     * @param enable true to use circumflex accent encoding, false not to
     * @see VCardRawWriter#setCaretEncodingEnabled(boolean)
     */
    public void setCaretEncodingEnabled(boolean enable) {
        writer.setCaretEncodingEnabled(enable);
    }

    /**
     * Determines which properties need to be written, including a product id.
     * @param vcard the vCard to write
     * @return the properties to write
     *
     */
    private List<VCardProperty> prepare(VCard vcard) {
        return prepare(vcard, true);
    }

    /**
     * Determines which properties need to be written.
     * @param vcard the vCard to write
     * @param addProdId Generate product id in output
     * @return the properties to write
     *
     */
    private List<VCardProperty> prepare(VCard vcard, boolean addProdId) {

        int targetVersion = getTargetVersion();
        List<VCardProperty> propertiesToAdd = new ArrayList<VCardProperty>();
        for (VCardProperty property : vcard.properties) {
            propertiesToAdd.add(property);
        }

        //add a PRODID property, saying the vCard was generated by this library
        if (addProdId) {
            VCardProperty property;
            if (targetVersion == VCard.VC_VER_2_1) {
                property = new VCardProperty(null, VCardProperty.VC_PRODID_2, DataStore.NAME + " " + DataStore.VERSION, null);
            } else {
                property = new VCardProperty(null, VCardProperty.VC_PRODID_3, DataStore.NAME + " " + DataStore.VERSION, null);
            }
            propertiesToAdd.add(property);
        }

        return propertiesToAdd;
    }

    // @SuppressWarnings({ "rawtypes", "unchecked" })
    protected void write(VCard vcard) throws IOException {
        List<VCardProperty> propertiesToAdd = prepare(vcard);

        // int targetVersion = getTargetVersion();
        writeVCard(getRawWriter(), propertiesToAdd);
    }

    private static void writeVCard (VCardRawWriter writer, List<VCardProperty> propertiesToAdd) throws IOException {
        int version = writer.getVersion();

        // Begin token
        writer.getFoldedLineWriter().append(VCard.VC_BEGIN_TOKEN)
                .append(writer.getFoldedLineWriter().getNewline());

        // Version
        VCardProperty versionProperty = new VCardProperty(null, VCardProperty.VC_VERSION,
                VCard.versionToString(version), null);
        versionProperty.writeProperty(writer);

        // Other properties
        for (VCardProperty property: propertiesToAdd) {
            if (!VCardProperty.VC_VERSION.equalsIgnoreCase(property.getName())) {
                property.writeProperty(writer);
            }
        }

        // End token
        writer.getFoldedLineWriter().append(VCard.VC_END_TOKEN)
                .append(writer.getFoldedLineWriter().getNewline());
    }


    /**
     * Flushes the underlying {@link Writer} object.
     * @throws IOException if there's a problem flushing the writer
     */
    public void flush() throws IOException {
        writer.flush();
    }

    /**
     * Closes the underlying {@link Writer} object.
     * @throws IOException if there's a problem closing the writer
     */
    public void close() throws IOException {
        writer.close();
    }
}
