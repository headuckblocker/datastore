package com.headuck.datastore.formats.vcard.textio;

import java.io.Closeable;
import java.io.Flushable;
import java.io.IOException;
import java.io.Writer;


/*
 Copyright (c) 2012-2015, Michael Angstadt
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:

 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 The views and conclusions contained in the software and documentation are those
 of the authors and should not be interpreted as representing official policies,
 either expressed or implied, of the FreeBSD Project.
 */

/**
 * Writes data to an vCard data stream.
 * @author Michael Angstadt
 * @see <a href="http://www.imc.org/pdi/vcard-21.rtf">vCard 2.1</a>
 * @see <a href="http://tools.ietf.org/html/rfc2426">RFC 2426 (3.0)</a>
 * @see <a href="http://tools.ietf.org/html/rfc6350">RFC 6350 (4.0)</a>
 */
public class VCardRawWriter implements Closeable, Flushable {


    private final FoldedLineWriter writer;
    private boolean caretEncodingEnabled = false;
    private int version;

    /**
     * @param writer the writer to wrap
     * @param version the vCard version to adhere to
     */
    public VCardRawWriter(Writer writer, int version) {
        this.writer = new FoldedLineWriter(writer);
        this.version = version;
    }

    /**
     * Gets the writer object that this object wraps.
     * @return the folded line writer
     */
    public FoldedLineWriter getFoldedLineWriter() {
        return writer;
    }

    /**
     * <p>
     * Gets whether the writer will apply circumflex accent encoding on
     * parameter values (disabled by default, only applies to 3.0 and 4.0
     * vCards). This escaping mechanism allows for newlines and double quotes to
     * be included in parameter values.
     * </p>
     *
     * <p>
     * Note that this encoding mechanism is defined separately from the vCard
     * specification and may not be supported by the vCard consumer.
     * </p>
     *
     * <table class="simpleTable">
     * <tr>
     * <th>Raw Character</th>
     * <th>Encoded Character</th>
     * </tr>
     * <tr>
     * <td>{@code "}</td>
     * <td>{@code ^'}</td>
     * </tr>
     * <tr>
     * <td><i>newline</i></td>
     * <td>{@code ^n}</td>
     * </tr>
     * <tr>
     * <td>{@code ^}</td>
     * <td>{@code ^^}</td>
     * </tr>
     * </table>
     *
     * <p>
     * Example:
     * </p>
     *
     * <pre>
     * GEO;X-ADDRESS="Pittsburgh Pirates^n115 Federal St^nPittsburgh, PA 15212":40.446816;80.00566
     * </pre>
     *
     * @return true if circumflex accent encoding is enabled, false if not
     * @see <a href="http://tools.ietf.org/html/rfc6868">RFC 6868</a>
     */
    public boolean isCaretEncodingEnabled() {
        return caretEncodingEnabled;
    }

    /**
     * <p>
     * Sets whether the writer will apply circumflex accent encoding on
     * parameter values (disabled by default, only applies to 3.0 and 4.0
     * vCards). This escaping mechanism allows for newlines and double quotes to
     * be included in parameter values.
     * </p>
     *
     * <p>
     * Note that this encoding mechanism is defined separately from the vCard
     * specification and may not be supported by the vCard consumer.
     * </p>
     *
     * <table class="simpleTable">
     * <tr>
     * <th>Raw Character</th>
     * <th>Encoded Character</th>
     * </tr>
     * <tr>
     * <td>{@code "}</td>
     * <td>{@code ^'}</td>
     * </tr>
     * <tr>
     * <td><i>newline</i></td>
     * <td>{@code ^n}</td>
     * </tr>
     * <tr>
     * <td>{@code ^}</td>
     * <td>{@code ^^}</td>
     * </tr>
     * </table>
     *
     * <p>
     * Example:
     * </p>
     *
     * <pre>
     * GEO;X-ADDRESS="Pittsburgh Pirates^n115 Federal St^nPittsburgh, PA 15212":40.446816;80.00566
     * </pre>
     *
     * @param enable true to use circumflex accent encoding, false not to
     * @see <a href="http://tools.ietf.org/html/rfc6868">RFC 6868</a>
     */
    public void setCaretEncodingEnabled(boolean enable) {
        caretEncodingEnabled = enable;
    }

    /**
     * Gets the vCard version that the writer is adhering to.
     * @return the version
     */
    public int getVersion() {
        return version;
    }

    /**
     * Sets the vCard version that the writer should adhere to.
     * @param version the version
     */
    public void setVersion(int version) {
        this.version = version;
    }


    /**
     * Flushes the underlying {@link Writer} object.
     * @throws IOException if there's a problem flushing the writer
     */
    public void flush() throws IOException {
        writer.flush();
    }

    /**
     * Closes the underlying {@link Writer} object.
     * @throws IOException if there's a problem closing the writer
     */
    public void close() throws IOException {
        writer.close();
    }
}

