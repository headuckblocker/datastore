package com.headuck.datastore.formats.vcard.textio;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/*
 Copyright (c) 2012-2015, Michael Angstadt
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met: 

 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution. 

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 The views and conclusions contained in the software and documentation are those
 of the authors and should not be interpreted as representing official policies, 
 either expressed or implied, of the FreeBSD Project.
 */

/**
 * Helper class for dealing with strings.
 * @author Michael Angstadt
 */
public final class StringUtils {
	/**
	 * The local computer's newline character sequence.
	 */
	public static final String NEWLINE = System.getProperty("line.separator");

	/**
	 * Trims the whitespace off the left side of a string.
	 * @param string the string to trim
	 * @return the trimmed string
	 */
	public static String ltrim(String string) {
		if (string == null) {
			return null;
		}

		int i;
		for (i = 0; i < string.length() && Character.isWhitespace(string.charAt(i)); i++) {
			//do nothing
		}
		return (i == string.length()) ? "" : string.substring(i);
	}

	/**
	 * Trims the whitespace off the right side of a string.
	 * @param string the string to trim
	 * @return the trimmed string
	 */
	public static String rtrim(String string) {
		if (string == null) {
			return null;
		}

		int i;
		for (i = string.length() - 1; i >= 0 && Character.isWhitespace(string.charAt(i)); i--) {
			//do nothing
		}
		return (i == 0) ? "" : string.substring(0, i + 1);
	}

	/**
	 * Creates a string consisting of "count" occurrences of char "c".
	 * @param c the character to repeat
	 * @param count the number of times to repeat the character
	 * @return the resulting string
	 */
	public static String repeat(char c, int count) {
		if (count <= 0) {
			return "";
		}

		StringBuilder sb = new StringBuilder(count);
		for (int i = 0; i < count; i++) {
			sb.append(c);
		}
		return sb.toString();
	}

	/**
	 * Creates a string consisting of "count" occurrences of string "str".
	 * @param str the string to repeat
	 * @param count the number of times to repeat the string
	 * @return the resulting string
	 */
	public static String repeat(String str, int count) {
		if (count <= 0) {
			return "";
		}

		StringBuilder sb = new StringBuilder(count * str.length());
		for (int i = 0; i < count; i++) {
			sb.append(str);
		}
		return sb.toString();
	}

	/**
	 * Joins a collection of values into a delimited list.
	 * @param collection the collection of values
	 * @param delimiter the delimiter (e.g. ",")
	 * @return the final string
	 */
	public static <T> String join(Collection<T> collection, String delimiter) {
		StringBuilder sb = new StringBuilder();
		join(collection, delimiter, sb);
		return sb.toString();
	}

	/**
	 * Joins a collection of values into a delimited list.
	 * @param collection the collection of values
	 * @param delimiter the delimiter (e.g. ",")
	 * @param sb the string builder to append onto
	 */
	public static <T> void join(Collection<T> collection, String delimiter, StringBuilder sb) {
		join(collection, delimiter, sb, new JoinCallback<T>() {
			public void handle(StringBuilder sb, T value) {
				sb.append(value);
			}
		});
	}

	/**
	 * Joins a collection of values into a delimited list.
	 * @param collection the collection of values
	 * @param delimiter the delimiter (e.g. ",")
	 * @param join callback function to call on every element in the collection
	 * @return the final string
	 */
	public static <T> String join(Collection<T> collection, String delimiter, JoinCallback<T> join) {
		StringBuilder sb = new StringBuilder();
		join(collection, delimiter, sb, join);
		return sb.toString();
	}

	/**
	 * Joins a collection of values into a delimited list.
	 * @param collection the collection of values
	 * @param delimiter the delimiter (e.g. ",")
	 * @param sb the string builder to append onto
	 * @param join callback function to call on every element in the collection
	 */
	public static <T> void join(Collection<T> collection, String delimiter, StringBuilder sb, JoinCallback<T> join) {
		boolean first = true;
		for (T element : collection) {
			if (first) {
				first = false;
			} else {
				sb.append(delimiter);
			}
			join.handle(sb, element);
		}
	}

	/**
	 * Joins a map into a delimited list.
	 * @param map the map
	 * @param delimiter the delimiter (e.g. ",")
	 * @param join callback function to call on every element in the collection
	 * @return the final string
	 */
	public static <K, V> String join(Map<K, V> map, String delimiter, final JoinMapCallback<K, V> join) {
		return join(map.entrySet(), delimiter, new JoinCallback<Map.Entry<K, V>>() {
			public void handle(StringBuilder sb, Map.Entry<K, V> entry) {
				join.handle(sb, entry.getKey(), entry.getValue());
			}
		});
	}

	/**
	 * Callback interface used with various {@code StringUtils.join()} methods.
	 * @author Michael Angstadt
	 * @param <T> the value type
	 */
	public static interface JoinCallback<T> {
		void handle(StringBuilder sb, T value);
	}

	/**
	 * Callback interface used with the
	 * {@link #join(Map, String, JoinMapCallback)} method.
	 * @author Michael Angstadt
	 * @param <K> the key class
	 * @param <V> the value class
	 */
	public static interface JoinMapCallback<K, V> {
		void handle(StringBuilder sb, K key, V value);
	}

	/**
	 * Creates a copy of the given map, converting its keys and values to
	 * lowercase.
	 * @param map the map
	 * @return the copy with lowercase keys and values
	 */
	public static Map<String, String> toLowerCase(Map<String, String> map) {
		Map<String, String> lowerCaseMap = new HashMap<String, String>(map.size());
		for (Map.Entry<String, String> entry : map.entrySet()) {
			String key = entry.getKey().toLowerCase();
			String value = entry.getValue();
			value = (value == null) ? null : value.toLowerCase();

			lowerCaseMap.put(key, value);
		}
		return lowerCaseMap;
	}

	private StringUtils() {
		//hide
	}

    // The followings are originally from VCardPropertyScribe.java in the ez-vcard project

	/**
	 * Unescapes all special characters that are escaped with a backslash, as
	 * well as escaped newlines.
	 * @param text the text to unescape
	 * @return the unescaped text
	 */
	public static String unescape(String text) {
		if (text == null) {
			return null;
		}

		StringBuilder sb = null; //only instantiate the StringBuilder if the string needs to be modified
		boolean escaped = false;
		for (int i = 0; i < text.length(); i++) {
			char ch = text.charAt(i);

			if (escaped) {
				if (sb == null) {
					sb = new StringBuilder(text.length());
					sb.append(text.substring(0, i - 1));
				}

				escaped = false;

				if (ch == 'n' || ch == 'N') {
					//newlines appear as "\n" or "\N" (see RFC 5545 p.46)
					sb.append(NEWLINE);
					continue;
				}

				sb.append(ch);
				continue;
			}

			if (ch == '\\') {
				escaped = true;
				continue;
			}

			if (sb != null) {
				sb.append(ch);
			}
		}
		return (sb == null) ? text : sb.toString();
	}

	/**
	 * <p>
	 * Escapes all special characters within a vCard value. These characters
	 * are:
	 * </p>
	 * <ul>
	 * <li>backslashes ({@code \})</li>
	 * <li>commas ({@code ,})</li>
	 * <li>semi-colons ({@code ;})</li>
	 * </ul>
	 * <p>
	 * Newlines are not escaped by this method. They are escaped when the vCard
	 * is serialized (in the {@link VCardRawWriter} class).
	 * </p>
	 * @param text the text to escape
	 * @return the escaped text
	 */
	public static String escape(String text) {
		if (text == null) {
			return null;
		}

		String chars = "\\,;";
		StringBuilder sb = null; //only instantiate the StringBuilder if the string needs to be modified
		for (int i = 0; i < text.length(); i++) {
			char ch = text.charAt(i);
			if (chars.indexOf(ch) >= 0) {
				if (sb == null) {
					sb = new StringBuilder(text.length());
					sb.append(text.substring(0, i));
				}
				sb.append('\\');
			}

			if (sb != null) {
				sb.append(ch);
			}
		}
		return (sb == null) ? text : sb.toString();
	}

	/**
	 * Creates a string splitter (takes escaped characters into account).
	 * @param delimiter the delimiter character (e.g. ',')
	 * @return the splitter object
	 */
	public static Splitter splitter(char delimiter) {
		return new Splitter(delimiter);
	}

	/**
	 * A helper class for splitting strings.
	 */
	public static class Splitter {
		private char delimiter;
		private boolean unescape = false;
		private boolean nullEmpties = false;
		private int limit = -1;

		/**
		 * Creates a new splitter object.
		 * @param delimiter the delimiter character (e.g. ',')
		 */
		public Splitter(char delimiter) {
			this.delimiter = delimiter;
		}

		/**
		 * Sets whether to unescape each split string.
		 * @param unescape true to unescape, false not to (default is false)
		 * @return this
		 */
		public Splitter unescape(boolean unescape) {
			this.unescape = unescape;
			return this;
		}

		/**
		 * Sets whether to treat empty elements as null elements.
		 * @param nullEmpties true to treat them as null elements, false to
		 * treat them as empty strings (default is false)
		 * @return this
		 */
		public Splitter nullEmpties(boolean nullEmpties) {
			this.nullEmpties = nullEmpties;
			return this;
		}

		/**
		 * Sets the max number of split strings it should parse.
		 * @param limit the max number of split strings
		 * @return this
		 */
		public Splitter limit(int limit) {
			this.limit = limit;
			return this;
		}

		/**
		 * Performs the split operation.
		 * @param string the string to split (e.g. "one,two,three")
		 * @return the split string
		 */
		public List<String> split(String string) {
			//doing it this way is 10x faster than a regex

			List<String> list = new ArrayList<String>();
			boolean escaped = false;
			int start = 0;
			for (int i = 0; i < string.length(); i++) {
				char ch = string.charAt(i);

				if (escaped) {
					escaped = false;
					continue;
				}

				if (ch == delimiter) {
					add(string.substring(start, i), list);
					start = i + 1;
					if (limit > 0 && list.size() == limit - 1) {
						break;
					}

					continue;
				}

				if (ch == '\\') {
					escaped = true;
					continue;
				}
			}

			add(string.substring(start), list);

			return list;
		}

		private void add(String str, List<String> list) {
			str = str.trim();

			if (nullEmpties && str.length() == 0) {
				str = null;
			} else if (unescape) {
				str = StringUtils.unescape(str);
			}

			list.add(str);
		}
	}

	/**
	 * Parses a "list" property value. This is used in plain-text vCards to
	 * parse properties
	 * @param value the string to parse (e.g. "one,two,three\,four")
	 * @return the parsed list (e.g. ["one", "two", "three,four"])
	 */
	public static List<String> list(String value) {
		if (value.length() == 0) {
			return new ArrayList<String>(0);
		}
		return splitter(',').unescape(true).split(value);
	}

	/**
	 * Generates a "list" property value. This is used in plain-text vCards to
	 * parse properties
	 * @param values the values to write (the {@code toString()} method is
	 * invoked on each object, null objects are ignored, e.g. ["one", "two",
	 * "three,four"])
	 * @return the property value (e.g. "one,two,three\,four")
	 */
	public static String list(Object... values) {
		return list(Arrays.asList(values));
	}

	/**
	 * Generates a "list" property value. This is used in plain-text vCards to
	 * parse properties
	 * @param values the values to write (the {@code toString()} method is
	 * invoked on each object, null objects are ignored, e.g. ["one", "two",
	 * "three,four"])
	 * @return the property value (e.g. "one,two,three\,four")
	 */
	public static <T> String list(Collection<T> values) {
		return join(values, ",", new JoinCallback<T>() {
			public void handle(StringBuilder sb, T value) {
				if (value == null) {
					return;
				}
				sb.append(escape(value.toString()));
			}
		});
	}

	/**
	 * Parses a "semi-structured" property value (a "structured" property value
	 * whose items cannot be multi-valued). This is used in plain-text vCards to
	 * parse properties such as Organization.
	 * @param value the string to parse (e.g. "one;two;three\;four,five")
	 * @return the parsed values (e.g. ["one", "two", "three;four,five"]
	 */
	public static SemiStructuredIterator semistructured(String value) {
		return semistructured(value, -1);
	}

	/**
	 * Parses a "semi-structured" property value (a "structured" property value
	 * whose items cannot be multi-valued). This is used in plain-text vCards to
	 * parse properties such as Organization.
	 * @param value the string to parse (e.g. "one;two;three\;four,five")
	 * @param limit the max number of items to parse (see
	 * {@link String#split(String, int)})
	 * @return the parsed values (e.g. ["one", "two", "three;four,five"]
	 */
	public static SemiStructuredIterator semistructured(String value, int limit) {
		List<String> split = splitter(';').unescape(true).limit(limit).split(value);
		return new SemiStructuredIterator(split.iterator());
	}

	/**
	 * Parses a "structured" property value. This is used in plain-text vCards
	 * to parse properties such as StructuredName.
	 * @param value the string to parse (e.g. "one;two,three;four\,five\;six")
	 * @return an iterator for accessing the parsed values (e.g. ["one", ["two",
	 * "three"], "four,five;six"])
	 */
	public static StructuredIterator structured(String value) {
		List<String> split = splitter(';').split(value);
		List<List<String>> components = new ArrayList<List<String>>(split.size());
		for (String s : split) {
			components.add(list(s));
		}
		return new StructuredIterator(components.iterator());
	}


	/**
	 * <p>
	 * Writes a "structured" property value. This is used in plain-text vCards
	 * to marshal properties such as StructuredName.
	 * </p>
	 * <p>
	 * This method accepts a list of {@link Object} instances.
	 * {@link Collection} objects will be treated as multi-valued components.
	 * Null objects will be treated as empty components. All other objects will
	 * have their {@code toString()} method invoked to generate the string
	 * value.
	 * </p>
	 * @param values the values to write
	 * @return the structured value string
	 */
	public static String structured(Object... values) {
		return join(Arrays.asList(values), ";", new JoinCallback<Object>() {
			public void handle(StringBuilder sb, Object value) {
				if (value == null) {
					return;
				}

				if (value instanceof Collection) {
					Collection<?> list = (Collection<?>) value;
					sb.append(list(list));
					return;
				}

				sb.append(escape(value.toString()));
			}
		});
	}

	/**
	 * Iterates over the items in a "structured" property value.
	 */
	public static class StructuredIterator {
		private final Iterator<List<String>> it;

		/**
		 * Constructs a new structured iterator.
		 * @param it the iterator to wrap
		 */
		public StructuredIterator(Iterator<List<String>> it) {
			this.it = it;
		}

		/**
		 * Gets the first value of the next component.
		 * @return the first value, null if the value is an empty string, or
		 * null if there are no more components
		 */
		public String nextString() {
			if (!hasNext()) {
				return null;
			}

			List<String> list = it.next();
			if (list.isEmpty()) {
				return null;
			}

			String value = list.get(0);
			return (value.length() == 0) ? null : value;
		}

		/**
		 * Gets the next component.
		 * @return the next component, an empty list if the component is empty,
		 * or an empty list of there are no more components
		 */
		public List<String> nextComponent() {
			if (!hasNext()) {
				return new ArrayList<String>(0); //the lists should be mutable so they can be directly assigned to the property object's fields
			}

			List<String> list = it.next();
			if (list.size() == 1 && list.get(0).length() == 0) {
				return new ArrayList<String>(0);
			}

			return list;
		}

		public boolean hasNext() {
			return it.hasNext();
		}
	}

	/**
	 * Iterates over the items in a "semi-structured" property value.
	 */
	public static class SemiStructuredIterator {
		private final Iterator<String> it;

		/**
		 * Constructs a new structured iterator.
		 * @param it the iterator to wrap
		 */
		public SemiStructuredIterator(Iterator<String> it) {
			this.it = it;
		}

		/**
		 * Gets the next value.
		 * @return the next value or null if there are no more values
		 */
		public String next() {
			return hasNext() ? it.next() : null;
		}

		public boolean hasNext() {
			return it.hasNext();
		}
	}

	// Copied from VCardRawWriter in the ez-vcard package

	/**
	 * Determines if a given string starts with whitespace.
	 * @param string the string
	 * @return true if it starts with whitespace, false if not
	 */
	public static boolean beginsWithWhitespace(String string) {
		if (string.length() == 0) {
			return false;
		}
		char first = string.charAt(0);
		return (first == ' ' || first == '\t');
	}

	/**
	 * Regular expression used to detect newline character sequences.
	 */
	public static final Pattern newlineRegex = Pattern.compile("\\r\\n|\\r|\\n");

	/**
	 * Escapes all newlines in a string.
	 * @param string the string to escape
	 * @return the escaped string
	 */
	public static String escapeNewlines(String string) {
		return newlineRegex.matcher(string).replaceAll("\\\\n");
	}
}
