package com.headuck.datastore.formats.vcard;

/*
 * Reader of vCard formatted contacts
 * based on and simplified from the ez-vcard library
 * https://github.com/mangstadt/ez-vcard
 * Original copy right notice below
 *
 */
import com.headuck.datastore.formats.vcard.textio.VCardParseException;
import com.headuck.datastore.formats.vcard.textio.VCardRawLine;
import com.headuck.datastore.formats.vcard.textio.VCardRawReader;
import com.headuck.datastore.instream.SmartEncodingInputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedInputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/*
 Copyright (c) 2012-2015, Michael Angstadt
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met: 

 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution. 

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 The views and conclusions contained in the software and documentation are those
 of the authors and should not be interpreted as representing official policies, 
 either expressed or implied, of the FreeBSD Project.
 */

/**
 * <p>
 * Parses {@link VCard} objects from a plain-text vCard data stream.
 * </p>
 * <p>
 * <b>Example:</b>
 * 
 * <pre class="brush:java">
 * File file = new File("vcards.vcf");
 * VCardReader reader = null;
 * try {
 *   reader = new VCardReader(file);
 *   VCard vcard;
 *   while ((vcard = reader.readNext()) != null){
 *     ...
 *   }
 * } finally {
 *   if (reader != null) reader.close();
 * }
 * </pre>
 * 
 * </p>
 * @author Michael Angstadt
 * @see <a href="http://www.imc.org/pdi/vcard-21.rtf">vCard 2.1</a>
 * @see <a href="http://tools.ietf.org/html/rfc2426">RFC 2426 (3.0)</a>
 * @see <a href="http://tools.ietf.org/html/rfc6350">RFC 6350 (4.0)</a>
 */
public class VCardReader implements Closeable {
    final static Logger log = LoggerFactory.getLogger("VCardReader");

	private final VCardRawReader reader;
	private Charset defaultQuotedPrintableCharset;
    private int lastVersion;

	/**
	 * @param str the string to read from
	 */
	public VCardReader(String str) {
		this(new StringReader(str));
	}

	/**
	 * @param in the input stream to read from
	 * @throws IOException if the FileInputStream does not support mark / reset
	 */
	public VCardReader(InputStream in) throws IOException {
		this(new SmartEncodingInputStream(new BufferedInputStream(in)).getReader());
		// this(new InputStreamReader(in));
	}

	/**
	 * @param file the file to read from
	 * @throws FileNotFoundException if the file doesn't exist
	 * (IOException if the FileInputStream does not support mark / reset)
	 */
	public VCardReader(File file) throws IOException {
		this(new SmartEncodingInputStream(new BufferedInputStream(new FileInputStream(file))).getReader());
	}

	/**
	 * @param reader the reader to read from
	 */
	public VCardReader(Reader reader) {
		this.reader = new VCardRawReader(reader);
		defaultQuotedPrintableCharset = this.reader.getEncoding();
		if (defaultQuotedPrintableCharset == null) {
			defaultQuotedPrintableCharset = Charset.defaultCharset();
		}
        this.reader.setDefaultQuotedPrintableCharset(defaultQuotedPrintableCharset);
	}

	/**
	 * Gets whether the reader will decode parameter values that use circumflex
	 * accent encoding (enabled by default). This escaping mechanism allows
	 * newlines and double quotes to be included in parameter values.
	 * @return true if circumflex accent decoding is enabled, false if not
	 * @see VCardRawReader#isCaretDecodingEnabled()
	 */
	public boolean isCaretDecodingEnabled() {
		return reader.isCaretDecodingEnabled();
	}

	/**
	 * Sets whether the reader will decode parameter values that use circumflex
	 * accent encoding (enabled by default). This escaping mechanism allows
	 * newlines and double quotes to be included in parameter values.
	 * @param enable true to use circumflex accent decoding, false not to
	 * @see VCardRawReader#setCaretDecodingEnabled(boolean)
	 */
	public void setCaretDecodingEnabled(boolean enable) {
		reader.setCaretDecodingEnabled(enable);
	}

	/**
	 * <p>
	 * Gets the character set to use when decoding quoted-printable values if
	 * the property has no CHARSET parameter, or if the CHARSET parameter is not
	 * a valid character set.
	 * </p>
	 * <p>
	 * By default, the Reader's character encoding will be used. If the Reader
	 * has no character encoding, then the system's default character encoding
	 * will be used.
	 * </p>
	 * @return the character set
	 */
	public Charset getDefaultQuotedPrintableCharset() {
		return defaultQuotedPrintableCharset;
	}

	/**
	 * <p>
	 * Sets the character set to use when decoding quoted-printable values if
	 * the property has no CHARSET parameter, or if the CHARSET parameter is not
	 * a valid character set.
	 * </p>
	 * <p>
	 * By default, the Reader's character encoding will be used. If the Reader
	 * has no character encoding, then the system's default character encoding
	 * will be used.
	 * </p>
	 * @param charset the character set
	 */
	public void setDefaultQuotedPrintableCharset(Charset charset) {
		defaultQuotedPrintableCharset = charset;
        reader.setDefaultQuotedPrintableCharset(defaultQuotedPrintableCharset);
	}

    /**
     * Reads all vCards from the data stream.
     * @return the vCards
     * @throws IOException if there's a problem reading from the stream
     */
    public List<VCard> readAll() throws IOException {
        List<VCard> vcards = new ArrayList<VCard>();
        VCard vcard;
        while ((vcard = readNext()) != null) {
            vcards.add(vcard);
        }
        return vcards;
    }

    /**
     * Reads next vCard from the data stream.
     * @return the next vCard
     * @throws IOException if there's a problem reading from the stream
     */
	public VCard readNext() throws IOException {
		VCard root = null;
		LinkedList<VCard> vcardStack = new LinkedList<VCard>();
		while (true) {
			//read next line
			VCardRawLine line;
			try {
				line = reader.readLine();
			} catch (VCardParseException e) {
				if (!vcardStack.isEmpty()) {
					log.error ("Parse error at line #{}, {} - {}", e.getLineNumber(), e.getMessage(), e.getLine());
				}
				continue;
			}

			//EOF
			if (line == null) {
				break;
			}

			//handle BEGIN:VCARD
			if ("BEGIN".equalsIgnoreCase(line.getName()) && "VCARD".equalsIgnoreCase(line.getValue())) {

				VCard vcard = new VCard();
				// vcard.setVersion(reader.getVersion());
				vcardStack.add(vcard);

                // Nested vcards are interpreted but ignored
				if (root == null) {
					root = vcard;
				}
				continue;
			}

			if (vcardStack.isEmpty()) {
				//BEGIN component hasn't been encountered yet, so skip this line
				continue;
			}

			//handle END:VCARD
			if ("END".equalsIgnoreCase(line.getName()) && "VCARD".equalsIgnoreCase(line.getValue())) {
				vcardStack.removeLast();

				if (vcardStack.isEmpty()) {
					//done reading the vCard
					break;
				}
				continue;
			}

			//handle property
			{
				String group = line.getGroup();
				List<VCardParam> parameters = line.getParameters();
				String name = line.getName();
				String value = line.getValue();

				VCard curVCard = vcardStack.getLast();

				VCardProperty property = new VCardProperty(group, name, value, parameters);

				curVCard.addProperty(property);
			}
		}
        lastVersion = reader.getVersion();
		return root;
	}

	/**
	 * Closes the underlying {@link Reader} object.
	 */
	public void close() throws IOException {
		reader.close();
	}

    /**
     * Get last read version
     */
    public int getLastVersion() {
        return lastVersion;
    }
}
