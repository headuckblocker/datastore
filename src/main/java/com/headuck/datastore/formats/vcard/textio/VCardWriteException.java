package com.headuck.datastore.formats.vcard.textio;
import java.io.IOException;


/**
 * Thrown when there's a problem writing vCard file.
 */
@SuppressWarnings("serial")
public class VCardWriteException extends IOException {

	/**
	 * @param message a message describing the problem.
	 */
	public VCardWriteException(String message) {
		super(message);
	}

}
