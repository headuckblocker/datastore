package com.headuck.datastore.formats.vcard;


import com.headuck.datastore.formats.vcard.textio.StringUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;

/**
 *
 * Utility to export headuck vcf format
 *
 * Created by headuck on 8/31/16.
 */

public class ExportVcf implements HeaduckVcf {

    static public final int ERROR_WRITER_CREATION_FAILED = -1;
    static public final int ERROR_IO = -4;
    static public final int ERROR_UNSUPPORTED_ENCODING = -6;

    final static Logger log = LoggerFactory.getLogger("ExportVcf");

    VCardWriter writer = null;
    ArrayList<VCard> cardList;

    public ExportVcf(final OutputStream os) {
        VCardWriter writer;
        writer = new VCardWriter (os, DEFAULT_VCARD_VERSION);
        this.writer = writer;
        cardList = new ArrayList<VCard>();
    }

    public void addEntry (String phoneNum, String name, int list, int roam) {
        addEntry (phoneNum, name, list, roam, null);
    }

    public void addEntry (String phoneNum, String name, int list, int roam, String ccat) {
        VCard vCard = new VCard();
        if (name == null) {
            name = "";
        }
        vCard.addProperty(new VCardProperty(null, VCardProperty.VC_FORMATTED_NAME, name, null));
        vCard.addProperty(new VCardProperty(null, VCardProperty.VC_NAME, StringUtils.structured(name,"","","",""), null));
        if (phoneNum != null) {
            vCard.addProperty(new VCardProperty(null, VCardProperty.VC_TELEPHONE, phoneNum, null));
        }
        String roamStr = roamValue(roam);
        if (roamStr != null) {
            vCard.addProperty(new VCardProperty(null, VC_HEADUCK_ROAM, roamStr, null));
        }
        String listTypeStr = listTypeValue(list);
        if (listTypeStr != null) {
            vCard.addProperty(new VCardProperty(null, VC_HEADUCK_LISTTYPE, listTypeStr, null));
        }
        if ((ccat != null) && (ccat.length() > 0)) {
            vCard.addProperty(new VCardProperty(null, VC_HEADUCK_CCAT, ccat, null));
        }
        cardList.add(vCard);
    }

    private String roamValue (int roam) {
        switch (roam) {
            case ROAM_TYPE_NON_ROAMING:
                return VALUE_ROAM_TYPE_NON_ROAMING;
            case ROAM_TYPE_ROAMING:
                return VALUE_ROAM_TYPE_ROAMING;
            default:
                return null;
        }
    }

    private String listTypeValue (int list) {
        switch (list) {
            case LISTTYPE_USER_BLACK:
                return VALUE_LISTTYPE_USER_BLACK;
            case LISTTYPE_USER_WHITE:
                return VALUE_LISTTYPE_USER_WHITE;
            case LISTTYPE_USER_JUNKWHITE:
                return VALUE_LISTTYPE_USER_JUNKWHITE;
            case LISTTYPE_USER_JUNKNAMEWHITE:
                return VALUE_LISTTYPE_USER_JUNKNAMEWHITE;
            case LISTTYPE_USER_JUNK:
                return VALUE_LISTTYPE_USER_JUNK;
            default:
                return null;
        }
    }

    /**
     * Write the entries to file in vcard format
     * @return 0 if success, error codes if fail
     */
    public int writeVcf() throws IOException {
        if (writer == null) {
            log.error("Error creating vcard in ExportVcf");
            return ERROR_WRITER_CREATION_FAILED;
        }
        try {
            for (VCard vCard : cardList) {
                writer.write(vCard);
            }
        } catch (IOException e) {
            log.error("Error writing VCard in VCardWriter", e);
            return ERROR_IO;
        }
        writer.flush();
        writer.close();
        return 0;
    }
}
