package com.headuck.datastore.formats.vcard;

import com.headuck.datastore.formats.vcard.textio.CharacterBitSet;
import com.headuck.datastore.formats.vcard.textio.StringUtils;
import com.headuck.datastore.formats.vcard.textio.VCardRawWriter;
import com.headuck.datastore.formats.vcard.textio.VCardWriteException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 * Created by headuck
 * Reference made to libvcard library
 * https://github.com/pol51/libvcard/tree/master/libvcard
 */

/**
 * Representation of the parameters of a vCard property
 */
public class VCardParam {

    static final String VC_GROUP_TOKEN = "%s=%s";
    public static final String VC_TYPE_TOKEN = "TYPE";
    public static final String VC_ENCODING_TOKEN = "ENCODING";
    public static final String VC_CHARSET_TOKEN = "CHARSET";
    public static final String VC_VALUE_TOKEN = "VALUE";

    // VCardParamGroup
    public static final int Type = 0;
    public static final int Encoding = 1;
    public static final int Charset = 2;
    public static final int Value = 3;
    public static final int Undefined = 4;
    public static final int Others = 5;

    final List<String> VCardValues = Arrays.asList(new String[]
            {"url", "content-id", "cid", "inline"});


    final List<String> VCardEncodings = Arrays.asList(new String[]
            {"quoted-printable", "base64", "8bit", "7bit"});

    // members
    int group;
    String value;

    public VCardParam() {
        value = null;
        group = Undefined;
    }

    public VCardParam(String value, int group) {
        this.group = group;
        this.value = value;
    }

    public VCardParam(String value, String name) {
        this.value = value;

        if (name == null) {
            this.group = guessParameterName(value);
        } else if (VC_TYPE_TOKEN.equalsIgnoreCase(name)) {
            this.group = Type;
        } else if (VC_VALUE_TOKEN.equalsIgnoreCase(name)) {
            this.group = Value;
        } else if (VC_ENCODING_TOKEN.equalsIgnoreCase(name)) {
            this.group = Encoding;
        } else if (VC_CHARSET_TOKEN.equalsIgnoreCase(name)) {
            this.group = Charset;
        } else {
            this.group = Others;  // unrecognized
        }
    }

    public VCardParam(List<String> values, String name) {
        this(encodeValues(values), name);
    }

    public VCardParam(List<String> values, int group) {
        this(encodeValues(values), group);
    }

    /**
     * If any of these characters are found within a parameter value, then the
     * entire parameter value must be wrapped in double quotes (applies to
     * versions 3.0 and 4.0 only).
     */
    private static final CharacterBitSet specialParameterCharacters = new CharacterBitSet(",:;");

    /**
     * Encode values into comma separated single value, adding double quotes as necessary
     * @param values values
     * @return encoded string
     * TODO: double quotes within values are not handled - should force caret escape?
     * TODO: Also if caret escape is on the double quote would be escaped also.
     */
    private static String encodeValues (List<String> values) {
        boolean first = true;
        StringBuilder sb = new StringBuilder();

        for (String parameterValue : values) {
            if (!first) {
                sb.append(',');
            }
            if (specialParameterCharacters.containsAny(parameterValue)) {
                sb.append('"').append(parameterValue).append('"');
            } else {
                sb.append(parameterValue);
            }
            first = false;
        }
        return sb.toString();
    }

    /**
     * Makes a guess as to what a parameter value's name should be.
     *
     * @param value the parameter value
     * @return the guessed name
     */
    private int guessParameterName(String value) {
        // Just included the 2.1 types

        if (VCardValues.contains(value.toLowerCase())) {
            return Value;
        }

        if (VCardEncodings.contains(value.toLowerCase())) {
            return Encoding;
        }

        //otherwise, assume it's a TYPE
        return Type;
    }

    public int getGroup() {
        return group;
    }

    public String getValue() {
        return value;
    }

    public boolean isValid() {
        return !(value == null);
    }

    @Override
    public boolean equals(Object otherObj) {
        if (!(otherObj instanceof VCardParam)) return false;
        final VCardParam other = (VCardParam) otherObj;
        return ((group == other.getGroup()) &&
                ((value == null) ? (other.getValue() == null) : (value.equals(other.getValue()))));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;

        result = prime * result + group;
        result = prime * result + ((value == null) ? 0 : value.hashCode());
        return result;
    }

    /**
     * List of characters which would break the syntax of the vCard if used
     * inside a parameter name. The list of characters permitted by the
     * specification is much more strict, but the goal here is to be as lenient
     * as possible.
     */
    private final CharacterBitSet invalidParameterNameCharacters = new CharacterBitSet(";:=\n\r");

    /**
     * List of characters which would break the syntax of the vCard if used
     * inside a parameter value with caret encoding disabled. These characters
     * cannot be escaped or encoded, so they are impossible to include inside of
     * a parameter value. The list of characters permitted by the specification
     * is much more strict, but the goal here is to be as lenient as possible.
     */
    private final Map<Integer, CharacterBitSet> invalidParamValueChars;
    {
        Map<Integer, CharacterBitSet> map = new HashMap<Integer, CharacterBitSet>();

        map.put(VCard.VC_VER_2_1, new CharacterBitSet(",:\n\r")); //note: semicolons can be escaped
        map.put(VCard.VC_VER_3_0, new CharacterBitSet("\"\r\n"));
        // map.put(VCard.VC_VER_4_0, new CharacterBitSet("\""));

        invalidParamValueChars = Collections.unmodifiableMap(map);
    }

    /**
     * List of characters which would break the syntax of the vCard if used
     * inside a parameter value with caret encoding enabled. These characters
     * cannot be escaped or encoded, so they are impossible to include inside of
     * a parameter value. The list of characters permitted by the specification
     * is much more strict, but the goal here is to be as lenient as possible.
     */
    private final Map<Integer, CharacterBitSet> invalidParamValueCharsWithCaretEncoding;
    {
        Map<Integer, CharacterBitSet> map = new HashMap<Integer, CharacterBitSet>();

        map.put(VCard.VC_VER_2_1, invalidParamValueChars.get(VCard.VC_VER_2_1)); //2.1 does not support caret encoding
        map.put(VCard.VC_VER_3_0, new CharacterBitSet(""));
        // map.put(VCard.VC_VER_4_0, new CharacterBitSet(""));

        invalidParamValueCharsWithCaretEncoding = Collections.unmodifiableMap(map);
    }

    public void writeParam(VCardRawWriter writer) throws IOException {

        int version = writer.getVersion();
        if (value == null) return;

        //check the parameter name for invalid characters
        // if (invalidParameterNameCharacters.containsAny(parameterName)) {
        //     throw Messages.INSTANCE.getIllegalArgumentException(11, propertyName, parameterName, printableCharacterList(invalidParameterNameCharacters.characters()));
        // }

        switch (version) {
            case VCard.VC_VER_2_1:
                boolean isTypeParameter = (group == Type);
                String parameterValue = sanitizeParameterValue(writer.isCaretEncodingEnabled(), version);

                if (isTypeParameter) {
                    //e.g. ADR;HOME;WORK:
                    writer.getFoldedLineWriter().append(VCard.VC_SEPARATOR_TOKEN)
                            .append(parameterValue.toUpperCase());
                } else {
                    //e.g. ADR;FOO=bar;FOO=car:
                    writer.getFoldedLineWriter().append(VCard.VC_SEPARATOR_TOKEN)
                            .append(String.format(VC_GROUP_TOKEN, ParameterName(version), parameterValue));
                }
                break;

            case VCard.VC_VER_3_0:

                //e.g. ADR;TYPE=home,work,"another,value":
                parameterValue = sanitizeParameterValue(writer.isCaretEncodingEnabled(), version);
                writer.getFoldedLineWriter().append(VCard.VC_SEPARATOR_TOKEN)
                        .append(String.format(VC_GROUP_TOKEN, ParameterName(version), parameterValue));
                break;
            default:
                break;

        }
    }

    public static void writeParams(VCardRawWriter writer, List<VCardParam> params) throws IOException {

        switch (writer.getVersion()) {
            case VCard.VC_VER_2_1:
            case VCard.VC_VER_3_0:
                for (final VCardParam param : params) {
                    param.writeParam(writer);
                }
                break;
                // throw new UnsupportedOperationException("Version 3 output not supported");
                // break;
            default:
                break;
        }
    }

    static java.nio.charset.Charset getCharset(List<VCardParam> params) {

        if (params != null) {
            for (final VCardParam param : params) {
                if (param.group == Charset) {
                    java.nio.charset.Charset paramCharset;
                    try {
                        paramCharset = java.nio.charset.Charset.forName(param.value);
                    } catch (Throwable t) {
                        paramCharset = java.nio.charset.Charset.forName("UTF-8");
                    }
                    return paramCharset;
                }
            }
        }
        return java.nio.charset.Charset.forName("UTF-8");
    }

    static String getEncoding(List<VCardParam> params) {
        if (params != null) {
            for (final VCardParam param : params) {
                if (param.group == Encoding) {
                    return (param.value);
                }
            }
        }
        return null;
    }

    static List<VCardParam> setCharset(List<VCardParam> params, java.nio.charset.Charset charset) {
        if (params != null) {
            for (final VCardParam param : params) {
                if (param.group == Charset) {
                    java.nio.charset.Charset paramCharset;
                    try {
                        paramCharset = java.nio.charset.Charset.forName(param.value);
                    } catch (Throwable t) {
                        paramCharset = java.nio.charset.Charset.forName("UTF-8");
                    }
                    if (!charset.equals(paramCharset)) {
                        param.value = charset.name();
                    }
                    return params;
                }
            }
            // Not found
            VCardParam newParam = new VCardParam(charset.name(), Charset);
            params.add(newParam);
            return params;
        }
        // Construct a new list
        VCardParam newParam = new VCardParam(charset.name(), Charset);
        List<VCardParam> newParams = new ArrayList<VCardParam>();
        newParams.add(newParam);
        return newParams;
    }

    static List<VCardParam> setEncoding(List<VCardParam> params, String encoding) {
        if (encoding == null) {
            return params;
        }
        if (params != null) {
            for (final VCardParam param : params) {
                if (param.group == Encoding) {
                    if (!encoding.equalsIgnoreCase(param.value)) {
                        param.value = encoding;
                    }
                    return params;
                }
            }
            // Not found
            VCardParam newParam = new VCardParam(encoding, Encoding);
            params.add(newParam);
            return params;
        }
        // Construct a new list
        VCardParam newParam = new VCardParam(encoding, Encoding);
        List<VCardParam> newParams = new ArrayList<VCardParam>();
        newParams.add(newParam);
        return newParams;
    }

    /**
     * Return sanitized parameter value for serialization.
     * @param caretEncodingEnabled true if caret encoding enabled
     * @param version vcard version
     * @return the sanitized parameter value
     * @throws VCardWriteException if the value contains invalid characters
     */
    private String sanitizeParameterValue(boolean caretEncodingEnabled, int version) throws VCardWriteException {
        CharacterBitSet invalidChars = (caretEncodingEnabled ? invalidParamValueCharsWithCaretEncoding : invalidParamValueChars).get(version);
        if (invalidChars.containsAny(value)) {
            throw new VCardWriteException("Value contains invalid characters: " + value);
        }

        String sanitizedValue = value;
        switch (version) {
            case VCard.VC_VER_2_1:
                //Note: 2.1 does not support caret encoding.

                //escape backslashes
                sanitizedValue = sanitizedValue.replace("\\", "\\\\");

                //escape semi-colons (see section 2)
                return sanitizedValue.replace(";", "\\;");

            case VCard.VC_VER_3_0:
                if (caretEncodingEnabled) {
                    return applyCaretEncoding(sanitizedValue);
                }
                return sanitizedValue;

            /* case VCard.VC_VER_4_0:
                if (caretEncodingEnabled) {
                    return applyCaretEncoding(sanitizedValue);
                }

			/*
			 * 4.0 allows newlines to be escaped (for the LABEL parameter).
			 */
            /*    return newlineRegex.matcher(sanitizedValue).replaceAll("\\\\\\n");

             */
        }

        return "";
    }

    /**
     * Return parameter name for serialization.
     * @return the parameter name
     */
    private String ParameterName(int version) throws VCardWriteException {
        switch (group) {
            case Charset:
                return (VC_CHARSET_TOKEN);
            case Encoding:
                return (VC_ENCODING_TOKEN);
            case Value:
                return (VC_VALUE_TOKEN);
            case Type:
                if (version != VCard.VC_VER_2_1) {
                    return (VC_TYPE_TOKEN);
                }
        }
        throw new VCardWriteException("Parameter name code invalid / unhandled");
        // return ("");
    }

    private String printableCharacterList(String list) {
        return list.replace("\n", "\\n").replace("\r", "\\r");
    }

    /**
     * Applies circumflex accent encoding to a string.
     * @param value the string
     * @return the encoded string
     */
    private String applyCaretEncoding(String value) {
        value = value.replace("^", "^^");
        value = StringUtils.newlineRegex.matcher(value).replaceAll("^n");
        value = value.replace("\"", "^'");
        return value;
    }
}