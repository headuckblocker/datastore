package com.headuck.datastore.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by headuck on 8/19/16.
 * Process junk list for inter / extrapolation
 */

public class DigitMultiScanner {

    private final static Logger log = LoggerFactory.getLogger("MultiScan");

    private DigitStatus[] statuses;
    private int scannerLen;
    private int digit;
    private int maxScope = 0;
    private int maxKeep = 0;
    private byte[] lastNum = null;

    // Circular buffer to keep pn and relevant info
    private byte[][] pnBuffer;
    private int[] catBuffer;
    private int[] scoreBuffer;
    // Corresponding # of preceeding phone numbers not on
    private int[] offsetBuf;

    private int head = 0;   // next position to write
    private int tail = 0;
    private boolean initTail = true;
    private Ptr lastSavePtr = new Ptr();
    private boolean outstandingSave = false;

    // Local use in save(), need not be normalized
    private Ptr currSavePtr = new Ptr();
    private Ptr idxPtr = new Ptr();

    // callback

    private Callback callback;

    public interface Callback {
        void output (final byte[] pn, final int cat);
        void outputGuess (final byte[] pn, final int count);
    }

    /**
     * Constructor
     * @param param int[][3] array: int[i][0]: scope; int[i][1]: threshold;
     *              int[i][2]: max extrapolated # at each end
     */
    public DigitMultiScanner (int digit, int[][] param, Callback callback) {
        scannerLen = param.length;
        this.callback = callback;
        statuses = new DigitStatus[scannerLen];
        this.digit = digit;
        for (int i = scannerLen - 1; i >= 0; i--) {
            maxScope = Math.max(maxScope, param[i][0]);

            // Buffer for keeping back numbers
            // Add 1 to account for added head before clearing tail, 1 for prev tail,
            // and 1 for circular buffer (still has 1 empty slot when full, to distinguish from empty)
            // and 1 for keeping head before save
            maxKeep = Math.max(maxKeep, param[i][1]+ param[i][2] + 4);
            statuses[i] = new DigitStatus(param[i][0], param[i][1], param[i][2]);
        }
        // System.out.format("Maxkeep = %d%n", maxKeep);
        pnBuffer = new byte[maxKeep][];
        scoreBuffer = new int[maxKeep];
        catBuffer = new int[maxKeep];
        offsetBuf = new int[maxKeep];
        outstandingSave = false;
    }

    /**
     * Scan the number pn
     * @param pn byte[] of phone number on list, must be passed in sorted order w/o duplicates
     * @param cat cat to be passed to the callback
     * @param score
     */
    public void scan (final byte[] pn, final int cat, final int score) {
        if (pn.length != digit) {
            throw new IllegalArgumentException("Number of digits mismatch: " + new String(pn));
        }
        // Calc distance from last number. 1 if adjacent or first number
        final int offset = (lastNum == null) ? 0 : calcOffset(pn, lastNum);
        // add to buffer
        pnBuffer[head] = pn;
        scoreBuffer[head] = score;
        catBuffer[head] = cat;
        offsetBuf[head] = offset;  // Note: usage of offsetBuf should be checked against maxscope
        // System.out.format("In #%d: %s offset: %d%n", head, new String(pn), offset);
        head++;
        head %= maxKeep;

        // Normalize all ptrs
        for (int i = scannerLen - 1; i >= 0; i--) {
            statuses[i].normalizePtrs();
        }
        lastSavePtr.normalize();

        if (head == tail) {
            throw new IllegalStateException("DigitMultiScanner circular buffer overflow");
        }

        boolean nextOutstandingSave = false;

        // advance pointer for scanners until no further moves possible
        do {
            // Proceed with scan
            boolean moved = false;
            for (int i = scannerLen - 1; i >= 0; i--) {
                // System.out.println("Scan: "+ i);
                DigitStatus status = statuses[i];
                while (status.movable()) {
                    status.scanNext();
                    moved = true;
                }
            }
            if ((!moved) && (!outstandingSave)) break;
            // save - preserve outstandingSave state only for the next call to scan()
            // mechanism to avoid outputing future nums before being scanned, even if confirmed,
            // to take into account actual phone numbers in range.
            nextOutstandingSave = save(false);
            outstandingSave = false;
        } while (true);

        outstandingSave = nextOutstandingSave;
        lastNum = pn;
    }

    public void end() {
        if (outstandingSave) save(true);
        do {
            // Proceed with scan
            boolean moved = false;

            for (int i = scannerLen - 1; i >= 0; i--) {
                // System.out.println("Scan: "+ i);
                DigitStatus status = statuses[i];

                // if status.consumed == -1, no scanning has started for all scanners and no need to save

                if ((status.consumed != -1) && (((status.consumed + 1) % maxKeep) == head)) {
                    if (!status.ended) {
                        status.scanEnd();
                        moved = true;
                    }
                } else {
                    while (status.movable() && (status.consumed != -1)) {
                        status.scanNext();
                        moved = true;
                    }
                }
            }
            if (!moved) break;
            save(true);
        } while (true);
    }

    /**
     * Check and save ranges
     * @return true if there is outstanding data to be saved after the head is advanced
     */
    boolean save (boolean flush) {

        // System.out.println("Save");
        boolean saved = false;

        // Get the latest confirmed pos
        // currSavePtr.setTo (head, 0);
        if (!flush) {
            currSavePtr.setTo((head + maxKeep - 1) % maxKeep, 1);
        } else {
            currSavePtr.setTo(head, 1);  // This would ensure getting the max nc w/o bound
        }
        boolean set = false;
        for (int i = scannerLen - 1; i >= 0; i--) {
            if (statuses[i].nc.before(currSavePtr)) {
                // System.out.format("Set currSave to %s%n", currSavePtr.toString());
                currSavePtr.setTo(statuses[i].nc);
                set = true;
            }
        }

        if (!set) {

            if (flush) throw new IllegalStateException("nc before head not found");
                /*
                System.out.println("nc before head not found");
                System.out.println("Head = " + head);
                for (int i = scannerLen - 1; i >= 0; i--) {
                    System.out.format("nc %s%n", statuses[i].nc);
                }
                */
            return true;
        }

        boolean[] inRange = new boolean[scannerLen];

        // Save from lastSavePtr to currSavePtr

        // System.out.format ("lastSave: %s currSave: %s%n", lastSavePtr.toString(), currSavePtr.toString());
        while (lastSavePtr.before(currSavePtr)) {
            saved = true;
            idxPtr.setTo(currSavePtr);
            for (int i = scannerLen - 1; i >= 0; i--) {
                Ptr p = statuses[i].inRange;
                if (lastSavePtr.before(p) && p.before(idxPtr)) {
                    idxPtr.setTo(statuses[i].inRange);
                }
            }

            // update inRange which accumulates states
            for (int i = scannerLen - 1; i >= 0; i--) {
                // System.out.format("Inrange [%d] %s %s%n", i, statuses[i].inRange, idxPtr);
                if (statuses[i].inRange.equals(lastSavePtr)) {
                    inRange[i] = true;
                }
            }
            // System.out.format("Output: from %s to %s %n", lastSavePtr.toString(), idxPtr.toString());
            if (!lastSavePtr.isVoid()) {
                output(lastSavePtr, idxPtr, inRange);
            }

            lastSavePtr.setTo(idxPtr);
        }

        if (saved) {
            // update last save
            for (int i = scannerLen - 1; i >= 0; i--) {
                statuses[i].ns.setTo(lastSavePtr);
                statuses[i].inRange.advanceTo(lastSavePtr);
            }
            // If the tail has not been moved (0), lastSavePtr may be of form (0, -X)
            // and without the check this would be freeing before the tail.
            if (!initTail || (lastSavePtr.idx > 0) ) {
                // Release buffer up to lastSavePtr (in format (pos,1) or (pos, -X))
                int pos = lastSavePtr.getPrevPos();

                if (head == tail) {
                    throw new IllegalStateException("DigitMultiScanner circular buffer overflow");
                }
                // Make sure that we do not overrun scanner consumption (due to forward extrapolation)
                for (int i = scannerLen - 1; i >= 0; i--) {
                    int nextConsume = (statuses[i].consumed + 1) % maxKeep;
                    if (beforeIdx (nextConsume, pos)) {
                        pos = nextConsume;
                    }
                }
                tail = pos;
                initTail = false;
                // System.out.println("Tail -> "+tail);
            }
        }
        return false;
    }


    int count0 = 0;
    int count1 = 0;

    public void outputStat () {
        log.info("count0 = {}, count1 = {}", count0, count1);
    }

    private void output (Ptr begin, Ptr end, boolean [] inRange) {

        byte[] pn;

        if (begin.isVoid()) return;
        // System.out.format("output %s %s%n", begin, end);
        // if (end.offset > 1) System.out.format("output %s %s%n", begin, end);

        // pn = calcOutput(pnBuffer[begin.idx], begin.offset);
        // pn1 = calcOutput(pnBuffer[end.idx], end.offset - 1);

        // Output for any b = true
        int count = 0;
        for (boolean b: inRange) {
            if (b) {
                count ++;
            }
        }
        if (count > 0) {
            if (begin.offset > 0) {
                if (end.idx != begin.idx) {
                    log.warn("output: begin offset > 0 with different begin and end ptr: {} {}", begin, end);
                    return;
                }
            }
            int i = begin.idx;
            int offset = begin.offset;
            do {
                if (offset != 0) {
                    pn = calcOutput(pnBuffer[i], offset);
                    // output pn
                    // System.out.format("%s (%d)%n", new String(pn), count);
                    callback.outputGuess(pn, count);
                    count1++;
                    offset ++;
                } else {
                    pn = pnBuffer[i];
                    // output pn
                    // System.out.format("%s ***%n", new String(pn));
                    callback.output(pn, catBuffer[i]);
                    count0++;
                    if (i != end.idx) {
                        i = (i + 1) % maxKeep;
                        offset = -(offsetBuf[i] - 1);
                    } else {
                        offset ++;
                    }
                }
            } while ((i != end.idx) || (offset < end.offset));

        } else {
            int i = begin.idx;
            if (begin.offset > 0) i = (i + 1) % maxKeep;
            int j = end.idx;
            if (end.offset <= 0) {
                if (i == j) return;  // initially tail = 0 and may incorrectly wrap around w/o this
                j = (j + maxKeep - 1) % maxKeep;
            }
            while ( (i == j) || (beforeIdx(i, j)) ) {  // i<=j
                pn = pnBuffer[i];
                // System.out.format("%s ---%n", new String(pn));
                callback.output(pn, catBuffer[i]);
                count0++;
                i = (i + 1) % maxKeep;
            }
        }
            /*if (end.offset > 1) {
                if (count > 0)
                    System.out.format("%s - %s (%d) %n", new String(pn), new String(pn1), count);
            }*/

    }

    /**
     * Calc offset between 2 nums. For offset >= scope returning scope is enough
     * @param pn current number, must > last number
     * @param lastNum last number
     * @return offset, max = scope
     */
    private int calcOffset (byte[] pn, byte[] lastNum) {
        if (lastNum == null) return 1;
        // perform byte[] subtraction
        long diff = 0;
        long e = 1;
        int d;
        for (int i = digit-1; i >=0; i-- ) {
            d = pn[i] - lastNum[i];
            diff += d * e;
            if (diff > maxScope) return maxScope;  // since curr number > last, this will not shrink to < scope again
            e *= 10;
        }
        if (diff <= 0) {
            throw new IllegalStateException("Scanned numbers not in order");
        }
        return diff > maxScope? maxScope: (int)diff;
    }

    /**
     * Returns calc pn + offset in char[];
     * @param pn
     * @param offset
     * @return pn + offset
     */
    private byte[] calcOutput(byte[] pn, int offset) {
        byte[] ret = new byte[digit];
        int d;
        if (offset <=0) {
            long diff = -offset;
            for (int i = digit - 1; i >= 0; i--) {
                d = (int) (diff % 10);
                ret[i] = (byte) (pn[i] - d);
                if (ret[i] < '0') {
                    ret[i] += 10;
                    d -= 10;
                }
                diff -= d;
                diff /= 10;
            }
        } else {
            long diff = offset;
            for (int i = digit - 1; i >= 0; i--) {
                d = (int) (diff % 10);
                ret[i] = (byte) (pn[i] + d);
                if (ret[i] > '9') {
                    ret[i] -= 10;
                    d -= 10;
                }
                diff -= d;
                diff /= 10;
            }
        }
        return ret;
    }

    /**
     * return if idx1 is before idx2 in the circular index
     * @return
     */
    private boolean beforeIdx (int idx1, int idx2) {
        if ((idx1 != -1) && (idx1 < tail)) idx1 += maxKeep;
        if ((idx2 != -1) && (idx2 < tail)) idx2 += maxKeep;
        return (idx1 < idx2);
    }


    // Keeping status of scanners
    private class DigitStatus {
        // embeded scanner
        DigitScanner scanner;
        int extrapolate;
        //
        // ^ns    ^inRange   ^nc
        // Non confirmed ptr and offset: start of non-confirmed pos, after last confirmed position
        Ptr nc = new Ptr();
        // Start of confirmed but non saved ptr and offset
        Ptr ns = new Ptr();
        // Start of confirmed in-range ptr, not yet saved
        Ptr inRange = new Ptr();

        // Last consumed pos
        int consumed = -1;
        boolean ended = false;

        DigitStatus(int scope, int threshold, int extrapolate) {
            scanner = new DigitScanner(scope, threshold);
            this.extrapolate = extrapolate;
        }

        boolean movable () {
            // System.out.format("nc (%d %d) ns (%d %d) consumed %d%n", nc.idx, nc.offset, ns.idx, ns.offset, consumed);
            // System.out.format("head: %d tail: %d%n", head, tail);
            if (!(nc.equals(ns))) {
                // System.out.println("not equal: not movable");
                return false;
            }
            // System.out.println("consumed = " + consumed);
            if ((consumed == -1) || (((consumed + 1) % maxKeep) != head)) {
                // System.out.println ("movable");
                return true;
            }
            // System.out.println("Head reached: not movable");
            return false;
        }

        /**
         * Called after advancing the head to normalise any + offset Ptrs
         */
        void normalizePtrs () {
            if (nc.offset > 0) {
                nc.normalize();
            }
            if (ns.offset > 0) {
                ns.normalize();
            }
            if (inRange.offset > 0) {
                inRange.normalize();
            }
        }

        void scanNext() {
            boolean oldOn = scanner.isOn;
            int scanPos;
            if (consumed == -1) {
                if ((tail != 0) || (head != 1)) {
                    throw new IllegalStateException("Invalid state when scan is started");
                }
            }
            scanPos = (consumed + 1) % maxKeep;
            int ret = scanner.scan(offsetBuf[scanPos]);

            switch (ret) {
                case DigitScanner.PhoneCircularBuffer.STATE_COUNT_OFF:
                    // System.out.println("Off");
                    extrapolateForward();
                    break;
                case DigitScanner.PhoneCircularBuffer.STATE_COUNT_ON:
                    // System.out.println("On");
                    if ((scanner.lastOnOffset < 0) || (scanner.numCount <= scanner.lastOnOffset)) {
                        // not bounded by last offset, calc gap (i.e. ns != inRange)
                        // calc inRange from numCount, incl. extrapolate back if needed
                        extrapolateBackward();
                    }
                    // advance nc to next position
                    nc.advanceTo(scanPos, 1);
                    break;
                case DigitScanner.PhoneCircularBuffer.STATE_NO_CHANGE:
                    if (oldOn) {
                        // System.out.println("Keep On");
                        // Still on - add (since last offset) to confirmed
                        // Advance non-confirmed pointer to after current
                        nc.advanceTo(scanPos, 1);
                    } else {
                        // System.out.format("Keep Off: numCount %d LastOffset %d%n", scanner.numCount, scanner.lastOnOffset);
                        // Still off: advance non-confirmed pointer
                        if ((scanner.lastOnOffset < 0) || (scanner.numCount <= scanner.lastOnOffset)) {
                            int beginNum = ((consumed + 2) % maxKeep) - scanner.numCount;
                            if (beginNum < 0) beginNum += maxKeep;
                            // nc / inRange advance to max extrapolate back from numCount
                            // System.out.format("Keep Off: consumed %d numcount %d beginNum %d%n",
                            //        consumed, scanner.numCount, beginNum);
                            // System.out.format("Head %d Tail %d%n", head, tail);
                            // Since the scanner count numbers independently,
                            // guard against beforeNum too old and falling outside the buffer
                            // In such case inRange / nc would not be advanced.
                            if (beforeIdx(beginNum, head)) {
                                inRange.advanceTo(beginNum, -extrapolate);
                                nc.advanceTo(beginNum, -extrapolate);
                            }
                        } //else {
                        // non-confirmed pointer remains
                        // }
                    }
            }
            consumed = scanPos;
        }

        void scanEnd() {
            // Assume consumed + 1 is head and not scan ended
            if (scanner.isOn) {
                // emulate Off
                extrapolateForward();
            } else {
                inRange.advanceTo(consumed, 1);
                nc.advanceTo(consumed, 1);
            }
            ended = true;
        }


        /**
         * Calc extrapolate forward from offset -1 [consumed] & adv non-confirmed pointer if needed
         */
        void extrapolateForward() {
            // Calc score at consumed (& backwards) and determine extrapolate value
            int extra = calcExtraForward(consumed);  // stub
            if (extra > extrapolate) {
                throw new IllegalStateException("calcExtraForward return a larger number than " + extrapolate);
            }
            // Take forward nc to consumed + extra pos, if < nc
            nc.advanceTo (consumed, extra + 1);
        }

        /**
         * Calc extrapolate backward from offset -1 & adv non-confirmed pointer if needed
         */
        void extrapolateBackward() {
            // consumed can be -1
            int beginNum = ((consumed + 2) % maxKeep) - scanner.numCount;
            if (beginNum < 0) beginNum += maxKeep;

            // Calc score at beginNum (& forward) and determine extrapolate value
            int extra = calcExtraBackward(beginNum);  // stub
            if (extra > extrapolate) {
                throw new IllegalStateException("calcExtraForward return a larger number than " + extrapolate);
            }
            // Since the scanner count numbers independently,
            // guard against beforeNum too old and falling outside the buffer
            // In such case inRange / nc would not be advanced.
            if (beforeIdx(beginNum, head)) {
                // inRange should == nc/ns at the beginning,
                // but nc may be after beginNum, in such case inRange should remain
                // (nc would be advanced subsequently, no gap between ns and inRange)
                inRange.advanceTo (beginNum, -extra);
            }

        }

        /**
         * TODO: Stubs
         * @param endNum
         * @return
         */
        int calcExtraForward(int endNum) {
            if (endNum == -1) {
                throw new IllegalStateException("Not yet started scanning before extrapolate");
            }
            byte[] pn = pnBuffer[endNum];
            // Do not cross 100 boundary
            int extra = extrapolate;
            if (pn.length > 2) {
                int end2digits = (pn[pn.length-2] - '0') * 10 + (pn[pn.length-1] - '0');
                if (extra > (99 - end2digits)) {
                    extra = 99 - end2digits;
                }
            }
            return extra; // return at most extrapolate
        }

        int calcExtraBackward(int beginNum) {
            byte[] pn = pnBuffer[beginNum];
            // Do not cross 00 boundary
            int extra = extrapolate;
            if (pn.length > 2) {
                int end2digits = (pn[pn.length-2] - '0') * 10 + (pn[pn.length-1] - '0');
                if (extra > end2digits) {
                    extra = end2digits;
                }
            }
            return extra; // return at most extrapolate
        }

    }

    private class Ptr {
        int idx = -1;
        int offset;

        boolean isVoid() {
            return (idx == -1);
        }
        @Override
        public boolean equals(Object o) {
            if ((o != null) && (o instanceof Ptr)) {
                Ptr oPtr = (Ptr) o;
                if ((idx == -1) && (oPtr.idx == -1)) return true;
                return (idx == oPtr.idx && (offset == oPtr.offset));
            }
            return false;
        }

        /**
         * Compare with index w/o offset
         * @param idx
         * @return true if the Ptr is before the position of idx, or Ptr is void
         */
        boolean before (int idx) {
            if (idx == this.idx) return (offset < 0);
            return (beforeIdx(this.idx, idx));
        }

        /**
         * Compare with index w offset, assume normalised
         */
        boolean before (final Ptr other) {
            if (beforeIdx(this.idx, other.idx)) return true;
            if (other.idx == this.idx) {
                return (other.offset > this.offset);
            }
            return false;
        }

        /**
         * copy value from other Ptr
         */
        void setTo (Ptr other) {
            this.idx = other.idx;
            this.offset = other.offset;
        }

        /**
         * set value to idx / offset
         * Note: currently skipped normalisation
         */
        void setTo (int idx, int offset) {
            this.idx = idx;
            this.offset = offset;
        }

        /**
         * set value to idx / offset only if the current value is before it
         */
        void advanceTo (int idx, int offset) {
            // Normalize
            normalize (idx, offset);
            idx = normIdx;
            offset = normOffset;
            boolean before = false;
            if (beforeIdx(this.idx, idx)) {
                before = true;
            }  else if (this.idx == idx) {
                before = (offset > this.offset);
            }
            if (before) {
                this.idx = idx;
                this.offset = offset;
            }
        }

        /**
         * set value to Ptr only if the current value is before it
         */
        void advanceTo (Ptr other) {
            boolean before = false;
            if (beforeIdx(this.idx, other.idx)) {
                before = true;
            }  else if (this.idx == other.idx) {
                before = (other.offset > this.offset);
            }
            if (before) {
                this.idx = other.idx;
                this.offset = other.offset;
            }
        }

        // Multiple return values for normalize
        int normIdx, normOffset;

        /**
         * Normalize Ptr components to use -ve offset where possible with ref to nearest number
         * @param idx
         * @param offset
         */
        void normalize (int idx, int offset) {
            int idx1;
            if (offset >0) {
                while ((offset > 0) && ((idx1 = (idx + 1) % maxKeep) != head)) {
                    int offset1 = offsetBuf[idx1];
                    if (offset1 < maxScope) {
                        offset -= offset1;
                        idx = idx1;
                    } else {
                        break;
                    }
                }
            } else if (offset < 0) {
                //if (idx == tail) {
                //    if (offsetBuf[idx] == 0) log.info("Normalising to position earlier than tail in initial run" );
                //    else if (offsetBuf[idx] >= maxScope) log.info("Normalising to position earlier than tail with >=maxScope offset" );
                //}
                do {
                    if (idx == tail) {
                        break;
                        // throw new IllegalStateException("Cannot normalise to position earlier than buffer");
                    }
                    int offset1 = offsetBuf[idx];
                    if (offset1 >= maxScope) break;
                    if (offset <= -offset1) {
                        offset += offset1;
                        idx--;
                        if (idx < 0) idx += maxKeep;
                    } else {
                        break;
                    }
                } while (true);

            }
            normIdx = idx;
            normOffset = offset;
        }

        /**
         * Normalize the Ptr itself
         */
        void normalize() {
            normalize(this.idx, this.offset);
            this.idx = this.normIdx;
            this.offset = this.normOffset;
        }

        /**
         * get the previous num position
         */
        int getPrevPos () {
            if (this.offset > 0) return this.idx;
            if (this.idx == tail) {
                throw new IllegalStateException("getPrevPos: getting earlier than tail");
            }
            int idx = this.idx - 1;
            if (idx < 0) idx += maxKeep;
            return idx;
        }


        @Override
        public String toString() {
            return "(" + idx + " " + offset + ")";
        }

    }


    private class DigitScanner {
        // Input parameters
        // private int digit;
        // private int scope;

        private PhoneCircularBuffer phoneBuffer;
        /* private byte[] beginRange;
        private byte[] lastRange;
        private byte[] lastNum = null;
        */
        boolean isOn;
        int lastOnOffset = -1;
        int numCount;

        // int count = 0;

        DigitScanner(int scope, int threshold) {
            // this.digit = digit;
            // this.scope = scope;
            this.isOn = false;
            phoneBuffer = new PhoneCircularBuffer(scope, threshold);
        }

        /**
         * Given the offset of the present number (representing a newly added number to the sequence),
         * return status change.
         * Status can be retrieved from members: isOn, lastOnOffset and numCount
         *
         * @param offset
         * @return status change STATE_NO_CHANGE / STATE_COUNT_OFF / STATE_COUNT_ON
         */
        int scan(int offset) {

            // System.out.println(" " + offset +  " "+ new String (pn));

            // Check if previous range should end and emit if so
            int ret = phoneBuffer.addOffset(offset);
            if (ret == PhoneCircularBuffer.STATE_COUNT_OFF) {
                if (!isOn)
                    throw new IllegalStateException("Change to off received when the state is already off");
                // Emit the previous range
                // emit (beginRange, lastRange);

                lastOnOffset = 1;
                isOn = false;
            } else if (ret == PhoneCircularBuffer.STATE_COUNT_ON) {
                // State on, ret = scope size
                if (isOn)
                    throw new IllegalStateException("Change to on received when the state is already on");
                isOn = true;
                // Get the number of numbers confirmed to be on
                // lastOnOffset may be -1
                if (lastOnOffset >= 0) lastOnOffset++;
                // beginRange = calcOutput (pn, ret);  // ret = Scope Size, inclusive
                // System.out.println ("Begin: " + new String(beginRange) + " pn = " + new String(pn) + " diff =" + ret);
            } else {
                // No change
                if (isOn) {
                    // lastRange = pn;
                    // ret = 1;
                } else {
                    if (lastOnOffset >= 0) {
                        lastOnOffset++;
                    }
                }
            }

            numCount = phoneBuffer.numCount;
            // lastNum = pn;
            // System.out.println("lastNum ->" + new String(lastNum));
            return ret;
        }


        public void end() {
            // if (isOn) {
            //    emit(beginRange, lastRange);
            // }
            isOn = false;
                /*beginRange = null;
                lastRange = null;
                lastNum = null;
                */
        }

            /*
            void emit (byte[] beginRange, byte[] lastRange) {
                count ++;
                System.out.format("%d: %s - %s%n", count, new String(beginRange), new String(lastRange));
            }
            */

        /**
         * Buffer to keep track of number of num scanned in a consecutive num range
         * in a compressed circular buffer to save fill and move time
         */
        private class PhoneCircularBuffer {
            static final int STATE_NO_CHANGE = 0;    // no cross of the threshold
            static final int STATE_COUNT_ON = -2;  // Change from num < threshold -> num >= threshold
            static final int STATE_COUNT_OFF = -1;    // Change from num >= threshold -> num < threshold

            int bufSize;  // Buffer size.

            int size;     // specified scope size
            int threshold;
            // Internal states
            int[] buf;  // compressed binary (num / space) buffer: even for num, odd for space

            // Pointers should always point to even positions
            int startidx; // idx of start
            int endidx;  // idx of next position

            int scopeSize;  // current size, max = size
            int numCount;   // # of num of scope

            boolean init = true;

            PhoneCircularBuffer(int size, int threshold) {
                this.size = size;
                // Ensure the buffer size is even
                // Even position is for number count and odd for following space count
                if ((size % 2) != 0) size++;
                this.bufSize = size + 2;  // to allow for 1 empty slot (+2) for full circular buffer
                this.threshold = threshold;
                buf = new int[this.bufSize];
                resetBuf();
            }

            void resetBuf() {
                // Flush all elements and reset position
                startidx = 0;
                endidx = 0;
                scopeSize = 0;
                numCount = 0;
                buf[startidx] = 0;
            }

            /**
             * Add a num representation to buffer at 'offset' pos from last one (min 1)
             *
             * @param offset
             * @return status change: STATE_NO_CHANGE / STATE_COUNT_OFF / STATE_COUNT_ON
             */
            int addOffset(int offset) {
                //
                final int ret;

                if (offset < 1) {
                    // guard against illegal offset. Offset = 0 for initial scan # only
                    if (init && (offset == 0)) {
                        offset = 1;
                        init = false;
                    } else {
                        throw new IllegalArgumentException("Offset for circular buffer less than 1");
                    }
                }
                // Overflow?
                if (offset >= size) {
                    // can emit or dump the current buffer and start afresh
                    if (numCount >= threshold) {
                        ret = STATE_COUNT_OFF;
                    } else {
                        ret = STATE_NO_CHANGE; // remain off
                    }
                    resetBuf();
                    addBuf(1);
                } else {
                    int overflow = offset + scopeSize - size;
                    if (overflow > 0) {
                        // Need to shift out at least "overflow" position first
                        int sizeConsumed = consumeBuf(overflow);
                        // if sizeConsumed = 1, state unchanged
                        if (((numCount + sizeConsumed) >= threshold) && (numCount + 1 < threshold)) {
                            ret = STATE_COUNT_OFF;
                        } else {
                            ret = STATE_NO_CHANGE;
                        }
                        // STATE_COUNT_ON is not possible, since scopeSize must > 0 and head of scopeSize always represents a num
                        // i.e. (((numCount + sizeConsumed) < threshold) && (numCount + 1 >= threshold))
                        // cannot occur since sizeConsumed >= 1
                    } else {
                        // pure add
                        if ((numCount + 1) == threshold) {
                            ret = STATE_COUNT_ON;
                        } else {
                            ret = STATE_NO_CHANGE;
                        }
                    }
                    addBuf(offset);
                }
                return ret;
            }

            /**
             * Consume overflow position at the head, change buf pointers
             *
             * @param overflow number
             * @return number consumed
             */
            int consumeBuf(int overflow) {
                int origNumCount = numCount;
                while (overflow > 0) {
                    if (buf[startidx] > overflow) {
                        buf[startidx] -= overflow;
                        numCount -= overflow;
                        scopeSize -= overflow;
                        break;
                    }
                    // Consume the num represented by the entire buf[startidx] and
                    // also shrink scopeSize by buf[startidx + 1]
                    // advance to next even buf[]
                    int consSize = buf[startidx];
                    overflow -= consSize + buf[startidx + 1];  // overflow may become negative
                    numCount -= consSize;
                    scopeSize -= consSize + buf[startidx + 1];

                    if (startidx == endidx)
                        throw new IllegalStateException("Circular buffer consume wrap around");

                    startidx += 2;
                    // Check
                    startidx %= bufSize;


                }
                return origNumCount - numCount;
            }

            /**
             * Add a num at offset position (min 1) from the tail
             * offset already checked for >= 1
             *
             * @param offset
             */
            void addBuf(int offset) {
                if (offset == 1) {
                    buf[endidx]++;
                    scopeSize++;
                } else {
                    // Add offset -1 "empty" pos
                    buf[endidx + 1] = offset - 1;
                    endidx += 2;
                    endidx %= bufSize;
                    scopeSize += offset;
                    if (endidx == startidx)
                        throw new IllegalStateException("Circular buffer add wrap around");
                    buf[endidx] = 1;
                }
                numCount++;
            }
        }
    }
}