package com.headuck.datastore.utils;
public final class Utils {

    private Utils () {
    }

    /**
     *
     * Replacement for String.getBytes() tailored for UTF-8 output
     * for better performance
     * @param str Input string to convert to byte []
     * @return UTF-8 byte[] converted from input string
     *
     */
	public static byte[] stringToBytes(final String str) {
		boolean nonAscii = false;

		int l = str.length();
		byte[] b = new byte[l];
		int i,j;
		int bytesToWrite;
		long utf32=0;

		for (i = 0, j = 0; i < l; i++) {
			char s = str.charAt(i);
			if (s>>7 == 0) {
				b[j++] = (byte)s;
			} else {
				/*
				 * Convert from UTF16 to UTF8, based on ConvertUTF.c from Unicode, Inc.
				 */
				final char UNI_SUR_HIGH_START = '\uD800';
				final char UNI_SUR_HIGH_END = '\uDBFF';
				final char UNI_SUR_LOW_START = '\uDC00';
				final char UNI_SUR_LOW_END = '\uDFFF';
				final char UNI_REPLACEMENT_CHAR = '\uFFFD';
				final int firstByteMark[] = {0x00, 0x00, 0xC0, 0xE0, 0xF0};

				if (!nonAscii) {
					/* 
					 * Optimized for the case when most of the calls are ASCII only.
					 */
					byte[] newb = new byte[l*4];
					System.arraycopy(b, 0, newb, 0, j);
					b = newb;
					nonAscii = true;
				}
				utf32 = s;
				if ((s >= UNI_SUR_HIGH_START) && (s <= UNI_SUR_HIGH_END)) {
					/* If the 16 bits following the high surrogate are in the source buffer... */
			        if (++i < l) {
			            char s2 = str.charAt(i);
			            /* If it's a low surrogate, convert to UTF32. */
			            if (s2 >= UNI_SUR_LOW_START && s2 <= UNI_SUR_LOW_END) {
			            	 utf32 = ((long)(s - UNI_SUR_HIGH_START) << 10)
			                       + (long)(s2 - UNI_SUR_LOW_START) + 0x0010000L;
			            } else {
				            // illegal: skip high surrogate
			            	i--;
			            	continue;
			            }
			        } else { /* We don't have the 16 bits following the high surrogate. */
			            break; /* ignore high surrogate */
			        }
				}
				else if (s >= UNI_SUR_LOW_START && s <= UNI_SUR_LOW_END) {
		            // illegal: skip low surrogate
	            	continue;
			    }
				/* Figure out how many bytes the result will require */
				if (utf32 < 0x80L) {
					bytesToWrite = 1;
				} else if (utf32 < 0x800L) {
					bytesToWrite = 2;
				} else if (utf32 < 0x10000L) {
					bytesToWrite = 3;
				} else if (utf32 < 0x110000L) {
					bytesToWrite = 4;
				} else {
					bytesToWrite = 3;
                    utf32 = UNI_REPLACEMENT_CHAR;
				}

				j += bytesToWrite;
                switch (bytesToWrite) { /* note: everything falls through. */
                	case 4: b[--j] = (byte)((utf32 | 0x80L) & 0xBFL);
                		    utf32 >>= 6;
					case 3: b[--j] = (byte)((utf32 | 0x80L) & 0xBFL);
						    utf32 >>= 6;
					case 2: b[--j] = (byte)((utf32 | 0x80L) & 0xBFL);
						    utf32 >>= 6;
				    case 1: b[--j] = (byte)(utf32 | firstByteMark[bytesToWrite]);
                }
                j += bytesToWrite;
			}
		}
		if (!nonAscii) {
			return b;
		} else {
			byte[] retb = new byte[j];
			System.arraycopy(b, 0, retb, 0, j);
			return retb;
		}
	}

	/**
	 * Parses the string argument as a signed integer i in radix 10
	 * ParseInt replacement for radix 10 and byte buffer for efficiency
	 * This does NOT handle non-ascii (eg unicode code points) chars
	 *
	 * Adopted from java.lang.Integer
	 *
	 */
	  public static int parseInt(final byte[] s) throws NumberFormatException {

		  if (s == null) {
			  throw new NumberFormatException("null");
		  }

          int result = 0;
          boolean negative = false;
          int i = 0, len = s.length;
          int limit = -Integer.MAX_VALUE;
          int multmin;
          int digit;
          if (len > 0) {
              byte firstByte = s[0];
              if (firstByte < (byte)'0') { // Possible leading "-"
            	  if (firstByte == (byte)'-') {
            		  negative = true;
            		  limit = Integer.MIN_VALUE;
                  } else
                      throw new NumberFormatException(new String(s));
                  if (len == 1) // Cannot have lone "-"
                      throw new NumberFormatException(new String(s));
                  i++;
              }
              multmin = limit / 10;
	          while (i < len) {
	        	  // Accumulating negatively avoids surprises near MAX_VALUE
                  digit = s[i++] - (byte)'0';
	              if ((digit < 0) || (digit > 9)) {
	            	  throw new NumberFormatException(new String(s));
	              }
                  if (result < multmin) {
                      throw new NumberFormatException(new String(s));
                  }
                  result *= 10;
                  if (result < limit + digit) {
                	  throw new NumberFormatException(new String(s));
                  }
                  result -= digit;
               }
           } else {
               throw new NumberFormatException(new String(s));
           }
           return negative ? result : -result;
       }




    /**
     * Comparison function for two byte[] strings
     * @param b1 string1
     * @param b2 string2
     * @return compare value = abs(b1 - b2)
     */
    public static int compare(final byte[] b1, final byte[] b2) {
        if ((b1 == null) && (b2 == null)) return 0;
        else if ((b1 == null)) return -1;
        else if ((b2 == null)) return 1;

        int len = Math.min(b1.length, b2.length);
        for (int i = 0; i < len; i++) {
            final int ba1 = (b1[i] & 0xFF);
            final int ba2 = (b2[i] & 0xFF);
            if (ba1 != ba2) return ba1 - ba2;
        }
        return (b1.length - b2.length);
    }
	/**
	 * Comparison function for two byte[] strings
	 * @param b1 string1
	 * @param b2 string2
	 * @return compare value = abs(b1 - b2)
	 */
	public static int compareMinLen(final byte[] b1, final byte[] b2) {
		if ((b1 == null) && (b2 == null)) return 0;
		else if ((b1 == null)) return -1;
		else if ((b2 == null)) return 1;

		int len = Math.min(b1.length, b2.length);
		for (int i = 0; i < len; i++) {
			final int ba1 = (b1[i] & 0xFF);
			final int ba2 = (b2[i] & 0xFF);
			if (ba1 != ba2) return ba1 - ba2;
		}
		return 0;
	}

    // For decoding of base64
    // source: http://stackoverflow.com/questions/469695/decode-base64-data-in-java
    private final static char[] ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".toCharArray();

    private static int[] toInt = new int[128];

    static {
        for(int i=0; i< ALPHABET.length; i++){
            toInt[ALPHABET[i]]= i;
        }
    }

    public static String[] decodeMultiple(String s) {
    	String multStr = decode(s);
    	return multStr.split(",");
	}

    /**
     * Translates the specified Base64 string into a String
     *
     * @param s the Base64 string (not null)
     * @return the String (not null)
     */
    public static String decode(String s) {
        byte[] buffer = decodeToBuffer(s);
        if (buffer == null) return "";
        return new String (buffer);
    }

	/**
	 * Translates the specified Base64 string into a byte array.
	 *
	 * @param s the Base64 string (not null)
	 * @return buffer (may be null)
	 */
    public static byte[] decodeToBuffer(String s) {
		byte[] buffer;
		try {
			int delta = (s.endsWith("((") || s.endsWith("==")) ? 2
					: (s.endsWith("(") || s.endsWith("=")) ? 1 : 0;
			buffer = new byte[s.length() * 3 / 4 - delta];
			int mask = 0xFF;
			int index = 0;
			for (int i = 0; i < s.length(); i += 4) {
				int c0 = toInt[s.charAt(i)];
				int c1 = toInt[s.charAt(i + 1)];
				buffer[index++] = (byte) (((c0 << 2) | (c1 >> 4)) & mask);
				if (index >= buffer.length) {
					return buffer;
				}
				int c2 = toInt[s.charAt(i + 2)];
				buffer[index++] = (byte) (((c1 << 4) | (c2 >> 2)) & mask);
				if (index >= buffer.length) {
					return buffer;
				}
				int c3 = toInt[s.charAt(i + 3)];
				buffer[index++] = (byte) (((c2 << 6) | c3) & mask);
			}
		} catch (StringIndexOutOfBoundsException e) {
			return null;
		}
		return buffer;
	}
}
