package com.headuck.datastore.instream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipInputStream;


public class ZipEntryInputStream extends InputStream {
	
	private ZipInputStream zipInputStream;
	private long min_size;
	final Logger log = LoggerFactory.getLogger("ZipEntryIS");

    /**
     * Constructs a new JunkZipInputStream, advance to a zip entry
     *  
     * @param in  the InputStream to delegate to
     * @throws IOException if an I/O error occurs
     */
	
	public ZipEntryInputStream (InputStream in) throws IOException {		
        zipInputStream = new ZipInputStream(in);
        this.min_size = 0;
        advanceZipEntry();
    }

    /**
     * Constructs a new JunkZipInputStream specifying min file size, advance to a zip entry
     *  
     * @param in  the InputStream to delegate to
     * @param min_size minimum uncompressed size of the zip entry
     * @throws IOException if an I/O error occurs
     */

	public ZipEntryInputStream(InputStream in, long min_size) throws IOException {		
        zipInputStream = new ZipInputStream(in);
        this.min_size = min_size;
        advanceZipEntry();
    }

	/**
	 * Advance to entry to prepare reading of the content
	 * @throws IOException if an I/O error occurs
	 * 
	 */
	
	private void advanceZipEntry() throws IOException {
		ZipEntry entry;
		do {
			entry = zipInputStream.getNextEntry();
			if ((entry != null) && (entry.isDirectory())) {
				continue; 
			}
			// choose the first file that occupies at least half of the zip file size
			if ((entry == null) || (entry.getCompressedSize() >= min_size)) 
			{
				break;
			}
		} while (true);
		/*
		 * Throw zip exception if entry satisfying criteria not found.
		 */
		if (entry == null) {
			throw new ZipException("HKJunkCall zip entry not found");
		}
		// Log debug info
		log.info("Reading ZIP entry {} with length {}.", entry.getName(), entry.getSize());
	}

    /**
     * Invokes the zipInputStream's <code>read()</code> method to read from zip entry.
     * @return the byte read or -1 if the end of stream
     * @throws IOException if an I/O error occurs
     */
	@Override
	public int read() throws IOException {
		//return (zipInputStream.read());
		int ret;
        do {
            ret = zipInputStream.read();
        } while (ret == 0);
		return ret;
	}
	

    /**
     * Invokes the zipInputStream's <code>read(byte[])</code> method to read from zip entry.
     * @param bts the buffer to read the bytes into
     * @return the number of bytes read or -1 if the end of stream
     * @throws IOException if an I/O error occurs
     */
    @Override
    public int read(byte[] bts) throws IOException {
        int ret;
        do {
            ret = zipInputStream.read(bts);
        } while ((ret == 0) && (bts.length != 0));
		return ret;
    }

    /**
     * Invokes the zipInputStream's <code>read(byte[], int, int)</code> method  to read from zip entry.
     * @param bts the buffer to read the bytes into
     * @param off The start offset
     * @param len The number of bytes to read
     * @return the number of bytes read or -1 if the end of stream
     * @throws IOException if an I/O error occurs
     */
    @Override
    public int read(byte[] bts, int off, int len) throws IOException {
   	    // return (zipInputStream.read(bts, off, len));
        int ret;
        do {
            ret = zipInputStream.read(bts, off, len);
        } while ((ret == 0) && (len != 0));
		return ret;
    }
    
    /**
     * Skips the zipInputStream over the specified number of bytes.
     *
     * @param length  the number of bytes to skip
     * @return the actual number of bytes skipped
     * @throws IOException if an I/O error occurs
     *
     */
    @Override
    public long skip(final long length) throws IOException {
        final long skip = zipInputStream.skip(length);
        return skip;
    }

    /**
     * Closes the zipInputStream.
     *
     * @throws IOException if an I/O error occurs
     *
     */
	@Override
	public void close() throws IOException {
		zipInputStream.close();
		super.close();
	}
    
	
	 
	/**
	 * Static method to test if input stream is a zip archive.
	 * 
	 * @param in the input stream to test            
	 * @return true if the input stream is a zip stream
	 */
	public static boolean isZipStream(InputStream in) {
		
	  final byte[] MAGIC = { 'P', 'K', 0x3, 0x4 };

	  if (!in.markSupported()) {
	     in = new BufferedInputStream(in);
	  }
	  
	  boolean isZip = true;
	  
	  try {
	    in.mark(MAGIC.length);
	    for (int i = 0; i < MAGIC.length; i++) {
	      if (MAGIC[i] != (byte) in.read()) {
	       isZip = false;
	       break;
	      }
	     }
	     in.reset();
	  } catch (IOException e) {
	     isZip = false;
	  }
	  return isZip;
	 }
	 
}