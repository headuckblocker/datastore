package com.headuck.datastore.instream;

import java.io.IOException;

public interface ICountProgressHandler {
		void onProgress (long byteRead) throws IOException, InterruptedException;
}
