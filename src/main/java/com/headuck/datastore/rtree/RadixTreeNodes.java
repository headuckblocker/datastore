package com.headuck.datastore.rtree;

/**
 * Class RadixTreeNodes is an implementation of Radix Tree nodes in byte buffers
 * for efficiency. 
 *
 */
public class RadixTreeNodes {
	// Initial buffer sizes
	// Sizes are enough for over 50,000 phone records to avoid reallocation
	static final int DEFAULT_NODEBUF_SIZE = 500000;
	static final int DEFAULT_STRBUF_SIZE = 120000;
	static final int DEFAULT_CHILDBUF_SIZE = 540000;

	private class ByteBuf {
		static final int INC_SIZE = 75000;
		private byte [] buf;
		private int allocSize;
		public int length;
		private final String name;

		public ByteBuf(int initsize, final String name) {
			buf = new byte[initsize];
			length = 0;  // len & next byte to write
			allocSize = initsize;
			this.name = name;
		}

		/**
		 * Allocate bytes at the end of the buffer, resize if necessary
		 * @param advlen length of allocation
		 */
		public void advance (final int advlen) {
			if (length + advlen >= allocSize) {
				realloc (INC_SIZE + advlen + length - allocSize);
			}
			length += advlen;
		}

		private void realloc (final int size) {
			allocSize += size;
			byte[] buf1 = new byte[allocSize];
			System.arraycopy(buf, 0, buf1, 0, buf.length);
			buf = buf1;
		}

		public byte getByte (final int idx) {
			return buf[idx];
		}

		public void setByte (final int idx, final byte val) {
			buf[idx] = val;
		}

		public boolean getBoolean (final int idx, final byte mask) {
			byte b = buf[idx];
		    return ((b & mask) != 0);
		}

		public void setBoolean (final int idx, final byte mask, final boolean value) {
			byte b = buf[idx];
		    if (value) {
		    	b = (byte) (b | mask);
		    } else {
		    	b = (byte) (b & (0xFF ^ mask));
		    }
		    buf[idx] = b;
		}

		// return unsigned value
		public int getShort (final int idx) {
			return ((int)(buf[idx] & 0xFF)) * 256 + ((int)(buf[idx + 1] & 0xFF));
		}

		public void setShort (final int idx, final int val) {
			if (val > 65535) {
				String msg = String.format("Buffer %s Overflow short value = %d\n", this.name, val);
				throw new RuntimeException("setShort Overflow: " + msg);
			}
			buf[idx] = (byte) (val >> 8);
			buf[idx+1] = (byte) (val & 0xFF);
		}

		// return value of unitSize bytes
		public int getEntry(final int idx, byte unitSize) {
			long retInt = 0;
			if (idx < 0) {
				String msg = String.format("Illegal idx in getEntry: %d", idx);
				throw new RuntimeException(msg);
			}
			for (int i = idx; i < idx + unitSize; i++) {
				retInt *= 256;
				retInt += (buf[i] & 0xFF);
			}
			if (retInt >= Integer.MAX_VALUE) {
				throw new RuntimeException("Integer Overflow in getEntry");
			}
			if (retInt < 0) {
				throw new RuntimeException("Negative Integer in getEntry");
			}
			return (int)retInt;
		}

		public void setEntry (final int idx, int val, byte unitSize) {
			if (val >= (1 << (unitSize * 8))) {
				String msg = String.format("Buffer %s Overflow entry value = %d\n", this.name, val);
				throw new RuntimeException("setEntry Overflow: " + msg);
			}
			for (int i = idx + unitSize - 1; i >= idx; i--) {
				buf[i] = (byte) (val & 0xFF);
				val >>>= 8;
			}
		}

		public byte[] getBuf(final int pos, final int len) {
			if (len == 0) return null;
			byte[] newbuf = new byte[len];
			System.arraycopy(buf, pos, newbuf, 0, len);
			return newbuf;
		}

		public void setBuf(final int pos, final byte[] value) {
			if (value.length == 0) return;
			System.arraycopy(value, 0, buf, pos, value.length);
		}

		// srcpos is offset into value. Substring (srcpos..end) is used.
		public void setBuf(final int pos, final byte[] value, final int srcpos) {
			int len = value.length - srcpos;
			if (len <= 0) return;
			System.arraycopy(value, srcpos, buf, pos, len);
		}
	}

	/*
	 * Node buffer
	 *  
     * Format for nodeBuf
     *    
     * Each node is a chunk of 8 bytes in ByteBuf
     * Fields format for each node:
     * Flag : 1 byte 
     * 	bit 7 (lsb): "real" - node represents valid string in trie
     * 	bit 6: terminal - node has no children. Must be real node
     *  bit 0-3: value associated with string.
     * Children: 3 bytes - pointer to childBuf, 0 represents null
     * Key: 2 bytes - pointer to strBuf 
     * Key Length: 2 bytes - length of the string
     */
    private ByteBuf nodeBuf = new ByteBuf(DEFAULT_NODEBUF_SIZE, "nodeBuf");
    // Next allocation position
    int nodeBufIdx = 0;
    final static int NODEBUF_UNIT_SIZE = 8;
    /*
     *  Offsets into fields
     */
    final static int NODEBUF_FLAG = 0;
    final static int NODEBUF_VAL = 0;
    final static int NODEBUF_KEY = 1;
	final static int NODEBUF_CHILDREN = 4;
	final static int NODEBUF_KEY_LEN = 7;

	final static int NODEBUF_CHILDREN_ENTRY_SIZE = 3;
	final static int NODEBUF_KEY_ENTRY_SIZE = 3;
	final static int NODEBUF_KEY_LEN_ENTRY_SIZE = 1;

    // Bit masks for flag
    final static byte NODEBUF_MASK_REAL = (byte)0x80;
    final static byte NODEBUF_MASK_TERM = (byte)0x40;
	final static byte NODEBUF_MASK_VAL = 0x0F;

    /*
	 * Child buffer
	 *  
     * Format for childBuf
     * 
     * Each unit is a chunk of 20 bytes (for num only trie) or (2 * maxChild) bytes.
     * Each with 10 / maxChild entries of 3 byte index to nodeBuf. 0 if null
     * 
     */
    private ByteBuf childBuf = new ByteBuf(DEFAULT_CHILDBUF_SIZE, "childBuf");
    // Next allocation position starts at 1, as 0 is reserved for null child
    private int childBufIdx = 1;
	final static int CHILDBUF_ENTRY_SIZE = 3;
	int childbufUnitSize;
	boolean numOnly;
    /*
     * Null buffer for clearance of child buffer
     */
	 byte[] emptyChildBuf;


    /*
     * String buffer
     * Used to store the keys 
     */
	private ByteBuf strBuf = new ByteBuf(DEFAULT_STRBUF_SIZE, "strBuf");
	public int maxChild = 10;
	/* strBuf is initialised to begin with '0...9' */

	/**
	 * Constructor for {@link RadixTreeNodes}
	 */
    public RadixTreeNodes () {
        this(true);
    }

	public RadixTreeNodes (boolean numOnly) {
		strBuf.advance(10);
		strBuf.setBuf(0, new byte[]
				{(byte)'0',(byte)'1',(byte)'2',(byte)'3',(byte)'4',
				(byte)'5',(byte)'6',(byte)'7',(byte)'8',(byte)'9'});
        if (numOnly) maxChild = 10;
        else maxChild = 12;
        this.numOnly = numOnly;
        childbufUnitSize = CHILDBUF_ENTRY_SIZE * maxChild;
        emptyChildBuf = new byte[childbufUnitSize];
	}

    /**
      * Allocate a new node
	  *
	  * @return integer reference to the new node
	  */
	public int newTreeNode() {
		final int nodeIdx = nodeBufIdx;
		nodeBuf.advance(NODEBUF_UNIT_SIZE);
		nodeBufIdx ++;
		return nodeIdx;
	}

    public byte getValue(final int nodeIdx) {
    	final int nodePtr = nodeIdx * NODEBUF_UNIT_SIZE;
    	return (byte)(nodeBuf.getByte(nodePtr+NODEBUF_VAL) & NODEBUF_MASK_VAL);
    }

	public void setValue(final int nodeIdx, final byte data) {
		final int nodePtr = nodeIdx * NODEBUF_UNIT_SIZE;
		byte val = (byte)(nodeBuf.getByte(nodePtr+NODEBUF_VAL) & (0xFF ^ NODEBUF_MASK_VAL));
    	nodeBuf.setByte(nodePtr+NODEBUF_VAL, (byte)(val | data));
	}


	public byte[] getKey(final int nodeIdx) {
		final int nodePtr = nodeIdx * NODEBUF_UNIT_SIZE;
		int pos = nodeBuf.getEntry(nodePtr+NODEBUF_KEY, (byte)NODEBUF_KEY_ENTRY_SIZE);
		int len = nodeBuf.getEntry(nodePtr+NODEBUF_KEY_LEN, (byte)NODEBUF_KEY_LEN_ENTRY_SIZE);
		return (strBuf.getBuf(pos, len));
	}

	public int getKeyLength(final int nodeIdx) {
		final int nodePtr = nodeIdx * NODEBUF_UNIT_SIZE;
        return nodeBuf.getEntry(nodePtr+NODEBUF_KEY_LEN, (byte)NODEBUF_KEY_LEN_ENTRY_SIZE);
	}

	/**
	 * Return a pointer into the string buffer for the key. <p/>
	 * {@link #getKeyCharAt} can then be used to get the key chars
	 *
	 * @param nodeIdx index to the node
	 * @return pointer to string buffer
	 */
	public int getKeyRef (final int nodeIdx) {
		final int nodePtr = nodeIdx * NODEBUF_UNIT_SIZE;
        return nodeBuf.getEntry(nodePtr+NODEBUF_KEY, (byte)NODEBUF_KEY_ENTRY_SIZE);
	}

	/**
	 * Get the pos-th byte of the string buffer pointed to by strBufIdx
	 *
	 * @param strBufIdx index into str buffer returned by {@link #getKeyRef(int)}
	 * @param pos position offset of the byte, starting from 0
	 * @return
	 */
	public byte getKeyCharAt (final int strBufIdx, final int pos) {
		return strBuf.getByte(strBufIdx + pos);
	}

	/**
	 * Set the key value of the node
	 *
	 * @param nodeIdx pointer to the node
	 * @param value key value
	 */
	public void setKey(final int nodeIdx, final byte[] value) {
		setKey(nodeIdx, value, 0);
	}

	/**
	 * Set the key value of the node. The key value to be used
	 * starts at an offset
	 *
	 * @param nodeIdx pointer to the node
	 * @param value byte buffer containing key value
	 * @param keypos position of the key value. value[keypos..end] is used as the key
	 */
	public void setKey(final int nodeIdx, final byte[] value, final int keypos) {
		// Save the string first
		final int len = value.length - keypos;
		int pos = -1;
		// Special handling for length = 1 & str is just one of the digits
		// point to the pre-set head of the string buffer instead of new allocation
		if (len == 1) {
		    int numVal = value[keypos] - (byte)'0';
		    if ((numVal >= 0) && (numVal <= 9)) {
				pos = numVal;
			}
		}
		if (pos == -1) {
            pos = strBuf.length;
            strBuf.advance(len);
			strBuf.setBuf (pos, value, keypos);
		}
		// save the pointer
		int nodePtr = nodeIdx * NODEBUF_UNIT_SIZE;
		nodeBuf.setEntry(nodePtr+NODEBUF_KEY, pos, (byte)NODEBUF_KEY_ENTRY_SIZE);
		nodeBuf.setEntry(nodePtr + NODEBUF_KEY_LEN, len, (byte)NODEBUF_KEY_LEN_ENTRY_SIZE);
	}

	/**
	 * Split the key of the node nodeIdx into two at position splitLength
	 * The node at headIdx (parent node) will get the head of the key
	 * The node at tailIdx (child node) will get the tail
	 * nodeIdx may be equal to either headIdx or tailIdx <p/>
	 *
	 * Use this method to split the key (instead of calling {@link #getKey(int)}
	 * and {@link #setKey(int, byte[])} to avoid new string allocation and
	 * creation of temp buffer
	 *
	 * @param nodeIdx index of the node key to split
	 * @param headIdx index of the node to get the head part of the key
	 * @param tailIdx index of the node to get the tail part of the key
	 * @param splitLength Key will be split as [0..splitLength-1] and [splitLength..originalLength]
	 * @return first byte of the tail split key (key[splitLength]), which can be
	 * 		   used for indexing of the tail node
	 */
	public byte splitKey (final int nodeIdx, final int headIdx, final int tailIdx, final int splitLength) {
		// Just move index and len around without assigning string buffer
		final int nodePtr = nodeIdx * NODEBUF_UNIT_SIZE;
		final int pos = nodeBuf.getEntry(nodePtr+NODEBUF_KEY, (byte)NODEBUF_KEY_ENTRY_SIZE);
		final int len = nodeBuf.getEntry(nodePtr+NODEBUF_KEY_LEN, (byte)NODEBUF_KEY_LEN_ENTRY_SIZE);
		assert ((len > splitLength) && (splitLength > 0));

		int headPtr = headIdx * NODEBUF_UNIT_SIZE;
		int tailPtr = tailIdx * NODEBUF_UNIT_SIZE;
		nodeBuf.setEntry(headPtr+NODEBUF_KEY, pos, (byte)NODEBUF_KEY_ENTRY_SIZE);
		nodeBuf.setEntry(headPtr+NODEBUF_KEY_LEN, splitLength, (byte)NODEBUF_KEY_LEN_ENTRY_SIZE);
		nodeBuf.setEntry(tailPtr+NODEBUF_KEY, pos + splitLength, (byte)NODEBUF_KEY_ENTRY_SIZE);
		nodeBuf.setEntry(tailPtr+NODEBUF_KEY_LEN, len - splitLength, (byte)NODEBUF_KEY_LEN_ENTRY_SIZE);
		return strBuf.getByte(pos + splitLength);
	}

    public boolean isReal(final int nodeIdx) {
    	final int nodePtr = nodeIdx * NODEBUF_UNIT_SIZE;
    	return nodeBuf.getBoolean(nodePtr + NODEBUF_FLAG, NODEBUF_MASK_REAL);
    }

	public void setReal(final int nodeIdx, final boolean realNode) {
		final int nodePtr = nodeIdx * NODEBUF_UNIT_SIZE;
    	nodeBuf.setBoolean(nodePtr+NODEBUF_FLAG, NODEBUF_MASK_REAL, realNode);
	}

    public boolean isTerm(final int nodeIdx) {
    	final int nodePtr = nodeIdx * NODEBUF_UNIT_SIZE;
    	return nodeBuf.getBoolean(nodePtr+NODEBUF_FLAG, NODEBUF_MASK_TERM);
    }

	public void setTerm (final int nodeIdx, final boolean termNode) {
		final int nodePtr = nodeIdx * NODEBUF_UNIT_SIZE;
    	nodeBuf.setBoolean(nodePtr+NODEBUF_FLAG, NODEBUF_MASK_TERM, termNode);
	}

    /**
     * Get the children at the idx-th entry, representing node starting with digit idx
     *
     * @param nodeIdx node pointer
     * @param idx index to the children
     * @return pointer to child node, 0 if null
     */
    public int getChildrenIdx(final int nodeIdx, final int idx) {
        final int nodePtr = nodeIdx * NODEBUF_UNIT_SIZE;
        final int childIdx = nodeBuf.getEntry(nodePtr + NODEBUF_CHILDREN, (byte)NODEBUF_CHILDREN_ENTRY_SIZE);
        if (childIdx == 0) {
            // Terminal node should not be called to get children!
            System.out.println("getChildrenIdx called for Terminal node");
            return 0;
        }
        final int childPtr = childIdx * childbufUnitSize;
        return childBuf.getEntry(childPtr + (idx * CHILDBUF_ENTRY_SIZE), (byte)CHILDBUF_ENTRY_SIZE);
    }

	/**
	 * Get the children indexed by chr
	 *
	 * @param nodeIdx node pointer
	 * @param chr character to be used as index
	 * @return pointer to child node, 0 if null
	 */
	public int getChildren(final int nodeIdx, final byte chr) {
		final int nodePtr = nodeIdx * NODEBUF_UNIT_SIZE;
		final int childIdx = nodeBuf.getEntry(nodePtr + NODEBUF_CHILDREN, (byte)NODEBUF_CHILDREN_ENTRY_SIZE);
		if (childIdx == 0) {
			// Terminal node should not be called to get children!
			System.out.format("getChildren called for Terminal node: %d\n", nodeIdx);
			throw new RuntimeException("get children is 0");
			// return 0;
		} else if (childIdx < 0) {
			System.out.format("Illegal childIdx: %d  nodePtr: %d\n", childIdx, nodePtr);
			return 0;
		}
		final int childPtr = childIdx * childbufUnitSize;
        final int idx = chrToIdx(chr);
		// System.out.format("getEntry call with childPtr %d idx %d\n", childPtr, idx);
		final int children = childBuf.getEntry(childPtr + (idx * CHILDBUF_ENTRY_SIZE), (byte)CHILDBUF_ENTRY_SIZE);
		if (children == nodeIdx && children != 0) {
			System.out.format("getChildren return the same node as parent: %d\n", nodeIdx);
			return 0;
		}
		return children;
	}

	/**
	 * Add a node as the children indexed by chr
	 *
	 * @param nodeIdx parent node pointer
	 * @param chr character to be used as index
	 * @param childNodeIdx child node pointer
	 */
	public void addChildren(final int nodeIdx, final byte chr, final int childNodeIdx) {
		final int nodePtr = nodeIdx * NODEBUF_UNIT_SIZE;
		int childIdx = nodeBuf.getEntry(nodePtr + NODEBUF_CHILDREN, (byte)NODEBUF_CHILDREN_ENTRY_SIZE);
		if (childIdx == 0) {
			// Create child idx node if necessary
			childIdx = childBufIdx;
			childBuf.advance(childbufUnitSize);
			childBufIdx ++;
			assert childIdx != 0;
			nodeBuf.setEntry(nodePtr+NODEBUF_CHILDREN, childIdx, (byte)NODEBUF_CHILDREN_ENTRY_SIZE);
		}
		final int idx = chrToIdx(chr);
		if (idx != -1) {
			final int childPtr = (childIdx * childbufUnitSize) + (idx * CHILDBUF_ENTRY_SIZE);
			// Check if already occupied, print warning if yes
			int currIdx = childBuf.getEntry(childPtr, (byte)CHILDBUF_ENTRY_SIZE);
			if (currIdx != 0) {
				// Existing child pointer should not be overwritten!
				System.out.println("setChildren is overwriting existing pointer");
			}
			childBuf.setEntry(childPtr, childNodeIdx, (byte)CHILDBUF_ENTRY_SIZE);
		} else {
			System.out.println("Invalid char to encode: " + (char) chr);
		}
	}

    // rule for mapping other chars to idx
    int chrToIdx (byte chr) {
        int idx;
        if ((chr >= (byte) '0') && (chr <= (byte) '9')) {
            idx = chr - (byte) '0';
        } else if (!numOnly) {
            if (chr == (byte) '*') {
                idx = 10;
            } else if (chr == (byte) '#') {
                idx = 11;
            } else {
                idx = -1;
            }
        } else {
            idx = -1;
        }
        return idx;
    }

	public void clearChildren(final int nodeIdx) {
		final int nodePtr = nodeIdx * NODEBUF_UNIT_SIZE;  
		final int childIdx = nodeBuf.getEntry(nodePtr+NODEBUF_CHILDREN, (byte)NODEBUF_CHILDREN_ENTRY_SIZE);
		if (childIdx == 0) {
			// clearing non-existent children			
			System.out.println("clearChildren call on node without children");
		} else {
			final int childPtr = childIdx * childbufUnitSize;
			childBuf.setBuf(childPtr, emptyChildBuf);
		}
	}
	  
	/**
	 * Move the child list from srcNodeIdx to nodeIdx
	 * Only the pointer to the child node is copied. 
	 * The pointer at srcNodeIdx will be cleared
	 * 
	 * @param nodeIdx target node 
	 * @param srcNodeIdx source node
	 */
	public void moveChildren(final int nodeIdx, final int srcNodeIdx) {
		final int nodePtr = nodeIdx * NODEBUF_UNIT_SIZE;  
		final int srcNodePtr = srcNodeIdx * NODEBUF_UNIT_SIZE;
		
		final int childIdx = nodeBuf.getEntry(srcNodePtr+NODEBUF_CHILDREN, (byte)NODEBUF_CHILDREN_ENTRY_SIZE);
		nodeBuf.setEntry(srcNodePtr+NODEBUF_CHILDREN, 0, (byte)NODEBUF_CHILDREN_ENTRY_SIZE);
		if (nodeBuf.getEntry(nodePtr+NODEBUF_CHILDREN, (byte)NODEBUF_CHILDREN_ENTRY_SIZE) != 0) {
			System.out.println("moveChildren overwrites existing children");
		}
		nodeBuf.setEntry(nodePtr+NODEBUF_CHILDREN, childIdx, (byte)NODEBUF_CHILDREN_ENTRY_SIZE);
	}

	/**
	 * Get number of matching characters between the key provided and the key in a node.
	 * 
	 * @param nodeIdx point to the node for matching
	 * @param key key value provided for matching
	 * @param keypos offset into key[]. Matching will be done on key[keypos..end]
	 * @return number of matching characters
     */
    public int getNumberOfMatchingCharacters(final int nodeIdx, final byte[] key, final int keypos) {
        final int nodePtr = nodeIdx * NODEBUF_UNIT_SIZE;
        final int pos = nodeBuf.getEntry(nodePtr+NODEBUF_KEY, (byte)NODEBUF_KEY_ENTRY_SIZE);
        final int len = nodeBuf.getEntry(nodePtr+NODEBUF_KEY_LEN, (byte)NODEBUF_KEY_LEN_ENTRY_SIZE);

        int numberOfMatchingCharacters = 0;

        while (numberOfMatchingCharacters < (key.length - keypos) && numberOfMatchingCharacters < len) {
            if (key[keypos + numberOfMatchingCharacters] != strBuf.getByte(pos + numberOfMatchingCharacters)) {
                break;
            }
            numberOfMatchingCharacters++;
        }
        return numberOfMatchingCharacters;
    }
	
	void printstat () {
		System.out.println ("RadixTreeNodes memory usage in bytes");
		System.out.println (" Node buffer: " + nodeBuf.length);
		System.out.println (" Child buffer: " + childBuf.length);
		System.out.println (" String buffer: " + strBuf.length);
	}
}
