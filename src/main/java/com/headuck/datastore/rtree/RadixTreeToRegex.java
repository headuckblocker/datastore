package com.headuck.datastore.rtree;

import java.util.SortedSet;
import java.util.TreeSet;

/**
 * Export a radix tree to regex form
 */
public class RadixTreeToRegex {
    /**
     * Convert two radix trees - first one for # without + and second one for +, to one regex string
     * @param radixTree rtree for non-'+' numbers
     * @param radixTreePlus rtree for '+' numbers
     * @return string
     */
    public static String toRegexWithPlus (RadixTree radixTree, RadixTree radixTreePlus) {
        if (radixTreePlus.getSize() == 0) {
            return toRegex(radixTree);
        }
        StringBuilder sb = new StringBuilder();
        RadixTreeNodes rt;
        int root;
        sb.append("(?:");
        if (radixTree.getSize() > 0) {
            rt = radixTree.getRadixTreeNodes();
            root = radixTree.getRadixTreeRoot();
            toRegex(rt, root, sb, rt.isReal(root), rt.isTerm(root));
            sb.append("|");
        }
        sb.append("\\+");
        rt = radixTreePlus.getRadixTreeNodes();
        root = radixTreePlus.getRadixTreeRoot();
        toRegex (rt, root, sb, rt.isReal(root), rt.isTerm(root));
        sb.append(")");
        return sb.toString();
    }

    public static String toRegex (RadixTree radixTree) {
        RadixTreeNodes rt = radixTree.getRadixTreeNodes();
        int root = radixTree.getRadixTreeRoot();
        StringBuilder sb = new StringBuilder();
        toRegex (rt, root, sb, rt.isReal(root), rt.isTerm(root));
        return sb.toString();
    }

    public static void toRegex (RadixTreeNodes rt, int node, StringBuilder sb, boolean real, boolean term) {
        int len = rt.getKeyLength(node);

        boolean [] isCharTerm = new boolean[rt.maxChild];
        boolean [] isStringTerm = new boolean[rt.maxChild];
        // For caching terminal childs
        TreeSet<Byte> termCharSet = new TreeSet<Byte>();
        boolean termStar = false;
        boolean termWildcard = false;
        boolean termCharOnly = true;

        int strBufIdx = rt.getKeyRef(node);
        int strBufLen = rt.getKeyLength(node);

        for (int i = 0; i < strBufLen; i++) {
            char chr = (char) rt.getKeyCharAt(strBufIdx, i);
            if (chr == (byte)'*') {
                sb.append("\\d*+");
            } else if (chr == (byte)'#') {
                sb.append("\\d");
            } else {
                sb.append(chr);
            }
        }

        if (!term) {
            // First pass to gather stat
            for (int childidx = 0; childidx < rt.maxChild; childidx++) {
                // System.out.println("getchild1");
                int child = rt.getChildrenIdx(node, childidx);
                if (child == 0) continue;


                if (!rt.isTerm(child)) {
                    termCharOnly = false;
                } else {
                    // Term
                    strBufIdx = rt.getKeyRef(child);
                    strBufLen = rt.getKeyLength(child);
                    if (strBufLen == 1) {
                        isCharTerm[childidx] = true;
                        byte chr = rt.getKeyCharAt(strBufIdx, 0);
                        if ((chr >= (byte) '0') && (chr <= (byte) '9')) {
                            termCharSet.add(chr);
                        } else if (chr == (byte) '*') {
                            termStar = true;
                            termCharOnly = false;
                        } else if (chr == (byte) '#') {
                            termWildcard = true;
                            termCharOnly = false;
                        } else {
                            // should not happen, ignore
                            System.out.println("Warning: unknown char in toRegex: " + chr);
                        }
                    } else {
                        isStringTerm[childidx] = true;
                        termCharOnly = false;
                    }
                    //childType[childIdx] = TYPE_TERM;
                    //childInfo[childIdx] = rt.getValue(child);
                }
            }


            // Second pass: append chars
            // Non-capturing group needed unless only char class terminal
            if (!termCharOnly) sb.append("(?:");
            boolean isFirstOption = true;
            // Char class
            int termCharSetSize = termCharSet.size();
            if (termCharSetSize > 0) {
                if (termCharSetSize == 10) {
                    // all digits: use \d
                    sb.append("\\d");
                } else {
                    if (termCharSetSize > 1) {
                        sb.append("[");
                    }
                    toCharRange(termCharSet, sb);
                    if (termCharSetSize > 1) {
                        sb.append("]");
                    }
                }
                isFirstOption = false;
            }
            // append term strings
            for (int childidx = 0; childidx < rt.maxChild; childidx++) {
                if (isStringTerm[childidx]) {
                    // System.out.println("getchild2");
                    int child = rt.getChildrenIdx(node, childidx);
                    if (child == 0) continue;
                    strBufIdx = rt.getKeyRef(child);
                    strBufLen = rt.getKeyLength(child);
                    if (!isFirstOption) {
                        sb.append('|');
                    } else {
                        isFirstOption = false;
                    }
                    for (int i = 0; i < strBufLen; i++) {
                        char chr = (char) rt.getKeyCharAt(strBufIdx, i);
                        if (chr == (byte) '*') {
                            sb.append("\\d*+");
                        } else if (chr == (byte) '#') {
                            sb.append("\\d");
                        } else {
                            sb.append(chr);
                        }
                    }

                }
            }
            for (int childidx = 0; childidx < rt.maxChild; childidx++) {
                if (!isStringTerm[childidx] && !isCharTerm[childidx]) {
                    // System.out.println("getchild3");
                    int child = rt.getChildrenIdx(node, childidx);
                    if (child == 0) continue;
                    if (!rt.isTerm(child)) {
                        if (!isFirstOption) {
                            sb.append('|');
                        } else {
                            isFirstOption = false;
                        }
                        toRegex(rt, child, sb, rt.isReal(child), rt.isTerm(child));
                    }
                }
            }
            // Append *
            if (termStar) {
                if (!isFirstOption) {
                    sb.append('|');
                } else {
                    isFirstOption = false;
                }
                sb.append("\\d*+");
            }
            if (termWildcard) {
                if (!isFirstOption) {
                    sb.append('|');
                } else {
                    isFirstOption = false;
                }
                sb.append("\\d");
            }
            if (!termCharOnly) sb.append(")");
            // If this node is real, mark child as optional
            if (real) {
                sb.append("?");
            }
        }
    }

    private static void toCharRange (SortedSet<Byte> termCharSet, StringBuilder sb) {
        byte rangeFirst = -1;
        byte rangeLast = -1;
        int rangeSize = 0;
        for (Byte b : termCharSet) {
            if (b == rangeLast + 1) {
                rangeLast = b;
                rangeSize ++;
            } else {
                if (rangeFirst != -1) {
                    if (rangeSize == 2) sb.append ((char)rangeLast);
                    if (rangeSize > 2) {
                        sb.append ('-');
                        sb.append ((char) rangeLast);
                    }
                }
                sb.append ((char)((byte) b));
                rangeFirst = b;
                rangeLast = b;
                rangeSize = 1;
            }
        }
        // Same code as inner loop flush
        if (rangeFirst != -1) {
            if (rangeSize == 2) sb.append ((char)rangeLast);
            if (rangeSize > 2) {
                sb.append ('-');
                sb.append ((char) rangeLast);
            }
        }
    }
}
