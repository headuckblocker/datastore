package com.headuck.datastore.rtree;

public class RadixTreeBufConst {
    static final int BUFFERLEN = 65536 * 6;  // length of buffer / max length

    static final int defaultBlockBitLength = 12;
    static final int longBlockBitLength = 20;
    static final int HEADER_FLAG_OFFSET = defaultBlockBitLength - 2;

    static final int SYMBOL_TERM = 0x0F;
    static final int SYMBOL_CONT = 0x0E;
}
