package com.headuck.datastore.rtree;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static com.headuck.datastore.rtree.RadixTreeBufConst.BUFFERLEN;
import static com.headuck.datastore.rtree.RadixTreeBufConst.HEADER_FLAG_OFFSET;
import static com.headuck.datastore.rtree.RadixTreeBufConst.SYMBOL_CONT;
import static com.headuck.datastore.rtree.RadixTreeBufConst.SYMBOL_TERM;
import static com.headuck.datastore.rtree.RadixTreeBufConst.defaultBlockBitLength;
import static com.headuck.datastore.rtree.RadixTreeBufConst.longBlockBitLength;

public class RadixTreeBufReader implements RadixTreeBufWritable {

    static final boolean DEBUG = false;

	static final byte[] NULL_BYTE_ARRAY = new byte[0];

    byte[] byteBuffer = null;
	final static Logger log = LoggerFactory.getLogger("RadixBufReader");

    private class ChildInfo {
    	// info for child node
    	public byte[] parentkey;
    	public boolean parentmatch;
    	public ChildInfoRecord[] child;
    }

    private class ChildInfoRecord {
        public boolean isInternal;
        public boolean isReal;     // child is real
        public char firstchar;     // first char, for prefix=null return
        public byte[] key; // remaining portion of key
        public int nodeOffset;   // offset for next internal node (if internal = true)
        public int info;         // info retrieved for terminal (if internal = false)
        // Source of info for update
        public BlockReader blockReader;   // BlockReader used to retrieve info
        public int infoOffset;            // offset of info, for index of blockReader
        public int infoPosBlock;          // # of 4-byte position block from start (msb) of buffer
    }

    /**
     * Search the buffer trie at byteBuffer for key 
     * @param key search key
     * @return info for matching node if found, -1 if not.
     */
    public int getTrieInfo (String key) {
    	if (byteBuffer == null) {
    		log.error("byteBuffer not initialised!");
    		return -1;
    	}
		ChildInfo cinfo = new ChildInfo();
        ChildInfoRecord cinfoRecord = new ChildInfoRecord();
        cinfoRecord.isReal = false;
        cinfoRecord.info = 0;
        cinfoRecord.nodeOffset = 0;
        cinfoRecord.blockReader = null;
        cinfo.child = new ChildInfoRecord[] {cinfoRecord};
        ChildInfoRecord cinfoRet = getTrieInfo(key, cinfo, 0);
		if (cinfoRet == null) {
			return -1;
		}
		return cinfoRet.info;
    }

	/**
	 * Search the buffer trie at byteBuffer for key, get the original value, and update with val
	 * @param key search key
	 * @param val byte value (lower 4 bit used)
	 * @return original value if found, -1 if not.
	 */
	public int putTrieInfo (String key, byte val) {
		if (byteBuffer == null) {
			log.error("byteBuffer not initialised!");
			return -1;
		}
		ChildInfo cinfo = new ChildInfo();
        ChildInfoRecord cinfoRecord = new ChildInfoRecord();
        cinfoRecord.isReal = false;
        cinfoRecord.info = 0;
        cinfoRecord.nodeOffset = 0;
        cinfoRecord.blockReader = null;
        cinfo.child = new ChildInfoRecord[] {cinfoRecord};
        ChildInfoRecord cinfoRet = getTrieInfo(key, cinfo, 0);
		if ((cinfoRet == null) || (cinfoRet.blockReader == null)) {
			return -1;
		}
		cinfoRet.blockReader.updateBlock(cinfoRet.infoOffset, cinfoRet.infoPosBlock, val);
		return cinfoRet.info;
	}


    /**
     * Search the buffer trie at byteBuffer for key, get the original value, and update with val
     * return original value if found, -1 if not.
     */
    public void visitTrieInfo (VisitCallback callback) {
        if (byteBuffer == null) {
            log.error("byteBuffer not initialised!");
            return;
        }
        ChildInfo cinfo = new ChildInfo();
        ChildInfoRecord cinfoRecord = new ChildInfoRecord();
        cinfoRecord.isReal = false;
        cinfoRecord.info = 0;
        cinfoRecord.nodeOffset = 0;
        cinfoRecord.blockReader = null;
        cinfo.child = new ChildInfoRecord[] {cinfoRecord};
        visitTrie(cinfo, 0, new SmallByteBuf(), callback);
    }

	/**
     * Read the trie from a file
     * @param filename path of the file to be read
     * @throws IOException
     */
    public void readTrie (String filename) throws IOException {  	
    	
    	File file = new File(filename);
    	
    	FileInputStream fis = null;
    	
    	try {
    		fis = new FileInputStream(file);  // Exception if not found
			readTrie(fis, (int) file.length());
    	} finally {
    		try {
    			if (fis != null) fis.close();
    		} catch (IOException e) {
                log.warn("Error while closing stream for "+ filename, e);
            }
    	}
    }

	/**
	 * Read the trie buffer from an input stream, with the number of bytes specified
	 * @param is input stream
	 * @param bufSize size of buffer to read from the stream
	 * @throws IOException
	 */
	public void readTrie (InputStream is, int bufSize) throws IOException {
		byteBuffer = new byte[bufSize];
		int bytesRead = is.read(byteBuffer);
		if (bytesRead != bufSize) {
			throw new IOException("Unexpected end of file while reading trie buffer.");
		}
	}

	/**
	 * Read the trie from a file
	 * @param filename path of the file to be read
	 * @return array of RadixTreeBufReader read
	 * @throws IOException
	 */
	public static RadixTreeBufReader[] readMultiTrie (String filename) throws IOException {

		File file = new File(filename);
		int numBuf;
		int [] bufSizes;
		RadixTreeBufReader [] rtBufReaders = null;
		FileInputStream fis = null;

		try {
			fis = new FileInputStream(file);  // Exception if not found
			byte [] magic = new byte[4];
			fis.read(magic);
			if ((magic[0] != (byte)'T') ||
				(magic[1] != (byte)'R') ||
				(magic[2] != (byte)'I') ||
				(magic[3] != (byte)'E')) {
				throw new IOException("Magic mismatch for MultiTrie buffer " + filename);
			}
			// Check version
			if (getIntFromStream(fis) != 1) {
				throw new IOException("Version mismatch for MultiTrie buffer " + filename);
			};
			// Number of buffers
			numBuf = getIntFromStream(fis);
			// Sanity check
			if (numBuf > 5) {
				throw new IOException("Number of buffer exceeds 5 : " + filename);
			}
			// Read buffer sizes
			bufSizes = new int [numBuf];
			for (int i = 0; i < numBuf; i++) {
				bufSizes[i] = getIntFromStream(fis);
				// sanity check
				if (bufSizes[i] > BUFFERLEN) {
					throw new IOException("Specified trie size too large : " + filename);
				}
			}
			// Read buffers
			rtBufReaders = new RadixTreeBufReader [numBuf];
			for (int i = 0; i < numBuf; i++) {
				rtBufReaders[i] = new RadixTreeBufReader();
                rtBufReaders[i].readTrie(fis, bufSizes[i]);
			}
		} finally {
			try {
				if (fis != null) fis.close();
			} catch (IOException e) {
				log.warn("Error while closing stream for "+ filename, e);
			}
		}
		return rtBufReaders;
	}

	/**
	 * Read 4 bytes from an input byte stream and convert to integer
	 */
	private static int getIntFromStream (InputStream is) throws IOException {
		int v = 0;
		for (int i = 0; i < 4; i ++) {
			v = (v << 8) + is.read();
		}
		return v;
	}

    /**
     * Recursively visit the buffer trie at byteBuffer until matching prefix is found
     * @param prefix search string from the current offset (node)
     * @param parentCinfo
	 * parentCinfo.isReal : internal node pointed to by offset is real
	 * parentCinfo.info : value
	 * parentCinfo.nodeOffset : offset in unit of defaultBlockBitLength) of internal node in buffer
     *
     * @return cinfo where cinfo.info is the 4-bit value for matching node if found, null if not.
     */
    private ChildInfoRecord getTrieInfo(final String prefix, final ChildInfo parentCinfo, int idx) {
        
    	// TODO: Check prefix validity

    	ChildInfo cinfo = getChildInfo (parentCinfo.child[idx].nodeOffset, prefix);

    	if (cinfo == null) {  // no match
    		return null;
    	}
    	
    	if (cinfo.parentmatch) {
    		// internal node exact match
    		if (parentCinfo.child[idx].isReal) {
	            // System.out.format("Found: %d%n", cinfo.info);
	            return parentCinfo.child[idx];  // parentCinfo.child[idx].info is the needed value
    		} else {
    			return null;
    		}
    	}
  	
    	if (!cinfo.child[0].isInternal) {
    		// Terminal node - matched
    		return cinfo.child[0];  // cinfo.info is the needed value;
    	} else {
    		// Internal node children w/ 1st char matching so far
    		String newText = prefix.substring(cinfo.parentkey.length + 1);
    		return getTrieInfo(newText, cinfo, 0);
    	}
    }

    public interface VisitCallback {
        int onVisit (SmallByteBuf key, int val);
    }
    /**
     * Recursively visit the buffer trie at byteBuffer and returns lists
     * @param parentCinfo
     * parentCinfo.isReal : internal node pointed to by offset is real
     * parentCinfo.info : value
     * parentCinfo.nodeOffset : offset in unit of defaultBlockBitLength) of internal node in buffer
     *
     * @return cinfo where cinfo.info is the 4-bit value for matching node.
     */
    private void visitTrie(final ChildInfo parentCinfo, int idx, SmallByteBuf prefix, final VisitCallback callback) {

        ChildInfo cinfo = getChildInfo(parentCinfo.child[idx].nodeOffset, null);

        if (cinfo == null) {  // should not happen
            System.out.print("Rtrie traverse returns null.");
            return;
        }

		int mark = prefix.length();

        prefix.append (cinfo.parentkey);

		/* if (prefix.toString().startsWith("38") && prefix.length<4) {
			int offset = parentCinfo.child[idx].nodeOffset;
			long header[] = new long[3];
			int flag[] = new int[3];

			System.out.println("---" + prefix.toString());
			System.out.println("node offset =" + parentCinfo.child[idx].nodeOffset);

			int i = 0;
			do {
				header[i] = getBlock(offset+i);
				flag[i] = (int) (header[i] >> HEADER_FLAG_OFFSET);
				System.out.println("header " + i + ":" + header[i]);
				System.out.println("flag " + i + ":" + flag[i]);
				i ++;
			} while ((i < 3) && (flag[i-1] == 3));

			for (i=0; i<cinfo.child.length; i++) {
				System.out.println("first char :" + cinfo.child[i].firstchar);
				System.out.println("node offset :" + cinfo.child[i].nodeOffset);
				System.out.println("info offset :" + cinfo.child[i].infoOffset);
			}
		} */

        if (parentCinfo.child[idx].isReal){
            // Output a terminal
            final ChildInfoRecord cinfoRecord = parentCinfo.child[idx];
            final int ret = callback.onVisit(prefix, cinfoRecord.info);
            // System.out.format("%s %d%n", newPrefix, parentCinfo.child[idx].info);
            if (ret != -1) {
                cinfoRecord.blockReader.updateBlock(cinfoRecord.infoOffset, cinfoRecord.infoPosBlock, (byte) ret);
            }
        }

		int mark1 = prefix.length();

        for (int i=0; i<cinfo.child.length; i++) {
			prefix.resetLength(mark1);
            if (!cinfo.child[i].isInternal) {
                // Terminal node - matched
                //System.out.format("%s %d%n", newPrefix + cinfo.child[i].firstchar + cinfo.child[i].key
                //, cinfo.child[i].info);
                // cinfo.child[i];  // cinfo.info is the needed value;
                final ChildInfoRecord cinfoRecord = cinfo.child[i];
				prefix.append((byte) cinfoRecord.firstchar);
				prefix.append(cinfoRecord.key);
                final int ret = callback.onVisit(prefix, cinfoRecord.info);
                if (ret != -1) {
                    cinfoRecord.blockReader.updateBlock(cinfoRecord.infoOffset, cinfoRecord.infoPosBlock, (byte) ret);
                }
            } else {
                // Internal node children w/ 1st char matching so far
				prefix.append ((byte)cinfo.child[i].firstchar);
				visitTrie(cinfo, i, prefix, callback);
            }
        }

		prefix.resetLength(mark);
    }

    /**
     * Return child info beginning with digit 'c' under the node at offset
     * @param offset offset (in unit of defaultBlockBitLength) for the buffer
     * @param prefix trailing string to be matched, or null for returning all children
     * @return ChildInfo if prefix is not null: child [0] is the matching child found, null if not found,
     *                   if prefix is null child[] is the array of all children
     */
    ChildInfo getChildInfo (final int offset, final String prefix) {
    	// Return value
    	ChildInfo cinfo = new ChildInfo();

    	long header[] = new long[3];
    	int flag[] = new int[3];
    	
    	// format / status variables
    	boolean nodeLen1 = false;
    	boolean isFar = false;
    	boolean splitmap = false; // split map, childnum = 2
    	boolean splitmap3 = false; // split map, childnum = 3

		int mode = -1;
    	long mapInt = 0;
    	long mapTerm = 0;
    	long map1;
    	int headerBlockLen = -1;
    	
    	final long MAP_MASK = ((long)((1<<10) - 1));
    	
    	// offset & length
    	int offset2 = 0;
    	
    	int idx = 0;
    	do {
    		header[idx] = getBlock(offset+idx);
    		flag[idx] = (int) (header[idx] >> HEADER_FLAG_OFFSET);
    		idx ++;
    	} while ((idx < 3) && (flag[idx-1] == 3));
    	int headerLen = idx; 
    	
    	
    	// decode format / status
    	if (flag[0] != 3) {
    		nodeLen1 = true;  // key len of this int node is 1, i.e. no extra key
    		mode = flag[0];    		
    	}
    	map1 = header[0] & MAP_MASK;
    	
    	// Special cases
    	if ((flag[0] == 1) && (map1 == 0)) {
    		isFar = true;
    		splitmap = true;
    	} else if ((flag[0] == 2) && (map1 == 0)) {
			isFar = true;
			splitmap = true;
			splitmap3 = true;
		}
    	
    	if (headerLen > 1) {
    		    	
	    	if (headerLen == 2) {
	    		if (flag[1] == 1) {
	    			mode = (int)((header[1] >> 8) & 3);
	    			if (mode == 3) isFar = true;
	    		} else {
	    			// two maps
	    			if ((flag[1] == 0) || (flag[1] == 2)) {
	    				nodeLen1 = true;
	    				if (flag[1] == 2) isFar = true;
	    			}
	    		}  
	    	}
	    	    	
	    	if (headerLen == 3) {
	    		if (flag[2] == 2) isFar = true;
	    	}
	    	
	    	if (mode == -1) {
	    		// two maps
		    	mapTerm = map1;
	    		mapInt = header[1] & MAP_MASK;
	    	}
    	}
    	
    	int readOffset;
    	// read the key for this node 
    	// 
    	if (!nodeLen1) {
    		// start read from position 1, last header block
    		readOffset = offset + headerLen - 1;
    		long buf = getBlock (readOffset);
    		byte[] key = readKeyFromLong (buf, 1, defaultBlockBitLength);
    		readOffset ++;
    		while (key == null) {
				long buf1 = getBlock(readOffset);
				key = readKeyFromLongCont (buf1, defaultBlockBitLength);
				readOffset ++;
			}
    		headerLen = readOffset - offset;
    		cinfo.parentkey = key;
            // For given prefix,
            // return if exact matches / no prospect for further matches
            if (prefix != null) {
                // match prefix passed in
                if ((prefix.length() == 0) && (key.length == 0)) {
                    cinfo.parentmatch = true;  // fully match parent
                    return cinfo;
                } else if (prefix.length() == 0) {
                    return null;
                } else if (prefix.startsWith(new String(key))) {     // expensive call, used only for search
                    if (prefix.length() == key.length) {
                        cinfo.parentmatch = true;  // fully match parent
                        return cinfo;
                    } else {
                        cinfo.parentmatch = false;
                    }
                } else {
                    return null;
                }
            }
    	} else {
    		cinfo.parentkey = NULL_BYTE_ARRAY;
            // For given prefix,
            // return if exact matches / no prospect for further matches
            if (prefix != null) {
                if (prefix.length() == 0) {
                    cinfo.parentmatch = true;  // fully match parent
                    return cinfo;
                }
                cinfo.parentmatch = false;
            }
    		readOffset = offset + headerLen;
    	}

        String remainingPrefix = null;

    	
    	int extrafieldLength = 0;
    	//headerBlockLen = readOffset - offset;
    	// For far block get the pointer
    	
    	// handle the case for single map, except for splitmap
    	int childnum = 0;

    	if (!splitmap) { 
    		if (mode != -1) {
	    		if ((mode == 1) || (mode == 3)) {
	    			// the map refers to internal node 
	    			mapInt = map1;
	    		} else {
	    			mapTerm = map1;
	    		}	
    		}
    		childnum = countBit (mapInt | mapTerm);
    	}
    	if (isFar) {
    		if (!splitmap) {
    			int longchildtermlen = longBlockBitLength - defaultBlockBitLength; // 8 bit
    			// read longchildterm    			
    			extrafieldLength = (int) getBlock(readOffset, longchildtermlen);    			
    			// get original headerBlockLen
    			headerBlockLen = headerLen + childnum + extrafieldLength;
    			// if len = 1 and no terminal child, one extra header block was allocated afterwards 
    			// (i.e. extrahead=true in writer). Deduct it for calculating offset  
    			if (nodeLen1 && (mapTerm == 0)) headerBlockLen --;    			
    			// read offset 2
    			offset2 = (int) getBlockBitOffset(readOffset, longchildtermlen, longBlockBitLength);
    			// System.out.format ("get offset2: %d headerlen %d %n", offset2, headerBlockLen);
    		} else {
        		// special case: split map
    			// no longchildterm stored in this case
    			offset2 = (int) getBlock(readOffset, longBlockBitLength);
				// splitmap is true only for childnum = 2 or 3
    			if (splitmap3) {
    				// child num = 3, otherwise 2
					headerBlockLen = headerLen + 3;
				} else {
					// deduce the length for the first block
					// ignoring any space for internal node info
					headerBlockLen = headerLen + 2;
				}

    			// System.out.format ("get offset2: %d headerlen %d %n", offset2, headerBlockLen);
    			// get the split map.      			
    			mapInt = getCompositeBlock (offset, headerBlockLen, offset2, // buffer config
    					headerLen, 1, // offset short block, long block
    					defaultBlockBitLength);
    			mapInt &= MAP_MASK;
    			childnum = countBit (mapInt);
    		}
    	}

        // childnum is final here
        // Allocate child records according to mode
        if (prefix == null) {
            cinfo.child = new ChildInfoRecord[childnum];
        } else {
            cinfo.child = new ChildInfoRecord[1];
        }

    	// get whether the char match and the type of match if any

        int childidx = -1;
        final long allmap = mapInt | mapTerm;

        if (prefix != null) {

            char c = prefix.charAt(cinfo.parentkey.length);

            if (prefix.length() > cinfo.parentkey.length + 1) {
                remainingPrefix = prefix.substring(cinfo.parentkey.length + 1);
            } else {
                remainingPrefix = "";
            }

            long currMask = 1 << (c - '0');

            cinfo.child[0] = new ChildInfoRecord();
            cinfo.child[0].firstchar = c;
            if ((currMask & mapInt) != 0) {
                cinfo.child[0].isInternal = true;
            } else if ((currMask & mapTerm) != 0) {
                cinfo.child[0].isReal = true;
            } else {
                // no match, return null
                return null;
            }

            // check order in map
            childidx = 0;
            // number of less significant bit set
            // is equivalent to idx into child table
            while (currMask > 0) {
                currMask >>= 1;
                if ((allmap & currMask) != 0) {
                    childidx++;
                }
            }
        }
   	
    	if (mode == 0) {
            BlockReader blockReader = new BlockReader (false, false, offset, 0, 0, headerLen);
            if (prefix != null) {
                return (mode0result(cinfo, 0, blockReader, childidx));
            } else {
                int recordIdx = 0;
                for (int i = 0; i <= 9 ; i++) {
                    final long currMask = 1 << i;
                    if ((currMask & allmap) == 0) continue;
                    cinfo.child[recordIdx] = new ChildInfoRecord();
                    cinfo.child[recordIdx].firstchar = (char)('0' + i);
                    if ((currMask & mapInt) != 0) {
                        cinfo.child[recordIdx].isInternal = true;
                    } else if ((currMask & mapTerm) != 0) {
                        cinfo.child[recordIdx].isReal = true;
                    } else {
                        // should not reach here
                    }
                    mode0result(cinfo, recordIdx, blockReader, recordIdx);
                    recordIdx ++;
                }
                return cinfo;
            }

    	}
    	
    	final int bitLen;
    	if (isFar) {
    		bitLen = longBlockBitLength;        		
    	} else {
    		bitLen = defaultBlockBitLength;
    	}    
    	
    	BlockReader blockReader = new BlockReader (isFar, splitmap, 
    			offset, headerBlockLen, offset2, headerLen);

        long tmpbuf;
    	// For prefix != null, allow shortcut returns if looking into the entry itself is suffice
    	// i.e. no extended child key & no internal node info
        if (prefix != null) {
            tmpbuf = blockReader.getBlock(childidx);
            if (cinfo.child[0].isInternal) {
                int intNodeFlag = (int) (tmpbuf >> (bitLen - 2));
                cinfo.child[0].nodeOffset = (int) (tmpbuf & ((long) (1 << (bitLen - 2)) - 1));
                if (!isFar) {
                    cinfo.child[0].nodeOffset += offset;
                }
                if (intNodeFlag == 0) {
                    // not a real node, no info
                    return cinfo;
                } else {
                    if (intNodeFlag != 2) {
                        log.warn("Internal node flag != 2, buffer probably in wrong format. flag = " + intNodeFlag + " prefix = " + prefix);
                    }
                    // child is also a real node, need to retrieve info
                    cinfo.child[0].isReal = true;
                }
            } else {
                // Terminal
                cinfo.child[0].info = (int) (tmpbuf >> (bitLen - 4));
                // Save the origin address of info -
                cinfo.child[0].blockReader = blockReader;
                cinfo.child[0].infoOffset = childidx;
                cinfo.child[0].infoPosBlock = 0;

                // test if the entry ends with a continuation char
                if ((tmpbuf & 0x0F) != SYMBOL_CONT) {
                    // if not
                    cinfo.child[0].key = readKeyFromLong(tmpbuf, 1, bitLen);
                    if (remainingPrefix.length() == 0) {
                        if (cinfo.child[0].key.length == 0) {
                            // matched
                            return cinfo;
                        }
                    } else {
                        if (remainingPrefix.equals(new String(cinfo.child[0].key))) {  // expensive
                            // matched
                            return cinfo;
                        }
                    }
                    return null;
                }
            }
        }
    	
    	// either multiblock child or int node info involved 
    	// scan children
    	
    	// long valuebuf[] = new long [childnum];
    	int idxmask = 1;
    	int idxnum = 0;  // number represented by current child entry
    	// accounting for internal real node
    	int intRealNodeCount = 0;
    	int intRealNodePos = -1;
    	// accounting for terminal node
    	int longchildnumidx = childnum;
    	for (idx = 0; idx < childnum; idx ++) {
    		// check if the current idx is terminal
    		while ((idxnum < 10) && ((idxmask & allmap) == 0)) {
    			idxmask <<= 1;
    			idxnum ++;    			
    		}
    		if (DEBUG && !(idxnum != 10)) {
    			throw new AssertionError("Index num should not be 10");
			}
    		
    		tmpbuf = blockReader.getBlock(idx);

            if (prefix == null) {
                // fill in fields previously missed
                cinfo.child[idx] = new ChildInfoRecord();
                cinfo.child[idx].firstchar = (char)('0' + idxnum);
            }

    		if ((idxmask & mapInt) != 0) {
                if (prefix == null) {
                    // fill in fields previously missed
                    cinfo.child[idx].isInternal = true;
                    cinfo.child[idx].nodeOffset = (int) (tmpbuf & ((long) (1 << (bitLen - 2)) - 1));
                    if (!isFar) {
                        cinfo.child[idx].nodeOffset += offset;
                    }
                }
    			// internal node
    			int intNodeFlag = (int)(tmpbuf >> (bitLen - 2)); 
        		if (intNodeFlag == 2) {
                    // child is also a real node, need to retrieve info
                    if (prefix == null) {
                        // fill in fields previously missed
                        cinfo.child[idx].isReal = true;
                        int infoOffset = intRealNodeCount / (bitLen / 4);
                        int infopos = intRealNodeCount % (bitLen / 4);
                        // Save the origin address of info -
                        cinfo.child[idx].blockReader = blockReader;
                        cinfo.child[idx].infoOffset = infoOffset; // to add longchildnumidx later
                        cinfo.child[idx].infoPosBlock = infopos;

                    } else {
                        // Record the position among all real int node
                        // if applicable
                        if (idx == childidx) {
                            intRealNodePos = intRealNodeCount;
                        }
                    }
        			intRealNodeCount++;
        		}
    		} else if ((idxmask & mapTerm) != 0) {
                if (prefix == null) {
                    cinfo.child[idx].isReal = true;
                    // Terminal
                    cinfo.child[idx].info = (int) (tmpbuf >> (bitLen - 4));
                    // Save the origin address of info -
                    cinfo.child[idx].blockReader = blockReader;
                    cinfo.child[idx].infoOffset = idx;
                    cinfo.child[idx].infoPosBlock = 0;

                    // test if the entry ends with a continuation char
                    if ((tmpbuf & 0x0F) != SYMBOL_CONT) {
                        // the case if not ends with symbol_cont
                        cinfo.child[idx].key = readKeyFromLong(tmpbuf, 1, bitLen);
                    }
                }
    			// terminal node
    			if ((tmpbuf & 0x0F) == SYMBOL_CONT) {
        			// if not    				
        			byte[] key = readKeyFromLong (tmpbuf, 1, bitLen);
        			while (key == null) {
        				long tmpbuf1 = blockReader.getBlock(longchildnumidx);
        				key = readKeyFromLongCont (tmpbuf1, bitLen);
        				longchildnumidx ++;
        			}
                    if (prefix != null) {
                        if (idx == childidx) {
                            cinfo.child[0].key = key;
                            if (remainingPrefix.length() == 0) {
                                if (cinfo.child[0].key.length == 0) {
                                    // matched
                                    return cinfo;
                                }
                            } else {
                                if (remainingPrefix.equals(new String(cinfo.child[0].key))) {  // expensive
                                    // matched
                                    return cinfo;
                                }
                            }
                            return null;
                        }
                    } else {
                        cinfo.child[idx].key = key;
                    }
        		}
    		} else {
                // should not happen
            }
			idxmask <<= 1;
			idxnum ++; 
    	}
    	
    	if (intRealNodePos >= 0) {
    		// Retrieve info on term node
    		int infoOffset = intRealNodePos / (bitLen / 4);
    		int infopos = intRealNodePos % (bitLen / 4);
    		tmpbuf = blockReader.getBlock(longchildnumidx + infoOffset);    		
    		tmpbuf >>= bitLen - 4 - infopos*4;			
    		tmpbuf &= 0x0F;
    		cinfo.child[0].info = (int) tmpbuf;
			// Save the origin address of info -
			cinfo.child[0].blockReader = blockReader;
			cinfo.child[0].infoOffset = longchildnumidx + infoOffset;
			cinfo.child[0].infoPosBlock = infopos;

    		return cinfo;
    	} else if (prefix == null) {
            for (idx = 0; idx < childnum; idx ++) {
                if (cinfo.child[idx].isReal && cinfo.child[idx].isInternal) {
                    cinfo.child[idx].infoOffset += longchildnumidx;
                    tmpbuf = blockReader.getBlock(cinfo.child[idx].infoOffset);
                    tmpbuf >>= bitLen - 4 - cinfo.child[0].infoPosBlock * 4;
                    tmpbuf &= 0x0F;
                    cinfo.child[idx].info = (int) tmpbuf;
                }
            }
        } else {
    		assert false;
    	}
    	if (prefix == null) {
            return cinfo;
        }
    	return null;
    }

    private ChildInfo mode0result (ChildInfo cinfo, int recordidx, BlockReader blockReader, int childidx) {
        // short terminal form
        int infoOffset = childidx / (defaultBlockBitLength / 4);
        int infopos = childidx % (defaultBlockBitLength / 4);
        long infoblock = blockReader.getBlock(infoOffset);
        infoblock >>= defaultBlockBitLength - 4 - infopos * 4;
        infoblock &= 0x0F;
        cinfo.child[recordidx].info = (int) infoblock;
        // Save the origin address of info -
        cinfo.child[recordidx].blockReader = blockReader;
        cinfo.child[recordidx].infoOffset = infoOffset;
        cinfo.child[recordidx].infoPosBlock = infopos;
        cinfo.child[recordidx].key = NULL_BYTE_ARRAY;
        return cinfo;
    }

    /**
     * Convenience class to store config info of buffer   
     * and read the child index table accordingly
     *
     */
    private class BlockReader {
    	boolean isFar;
    	boolean splitmap;
    	int offset, offset2; 
    	int headerBlockLen, headerLen;
    	
    	public BlockReader (boolean isFar, boolean splitmap, int offset, int headerBlockLen, int offset2, int headerLen) {
    		this.isFar = isFar;
    		this.splitmap = splitmap;
    		this.offset = offset;
    		this.headerBlockLen = headerBlockLen;
    		this.offset2 = offset2;
    		this.headerLen = headerLen;    		
    	}
    	
    	public long getBlock (int idx) {   		
    		if (isFar) {
    			return getCompositeBlock (offset, headerBlockLen, offset2, // buffer config
    				headerLen + (splitmap? 1: -1), (splitmap? 1: 2) + idx, // offset short block, long block
    				longBlockBitLength);
    		} else {
    			return RadixTreeBufReader.this.getBlock (offset + headerLen + idx);
    		}
    	}

		/**
		 * Update the 4 bit val at address idx + pos (bitblock offset)
		 *
		 * @param idx index used to retrieve the value
		 * @param pos bitblock offset
		 * @param val 4 bit val to be written
		 */
		public void updateBlock (int idx, int pos, byte val) {
			if (isFar) {
				updateCompositeBlock (offset, headerBlockLen, offset2, // buffer config
						headerLen + (splitmap? 1: -1), (splitmap? 1: 2) + idx, // offset short block, long block
						pos, val);
			} else {
				RadixTreeBufReader.this.updateBlockBitBlockOffset (offset + headerLen + idx, pos, val);
			}
		}
    }
    
    /**
     * Count the number of '1' bits in the digit map
     * @param bits map to be counted
     * @return number of '1' bits 
     */
    public int countBit (long bits) {    	
    	int mask = 1;
    	int count = 0;
    	for (int i = 0; i < 10; i++) {
    		if ((bits & mask) != 0) count ++;
    		mask <<= 1;
    	}
    	return count;
    }
    
    /**
     * State variables used by {@link #readKeyFromLong} 
     * and {@link #readKeyFromLongCont}
	 * This makes the object non-thread safe
     */
	SmallByteBuf readKeyTempString = new SmallByteBuf ();
    int countLoop;
    
    /**
     * Decode digit string stored in blocks
     * Decode by first calling this function with the first block
     * and subsequently calling {@link #readKeyFromLongCont}
     * with the remaining blocks
     *    
     * @param buf first block to be decoded 
     * @param position position to start decoding 
     * @param blockBitLength length of a block in bits
     * @return string decoded as byte[], or null if more input needed
     */
    byte[] readKeyFromLong (long buf, int position, int blockBitLength) {
    	readKeyTempString.reset();
    	countLoop = 0;
    	
    	int bitpos = position * 4;
    	int digit;
    	boolean cont = false;
    	
    	while (bitpos < blockBitLength) {
    		// get digit
    		digit = (int) (buf >> ((blockBitLength - bitpos) - 4));
    		digit &= 0x0F;    		
    		if (digit == SYMBOL_TERM) {
    			// String completed
    			return (readKeyTempString.getByteCopy());
    		} else if (digit == SYMBOL_CONT) {
    			if (DEBUG && !(bitpos + 4 == blockBitLength)) {
    				throw new AssertionError("bitpos error");
				}
    			cont = true;
    			break;
    		} else {
    			if (digit > 9) {
    				log.warn("Invalid digit, buffer probably in wrong format");
    				return null;
    			}
	    		readKeyTempString.append ((byte)(digit + '0'));
	    		bitpos += 4;
    		}
    	}
    	if (!cont) {
    		return (readKeyTempString.getByteCopy());
    	}
    	return null;
    }
    
    /**
     * Decode digit string stored in blocks
     * Decode by first calling {@link #readKeyFromLong} with the first block
     * and subsequently calling this function
     * with the remaining blocks
     *    
     * @param buf one block to be decoded 
     * @param blockBitLength length of a block in bits
     * @return decoded string in byte[], or null if more input needed
     *         if this is called 10 times in a row without 
     *         reaching termination of string, "" is returned 
     *         to avoid looping
     */
    byte[] readKeyFromLongCont (long buf, int blockBitLength) {
    	int bitpos = 0;
    	int digit;
    	countLoop ++;
    	
    	while (bitpos < blockBitLength) {
    		// get digit
    		digit = (int) (buf >> ((blockBitLength - bitpos) - 4));
    		digit &= 0x0F;    		
    		if (digit == SYMBOL_TERM) {
    			// String completed
    			return (readKeyTempString.getByteCopy());
    		} else {
    			if (DEBUG && !(digit <= 9)) {
    				throw new AssertionError("Digit > 9");
				}
	    		readKeyTempString.append ((byte)(digit + '0'));
	    		bitpos += 4;
    		}
    	}    	
    	if (countLoop >= 10) {
			log.warn("Key too long, buffer probably in wrong format");
    		return NULL_BYTE_ARRAY;
    	}
    	return null;
    }
		
    /**
     * Get one block from internal byte [] buffer, using defaultBlockLength
     * @param offset start block address, at defaultBlockBitLength
     * @return the block retrieved
     */
    long getBlock (int offset) {
    	return getBlock (offset, defaultBlockBitLength);
    }

    /**
     * Get one block from internal byte [] buffer, using specified blocklength
     * @param offset start block address, at defaultBlockBitLength
     * @param blockBitLength length of the block to retrieve
     * @return the block retrieved
     */
    long getBlock (int offset, int blockBitLength) {
    	return getBlockBitOffset(offset, 0, blockBitLength);
    }
    
    /**
     * Get one block from internal byte [] buffer, using specified blocklength
     * @param offset start block address, at defaultBlockBitLength
     * @param bitOffset addition offset shift in bits 
     * @param blockBitLength length of the block to retrieve
     * @return the block retrieved
     */
    long getBlockBitOffset(int offset, long bitOffset, int blockBitLength) {
		// convert start block to bit / 4
		long startBitBlock = offset * (defaultBlockBitLength / 4);
		startBitBlock += bitOffset / 4;
		if ((bitOffset % 4) != 0) {
			log.warn("Invalid bitOffset {} provided in getBlockBitOffset", bitOffset);
		}

		int bitBlockUnit = blockBitLength / 4;
		
    	int byteOffset = (int) (startBitBlock / 2);    	
    	int mod = (int) (startBitBlock % 2);

		long bitbuf = 0;
		for (int i = 0; i < bitBlockUnit; i++) {
			bitbuf <<= 4;
			// get 4 bit from startBitBlock
			if (mod == 1) {
				bitbuf |= byteBuffer[byteOffset] & 0x0F;
				byteOffset ++;
			} else {
				bitbuf |= (byteBuffer[byteOffset] >> 4) & 0x0F;
			}
			mod = 1 - mod;
		}
		return bitbuf;
	}
    
    /**
     * Get block from composite buffers
     * @param offset 1st start block address (in defaultBlockBitLength)
     * @param blockLen length of 1st start block
     * @param offset2 2nd start block address (in defaultBlockBitLength)
     * @param defBlockOffset offset in number of defaultBlockBitLength
     * @param longBlockOffset offset in number of longBlockBitLength.
     *        The total offset is the sum of the bits represented by defBlockOffset 
     *        and longBlockOffset
     * @param blockBitLength length of a block in buffer
     */
    long getCompositeBlock (
    		int offset, int blockLen, int offset2, // buffer config
    		int defBlockOffset, int longBlockOffset,// offset short block, long block
			int blockBitLength) {
    	
    	final long block1BitLen = blockLen * (defaultBlockBitLength / 4);
    	
    	// calculate total offset in units of bit / 4
		final long currBitOffset = defBlockOffset * (defaultBlockBitLength / 4) +
    			longBlockOffset * (longBlockBitLength / 4);
    	
    	// Split value buffer to write to the two blocks   	    	
		final int bitBlockUnit = blockBitLength / 4;

		long bitbuf = 0;
    	for (int i=0; i<bitBlockUnit; i++) {
    		bitbuf <<= 4;
    		if ((currBitOffset + i) < block1BitLen) {
    			bitbuf |= (int) getBlockBitOffset(offset, (currBitOffset + i) * 4, 4);		
    		} else {
    			//System.out.format("Using block 2 at %d, %d (%d) \n", offset2, currBitOffset+i, block1BitLen);
    			bitbuf |= (int) getBlockBitOffset(offset2, (currBitOffset + i - block1BitLen) * 4, 4);    			
    		}
    	}    
    	return bitbuf;
    }

	/**
	 * Update 4-bit bit block in buffer
	 * @param offset start block address, at defaultBlockBitLength
	 * @param bitBlockOffset further offset, in unit of 4-bits from offset parameter
	 * @param val byte value (4 bit) to be written
	 */
	void updateBlockBitBlockOffset(final int offset, final long bitBlockOffset, final byte val) {
		// convert start block to bit / 4
		final long startBitBlock = offset * (defaultBlockBitLength / 4) + bitBlockOffset;

		final int byteOffset = (int) (startBitBlock / 2);
		final int mod = (int) (startBitBlock % 2);

		if (mod == 1) {
			byteBuffer[byteOffset] = (byte) (byteBuffer[byteOffset] & 0xF0 | (val & 0x0F));
		} else {
			byteBuffer[byteOffset] = (byte) (byteBuffer[byteOffset] & 0x0F | (val << 4));
		}
	}

	/**
	 * Update 4-bit bit block in composite buffers
	 * @param offset 1st start block address (in defaultBlockBitLength)
	 * @param blockLen length of 1st start block
	 * @param offset2 2nd start block address (in defaultBlockBitLength)
	 * @param defBlockOffset offset in number of defaultBlockBitLength
	 * @param longBlockOffset offset in number of longBlockBitLength.
	 *        The total offset of start pos is the sum of the bits represented by defBlockOffset
	 *        and longBlockOffset
	 * @param pos further bit block offset
	 * @param val byte value (4 bit) to be written
	 */
	void updateCompositeBlock (int offset, int blockLen, int offset2, // buffer config
							   int defBlockOffset, int longBlockOffset,// offset short block, long block
	                           int pos, byte val) {

		final long block1BitLen = blockLen * (defaultBlockBitLength / 4);

		// calculate total offset in units of bit / 4
		final long currBitOffset = defBlockOffset * (defaultBlockBitLength / 4) +
				longBlockOffset * (longBlockBitLength / 4);

		if ((currBitOffset + pos) < block1BitLen) {
			updateBlockBitBlockOffset(offset, currBitOffset + pos, val);
		} else {
			//System.out.format("Using block 2 at %d, %d (%d) \n", offset2, currBitOffset+i, block1BitLen);
			updateBlockBitBlockOffset(offset2, currBitOffset + pos - block1BitLen, val);
		}
	}

	/** {@link RadixTreeBufWritable} interface */

	/**
	 * Return length of trie buffer in bytes
	 * @return buffer size
	 */
	@Override
	public int getBufSize() {
		return byteBuffer.length;
	}

	/**
	 * Write the trie byte buffer to the output stream
	 * @param os output stream to write to
	 * @throws IOException
	 */
	@Override
	public void writeTrie (OutputStream os) throws IOException {
		os.write(byteBuffer, 0, getBufSize());
	}

	public class SmallByteBuf {
		static final int INC_SIZE = 50;
		private byte [] buf;
		private int allocSize;
		public int length;

		public SmallByteBuf() {
			buf = new byte[INC_SIZE];
			length = 0;  // len & next byte to write
			allocSize = INC_SIZE;
		}

		/**
		 * Allocate bytes at the end of the buffer, resize if necessary
		 * @param advlen length of allocation
		 */
		public void advance (final int advlen) {
			if (length + advlen >= allocSize) {
				realloc (INC_SIZE + advlen + length - allocSize);
			}
			length += advlen;
		}

		private void realloc (final int size) {
			allocSize += size;
			byte[] buf1 = new byte[allocSize];
			System.arraycopy(buf, 0, buf1, 0, buf.length);
			buf = buf1;
		}

		public void append (final byte ch) {
			int pos = length;
			advance(1);
			buf[pos] = ch;
		}

		public void append (final byte[] ch) {
			int pos = length;
			advance(ch.length);
			System.arraycopy(ch, 0, buf, pos, ch.length);
		}

		public byte getByteAt (final int idx) {
			if ((idx >= length) || (idx < 0)) {
				throw new ArrayIndexOutOfBoundsException("Invalid position for getByteAt SmallByteBuf");
			}
			return buf[idx];
		}

		public byte[] getByteCopy () {
			byte[] buf1 = new byte[length];
			System.arraycopy(buf, 0, buf1, 0, length);
			return buf1;
		}

		public void reset() {
			length = 0;
		}

		public void resetLength(int length) {
			if ((length <= this.length) && (length >= 0)) {
				this.length = length;
			} else {
				throw new RuntimeException("Invalid length, must be smaller or equal to current length");
			}
		}

		public int length() {
			return length;
		}

		@Override
		public String toString() {
			return new String(getByteCopy());
		}
	}

}
