package com.headuck.datastore.rtree;

/**
 * Implementation for the creation of Radix tree for compression of digits (phone numbers)
 * Strings are stored in ASCII format in byte arrays. No charset conversion is done.
 */

/*
 * The radix tree creation algorithm is based upon the implementation of the radixtree library
 * http://code.google.com/p/radixtree/
 * 
 * Authors of the original radixtree class:
 * @author Tahseen Ur Rehman (tahseen.ur.rehman {at.spam.me.not} gmail.com)
 * @author Javid Jamae 
 * @author Dennis Heidsiek
 * 
 * Original copyright notice included below  
 * 
 */

/*
The MIT License

Copyright (c) 2008 Tahseen Ur Rehman, Javid Jamae

http://code.google.com/p/radixtree/

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/


/**
 * Implementation for Radix tree using byte buffer for the nodes
 * 
 */
public class RadixTree  {
	static public final int MAXKEYLEN = 20;
		
	RadixTreeNodes rt;
	protected int root;
    protected long size;
    protected long sizenode;
    
    /**
     * Create a Radix Tree with only the default node root.
     */
    public RadixTree() {
    	rt = new RadixTreeNodes();
        root = rt.newTreeNode();        
        rt.setTerm(root, true);
        
        size = 0;
        sizenode = 0;
    }
    public RadixTree(boolean numOnly) {
        rt = new RadixTreeNodes(numOnly);
        root = rt.newTreeNode();
        rt.setTerm(root, true);

        size = 0;
        sizenode = 0;
    }
    public void find(byte[] key) {
    	visit(key, 0, root);  
    }

    /**
     * Insert the key in the radix tree from the root
     */
    public void insert(final byte[] key, final byte value) {
        insert(key, 0, root, value);		
        size++;
    }

    /**
     * Insert the key in the radix tree from the root, starting at pos of the byte[] key
     *
     */
    public void insert(final byte[] key, final byte value, final int pos) {
        insert(key, pos, root, value);
        size++;
    }

    /**
     * Recursively insert the key in the radix tree from specified node and position.
     * 
     * @param key The key to be inserted
     * @param pos starting position of the bytebuf key[] to be compared
     * @param node The current node
     * @param value The value associated with the key 
     * 
     */
    private void insert(final byte[] key, final int pos, final int node, final byte value) {

        int numberOfMatchingCharacters = rt.getNumberOfMatchingCharacters(node, key, pos);
        // we are either at the root node
        // or we need to go down the tree
        if (node == root || numberOfMatchingCharacters == 0 || 
        		(numberOfMatchingCharacters < (key.length - pos) 
        				&& numberOfMatchingCharacters >= rt.getKeyLength(node))) {
            boolean flag = false;
            
            if (!rt.isTerm(node)) {
	            int child = rt.getChildren(node, key[pos + numberOfMatchingCharacters]);
	            if (child > 0) {
	            	flag = true;
	            	insert(key, pos + numberOfMatchingCharacters, child, value);
	            }
            }

            // just add the node as the child of the current node
            if (flag == false) {
            	int n = rt.newTreeNode();                
                sizenode ++;
                rt.setKey(n, key, pos + numberOfMatchingCharacters);                
                rt.setReal(n, true);
                rt.setValue(n, value);
                rt.setTerm(n, true);
                
                rt.addChildren(node, key[pos + numberOfMatchingCharacters], n);
                rt.setTerm(node, false);
            }
        }
        // there is a exact match just make the current node as data node
        else if (numberOfMatchingCharacters == (key.length - pos) && numberOfMatchingCharacters == rt.getKeyLength(node)) {
            if (rt.isReal(node) == true) {
            	System.out.format  ("Duplicate key: %s%n", key);
            } else {            
            	rt.setReal(node,true);
            }
            rt.setValue(node,value);
        }
        // This node need to be split as the key to be inserted
        // is a prefix of the current node key
        else if (numberOfMatchingCharacters > 0 && numberOfMatchingCharacters < rt.getKeyLength(node)) {
        	final int n1 = rt.newTreeNode();
            sizenode ++;
            
            final byte n1key0 = rt.splitKey(node, node, n1, numberOfMatchingCharacters);
            
            rt.setReal(n1,rt.isReal(node));
            rt.setValue(n1,rt.getValue(node));
            rt.moveChildren(n1, node);  // This also clears the children of node 
            rt.setTerm(n1,rt.isTerm(node));

            rt.setReal(node,false);            
            rt.addChildren(node, n1key0, n1);
            rt.setTerm(node,false);
            
            if(numberOfMatchingCharacters < (key.length - pos)) {
	            int n2 = rt.newTreeNode();
                sizenode ++;

                rt.setKey(n2, key, numberOfMatchingCharacters + pos);
                rt.setReal(n2,true);
                rt.setValue(n2,value);
                rt.setTerm(n2,true);
                rt.addChildren(node, key[numberOfMatchingCharacters + pos], n2);
            } else {
                rt.setValue(node,value);
                rt.setReal(node,true);
            }
        }        
        // this key need to be added as the child of the current node
        else {
        	final int n = rt.newTreeNode();
            sizenode ++;

            rt.moveChildren(n, node);
            rt.setReal(n, rt.isReal(node));
            rt.setValue(n, rt.getValue(node));
            rt.setTerm(n, rt.isTerm(node));            

            final byte nkey0 = rt.splitKey(node, node, n, numberOfMatchingCharacters);
            rt.setReal(node, true);
            rt.setValue(node, value);

            rt.addChildren(node, nkey0, n);
            rt.setTerm(node, false);            
        }
    }


    /**
     * Recursively visit the tree based on the supplied "key". 
     * Search for the node those key matches the given prefix
     * 
     * @param prefix
     *            Byte array containing the prefix for searching
     * @param keypos 
     *            Position in the prefix to start searching (i.e. search for prefix[keypos..end])
     * @param node
     *            The Node from where onward to search
     * @return -1 if no match, node if match found
     */
    private int visit(byte[] prefix, int keypos, int node) {
        
    	int numberOfMatchingCharacters = rt.getNumberOfMatchingCharacters(node, prefix, keypos);

        // if the node key and prefix match, we found a match!
        if (numberOfMatchingCharacters == (prefix.length - keypos) && numberOfMatchingCharacters == rt.getKeyLength(node)) {
        	if (rt.isReal(node)) {
                System.out.format("Internal node match: %s%n", new String(prefix));
                return node;
        	}
        } else if (node == root // either we are at the root
                || (numberOfMatchingCharacters < (prefix.length - keypos) 
                && numberOfMatchingCharacters >= rt.getKeyLength(node))) { // OR we need to
            // traverse the children
            assert !rt.isTerm(node);
            int child = rt.getChildren(node, prefix[keypos + numberOfMatchingCharacters]);
            if (child > 0) {
                // recursively search the child nodes
            	return visit(prefix, keypos + numberOfMatchingCharacters, child);
            }
        }
        return -1;
    }

    public long getSize() {
        return size;
    }
    
    public long getNodeSize() {
        return sizenode;
    }
    
    public RadixTreeNodes getRadixTreeNodes() {
    	return rt;
    }
    
    public int getRadixTreeRoot() {
    	return root;
    }
}

