package com.headuck.datastore.rtree;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Radixtree Writeable interface
 * for RadixTreeBufWriter.writeMultiTrie
 * Created by headuck
 */
public interface RadixTreeBufWritable {

    /**
     * Get the size (in byte) of the radix tree buffer to be written
     * @return size of the buffer in bytes
     */
    int getBufSize ();

    /**
     * Write the trie byte buffer to the output stream
     * @param os output stream to write to
     * @throws IOException
     */
    void writeTrie (OutputStream os) throws IOException;

}
