package com.headuck.datastore.rtree;

// For stat only
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map.Entry;

import static com.headuck.datastore.rtree.RadixTreeBufConst.BUFFERLEN;
import static com.headuck.datastore.rtree.RadixTreeBufConst.HEADER_FLAG_OFFSET;
import static com.headuck.datastore.rtree.RadixTreeBufConst.SYMBOL_CONT;
import static com.headuck.datastore.rtree.RadixTreeBufConst.SYMBOL_TERM;
import static com.headuck.datastore.rtree.RadixTreeBufConst.defaultBlockBitLength;
import static com.headuck.datastore.rtree.RadixTreeBufConst.longBlockBitLength;

public class RadixTreeBufWriter implements RadixTreeBufWritable {

	static final boolean DEBUG = false;

	RadixTreeNodes rt;	// Internal nodes	
	int root;			// reference to root node of radix tree (normally 0)
	byte[] byteBuffer;	// internal buffer for output of the RadixTreeBuf
	final static Logger log = LoggerFactory.getLogger("RadixBufWriter");

	/*
     * HashMaps for storing statistics 
     */ 
    HashMap<Integer, Integer> countReal;
    HashMap<Integer, Integer> countOther;
    HashMap<Integer, Integer> countChild;
    boolean stat = false;
	
	public RadixTreeBufWriter (RadixTree radixTree) {
		this.rt = radixTree.getRadixTreeNodes();
		this.root = radixTree.getRadixTreeRoot();
		byteBuffer = new byte[BUFFERLEN];
	}
		
    public void initStat () {
    	stat = true;
    	countReal = new HashMap<Integer, Integer>();
        countOther = new HashMap<Integer, Integer>();
        countChild = new HashMap<Integer, Integer>();
    }
    
    public void encodeTrie () {
    	encodeTrie(root);
    }
    
    protected void encodeTrie (int node) {
    	int len = rt.getKeyLength(node);
    	
    	// Get stat of child nodes first
    	int childnum = 0;       // # child
    	int childterm = 0;      // # child which is terminal
    	int childterm1 = 0;		// # child which is terminal & length=1
    	int longchildterm = 0;
    	int longchildterm20 = 0;
    	int intNodeInfoCount = 0;    	
  	        
        long mapTerm = 0;
        long mapInt = 0;
        long currMask;
        boolean isFar = false;
        
    	// save starting address
        int startBlock = block;
        int[] childref = new int[10];  // reference to child

		// byte childnumChar[] = new byte[10];
		assert !rt.isTerm(node);
    	for (byte chr = '0'; chr <= '9'; chr++)   {
    		int child = rt.getChildren(node, chr);
    		if (child == 0) continue;

    		currMask = 1L << (chr - (byte)'0');
        	
    		childref[childnum] = child;
    		// childnumChar[childnum] = chr;
            childnum++;

            if (rt.isTerm(child)) {
            	childterm++;
            	mapTerm |= currMask;
            	int childlen = rt.getKeyLength(child);
            	if (childlen == 1) childterm1++;
            	// First digit of the child is implicit 
            	// Calculate additional blocks needed for encoding the key of the child 
            	// under two scenarios
            	// (a) Additional block for default block bit length (12 bits)
            	//     (encoding starts at 4th bit (reserve most-significant 4 bits for info))
           	    longchildterm += lengthBitEncode(1, childlen-1); // 4-5: + 1 block; 6-8 + 2 blocks
           	    
            	// (b) Additional block for long block bit length (20 bits), should far reference be needed
            	longchildterm20 += lengthBitEncode(1, childlen-1, longBlockBitLength);// 6-9: +1 block;  10-14: +2 blocks 
            	
    			// Count stat
            	if (stat) {
	        		if (countReal.containsKey(childlen))
	        			countReal.put(childlen, countReal.get(childlen) + 1);
	        		else
	        			countReal.put(childlen, 1);
            	}
        		
            } else {
            	mapInt |= currMask;
            	// For each real internal node, 4 bits of info need to be stored 
            	if (rt.isReal(child)) {
            		intNodeInfoCount ++;
            	}
            }
            
        }

    	// Allocation for this node, 
    	// pointers to internal nodes, and terminal children
    	// one or two block for header (block type + child map)
    	// + one for each children pointer / terminal node
    	// Except when all children are terminal & length=1, in such case
    	// one block for 5 children

        lastStartBlock = block;

    	// internal node header
		// 2 blocks for len > 1 / mixed / real with star suffix
    	// Only 1 block for the internal node header needed 
    	// under the following scenario  
    	      // no overflowing of internal node label
    			// no mixed internal / term nodes
    	    	
		int extraBlock = 0;

		// first digit is implicit in trie
		if (len > 1) { 
			// Encode (len - 1) digits starting at 2nd block, 4th bit 
			extraBlock += 1 + lengthBitEncode (1, len-1); 
			// len: 2-3: 1 block; 4-6: 2 block 
		}
		if (((childnum - childterm) > 0) && (childterm > 0)) {
			// uses 2 maps
			extraBlock += 1;
		}
		// stat
		totalExtraBlock += extraBlock;
		// provisionally allocate header block. 
    	allocate (1 + extraBlock);

    	// Allocate child pointer / info blocks
    	int headerBlockLen; 
    	if (childnum == childterm1) {
    		// if all child are terminal with len=1, 4 bit for each child
    		headerBlockLen = bitToBlock(childnum * 4); 
    	} else {
    		// else one block each, plus the overflow blocks for each terminal child
    		headerBlockLen = childnum + longchildterm;  
    		// plus blocks for internal node info, 4 bits each
    		headerBlockLen += bitToBlock(intNodeInfoCount*4);
    	} 
    	int fieldLength = headerBlockLen;
    	allocate(fieldLength);

    	headerBlockLen += 1 + extraBlock; 
    			
    	int childType[]=  new int [childnum];
    	final int TYPE_INT = 1;
    	final int TYPE_TERM = 2;

    	// Fields for non-terminal child
    	long childvalue[] = new long [fieldLength]; 
    	int intChildFlag[] =  new int [childnum];
    	
    	// Field for terminal child, eventually merge to childvalue
    	int childInfo[] =  new int [childnum];
    	     	
    	
    	int lastChildBlock = -1;
    	
    	for (int childIdx=0; childIdx < childnum; childIdx++)   {
    		
    		int child = childref[childIdx];
    				
    		if (!rt.isTerm(child)) { 
    			childType[childIdx] = TYPE_INT;
    			childvalue[childIdx] = block;
    			lastChildBlock = block;
    			
    			if (rt.isReal(child)) {
    				intChildFlag[childIdx] = 2; // 10
    				childInfo[childIdx] = rt.getValue(child);
    			} else {
    				intChildFlag[childIdx] = 0; // 10
    				childInfo[childIdx] = -1;
    			}
    			/* String childCode = new StringBuilder(thisCode) .append( (char) childnumChar[childIdx]).toString();
    			if (childCode.startsWith("38") && childCode.length() < 4) {
    				System.out.println("Block Addr of " + childCode + " = " + block);
				}
    			encodeTrie(child, childCode);
				if (childCode.startsWith("38") && childCode.length() < 4) {
					System.out.println("  Afterwards, block Addr ->" + block);
				} */
				encodeTrie(child);
    	    	
    		} else {
    			childType[childIdx] = TYPE_TERM;
    			childInfo[childIdx] = rt.getValue(child);

    		}
    	}
    	
    	// If any block address is far away, switch to use absolute reference
    	// with 20 bits per block ("far" mode). 
    	// Additional allocation is necessary for the additional info
    	// First block after header act as pointer to new chunk
    	
    	int startBlockEx = -1;
    	int extraBlockEx = 0;
    	boolean splitmap = false;
    	boolean extrahead = false;

    	// Pointer to block: 1st bit (msb) 0 if not real, 1 if real
    	// test if more than 10 bits needed (Top 2 bits reserved)
        if (lastChildBlock - startBlock >= (((long)1) << (defaultBlockBitLength-2))) {
        	isFar = true;
        	// allocate new block
        	// save starting address
        	startBlockEx = block;
        	if ((len <= 1) && (childterm == 0)) {
        		// need an extra header map block 
				extrahead = true;			
				if (DEBUG && !(childnum > 1)) {
					throw new AssertionError("childnum <= 1");
				}
				if (childnum == 2 || childnum == 3) {
					splitmap = true;
				}
			}
        	extraBlockEx +=        			
        			// total additional space required for far encoding, expressed in default block length
        			// (including one long block for pointing to address of new block)
        			bitToBlock ((childnum + longchildterm20 + 1) * longBlockBitLength
        					// plus 8 bit space to save (fieldLength - childnum) except when split map is true
        					+ (splitmap? 0: (longBlockBitLength - defaultBlockBitLength)))
					// space shifted by extra header map for len == 1
					+ (extrahead? 1 : 0)
					// space for internal node info
					+ bitToBlock(intNodeInfoCount*4, longBlockBitLength)
        			// minus existing blocks already allocated, but in case
					// of split map, the space allocated for internal node info is not deduced
					// and remain unused, as there is no space for saving the count info. 
					// (At most wasted 1 block)
        			- (splitmap? (childnum + longchildterm) : fieldLength);
    			
        	allocate(extraBlockEx); 
        	// Stat
        	farcount ++;
        }

        // Write header for this node
        // Format:
    	// 1st block: 
    	// first 2 bits:  
    	//  00: node len=1; all child are terminal w/len 1  (4 bit)
    	//  01: node len=1; all child are short int node (12 bit)
    	//  10: node len=1; all child are short terminal & some w/ len > 1 (12 bit)
    	//  11: see second 2 bits of 2nd block.
		//  special case: 1st byte = 0 if no child (for empty root)
    	// second 2 bits if [11] above
        //  00: len = 1; 2 short maps (prev map is for short term node; following map are short int node)
    	//  01[XY]: len > 1; 1 map (overflow at this block)
    	//  10: len = 1; 2 maps; far node ref following 
    	//  11: see second 2 bit of 3rd block
    	//     XY: same interpretation as 1st block for 1 map
        //     if (11): children are far int node
    	// third 2 bits if [11] above
    	//  00: len > 1; 2 short maps; overflow at this block    	
    	//  10: len > 1;2 maps; far node ref; overflow (if any) at this block  
    	//
        // for len=1 & children are far int node, additional header block is needed
        // (if map has > 2 entries) 
        //     map of first block = 11 + (map) 0
        //     second block: 10 + far map
        // 	   second overlapping block (last 8 bit): saved longchildterm
        // (if map has 2/3 entries, the far map has to be placed in the new
        //  block to reserve space (20 bit) for the pointer => splitmap )
        //     map of first block = 01 + map (0)
        //     second long block = 00 + next chunk addr 
        // 	   followed by split map block (short) (which starts with 10 + map)
        // (To avoid reallocation & shifting)
        // * The case of far block should not happen if there is only 1 entry 
        // 
    	
    	long header[] =  new long [1+extraBlock+1];  // reserve 1 more block for special case 
    	long flag[] =  new long [1+extraBlock+1];
    	long mode;
        int headerLen;
        
    	
    	// 1st block
		if (childnum == 0) {
			// Special case: no child
			mode = 0;
			header[0] = 0;
		} else if (childterm != 0) {
    		header[0] = mapTerm;
    		if (childnum != childterm) {
    			header[1] = mapInt;
    			mode = -1; // 2 maps 
    		} else {
    			// All children are terminal
    			if (childnum == childterm1) {
    				// All are of len 1
    				mode = 0;  // 00
    			} else {
    				mode = 2;  // 10
    			}
    		}
    	} else {
    		if (!isFar || (len > 1)) {
	    		// All children are internal
	    		header[0] = mapInt;
	    		if (!isFar) {
	    			mode = 1;  // 01
	    		} else {
	    			mode = 3;  // 11 (len > 1 & far)
	    		}
    		} else {
    			// special case of len=1 & far & all int node; treat as 2 maps
    			header[0] = 0;
				header[1] = mapInt;
				flag[1]= 2; // 10
				if (DEBUG && !extrahead) {
					throw new AssertionError("extrahead");
				}
				
    			if (splitmap) {
    				// write one maps for header[0], header[1]/flag[1] processed later
					if (childnum == 2) {
						mode = 1;
					} else {
						mode = 2;
					}
    			} else {
    				// treat as two maps
    				mode = -1;    				
    			}
    		}
    	}
    	if (len <= 1) {
    		if (mode != -1) {
    			// one map
    			flag[0] = mode;
    			headerLen = 1; // 
    		} else {
    			// two map
    			flag[0] = 3;
    			if (isFar) {
    				flag[1] = 2;
    			} else {
    				flag[1] = 0;
    			}
    			headerLen = 2;
    		}
    	} else {
    		// len > 1
    		flag[0] = 3;
    		if (mode != -1) {
    			// one map
    			flag[1] = 1;
    			header[1] = mode << 8; 
    			//     			    		
    			// second byte, 2nd pos
    			writeKeyToBuf (header, 1, 1, node, 1);
    			headerLen = 2 + lengthBitEncode(1, len-1);
    		} else {
    			flag[1] = 3;
    			if (isFar) {
    				flag[2] = 2;
    			} else {
    				flag[2] = 0;
    			}    			
    			// third byte, 2nd pos 
    			writeKeyToBuf (header, 2, 1, node, 1);
    			headerLen = 3 + lengthBitEncode(1, len-1);
    		}

    	}
   	    	
    	if (!extrahead || splitmap) {     	
    		if (DEBUG && !(headerLen == 1+extraBlock)) {
    			throw new AssertionError("headerLen mismatch");
			}
    	} else {
    		if (DEBUG && !(headerLen == 1+extraBlock+1)) {
				throw new AssertionError("headerLen mismatch - extrahead && splitmap");
			}
    	}
    	// write flags
    	for (int i = 0; i < headerLen; i++) {
        	header[i] |= flag[i] << HEADER_FLAG_OFFSET;
    	}
    	        	
    	writeBlock (startBlock, header, headerLen);
    	
    	
    	if (isFar) {
    		
        	// Before writing pointer, save original extra variable allocation  
    		// (longchildterm  + buffer for internal real nodes)
    		// such that the reader can calculate the length of first block chunk
    		// 8 bit is used for the purpose. This is equivalent to promoting a short block to a long one in header
    		// no need to write for splitmap: there may not be enough space
    		// assume no block allocated (at most wasted 1 block)
    		if (!splitmap) {
	    		long longchildtermbuf[] = new long[1];
	    		longchildtermbuf[0] = fieldLength - childnum;
	    		int longchildtermlen = longBlockBitLength - defaultBlockBitLength; // 8 bit
				if (DEBUG && !(longchildterm < (1<<longchildtermlen))) {
					throw new AssertionError("longchildtermlen overflow");
				}
	    		writeBlock (startBlock + headerLen, longchildtermbuf, 1, longchildtermlen);
    		} else {
    			// skip the block reserved for internal node info in case of splitmap
    			headerBlockLen -= bitToBlock(intNodeInfoCount*4);
    		}
    		
        	// Write pointer to new block for far

    		long pointer[] = new long[1];
    		pointer[0] = startBlockEx;
    		if (!splitmap) {
    			writeCompositeBlock (
        			startBlock, headerBlockLen, startBlockEx,  // buffer config 
        			headerLen-1, 1, // offset short block, long block
        			pointer, 1,  // long data to write
        			longBlockBitLength);
    			
    			// System.out.format("pointer: %d;  headerBlockLen %d%n", pointer[0], headerBlockLen);
    			// System.out.format("pos:%d %d %d%n",startBlock, headerLen-1, 1);
    		} else {
				/* if (DEBUG1) {
					if (childnum == 3) {
						// if childnum is 3 get bit 20 to 1
						pointer[0] |= (long) 1 << (longBlockBitLength - 1);
					}
					System.out.println("Written splitmap pointer = " + pointer[0] + " at " + startBlock + headerLen);
				} */
    			writeBlock (startBlock + headerLen, pointer, 1, longBlockBitLength);
    			// System.out.format("pointer(split): %d;  headerBlockLen %d%n", pointer[0], headerBlockLen);
    		}
        	
        	// write the split map if needed
        	if (splitmap) {
            	// write map in header[1]        	
	        	long splitheader[] = new long[1];
	        	splitheader[0] = header[1] | flag[1] << HEADER_FLAG_OFFSET;
	        	writeCompositeBlock (
	        			startBlock, headerBlockLen, startBlockEx,  // buffer config 
	        			headerLen, 1, // offset short block, long block
	        			splitheader, 1,  // short data to write
	        			defaultBlockBitLength);
        	}
    	}
   

        // Write child info
    	if (childnum == childterm1) {
    		long buf = 0;
    		long bufBitLen = 0;
    		int valueIdx = 0;
    		for (int idx = 0; idx < childnum; idx ++) {
    			buf <<= 4;
    			buf |= childInfo[idx];
    			bufBitLen += 4;
    			if (bufBitLen == defaultBlockBitLength) {
    				childvalue[valueIdx] = buf;
    				valueIdx ++;
    				buf = 0;
    				bufBitLen = 0;
    			}    			
    		}
    		if (bufBitLen > 0) {
    			buf <<= (defaultBlockBitLength - bufBitLen);
    			childvalue[valueIdx] = buf;
    			valueIdx ++;
    		}
    		if (DEBUG && !(bitToBlock(childnum * 4) == valueIdx)) {
    			throw new AssertionError("valueIdx mismatch");
			}
    		writeBlock (startBlock + headerLen, childvalue, valueIdx);
    	} else {
        	final int bitLen;
        	        	
        	if (isFar) {
        		bitLen = longBlockBitLength;        		
        	} else {
        		bitLen = defaultBlockBitLength;
        	}    
        	
        	int longChildTermIdx = childnum;
	        for (int idx = 0; idx < childnum; idx ++) {
	
	        	if (childType[idx] == TYPE_INT) {
	        		if (!isFar) {
	        			// use relative idx
	        			childvalue[idx] -= startBlock;
	        		}
	        		// Add flag to most significant 2 bits
	        		if (DEBUG && !((childvalue[idx] & (3 << (bitLen - 2))) == 0)) {
	        			throw new AssertionError("flag error");
					}
	        		childvalue[idx] |= intChildFlag[idx] << (bitLen - 2);
	        	} else {
	        		long tmpbuf[] = new long[RadixTree.MAXKEYLEN/3+1];  // should be enough for all key lengths
	        		// Add info to most significant 4 bits
	        		tmpbuf[0] = childInfo[idx] << (bitLen - 4);
	        		writeKeyToBuf (tmpbuf, 0, 1, childref[idx], 1, bitLen);
	        		
	        		childvalue[idx] = tmpbuf[0];
	        		int overflowLen = lengthBitEncode(1, rt.getKeyLength(childref[idx])-1, bitLen);
	        		for (int j = 0; j < overflowLen; j++ ) {        			
	        			childvalue[longChildTermIdx] = tmpbuf[j+1];
	        			longChildTermIdx ++;
	        		}        		
	        	}
	        }
	        
	        if (!isFar) {
	        	if (DEBUG && !(longChildTermIdx == childnum + longchildterm)) {
	        		throw new AssertionError("longChildTermIdx error");
				}
	        } else {
	        	if (DEBUG && !(longChildTermIdx == childnum + longchildterm20)) {
					throw new AssertionError("longChildTermIdx error for far");
				}
	        }
	        
	        // write internal node info
	        long buf = 0;
    		long bufBitLen = 0;
    		int valueIdx = longChildTermIdx;
    	
        	for (int idx = 0; idx < childnum; idx ++) {
        		if ((childType[idx] == TYPE_INT) && (childInfo[idx] >= 0)) {
	    			buf <<= 4;
	    			buf |= childInfo[idx];
	    			bufBitLen += 4;
	    			if (bufBitLen == bitLen) {
	    				childvalue[valueIdx] = buf;
	    				valueIdx ++;
	    				buf = 0;
	    				bufBitLen = 0;
	    			}
        		}
        	}
    		if (bufBitLen > 0) {
    			buf <<= (bitLen - bufBitLen);
    			childvalue[valueIdx] = buf;
    			valueIdx ++;
    		}

	        if (!isFar) {	        	
	        	writeBlock (startBlock + headerLen, childvalue, valueIdx);
	        } else {	        	
	        	writeCompositeBlock (
	        			startBlock, headerBlockLen, startBlockEx,  // buffer config 
	        			headerLen + (splitmap? 1:-1), (splitmap? 1:2), // offset short block, long block
	        			childvalue, valueIdx, // long data to write 
	        			longBlockBitLength);  
	        }
    	}

    	if (stat) {
	    	if (!rt.isTerm(node)) {
	    		if (countOther.containsKey(len))
	    			countOther.put(len, countOther.get(len) + 1);
	    		else
	    			countOther.put(len, 1);
	    		
				if (countChild.containsKey(childnum))
					countChild.put(childnum, countChild.get(childnum) + 1);
				else
					countChild.put(childnum, 1);
	    	}
    	}
    }
    
    int block = 0;
    int lastStartBlock = 0;
    int farcount = 0;
    int totalExtraBlock = 0;

    private int allocate (int numBlock) {
    	int allocBlock = block;
    	block += numBlock;
    	return allocBlock;
    }
    

    /**
     * Return the number of ADDITIONAL blocks needed to encode a digit string
     * of length encodeLen, with default block length 
     *   
     * @param startPos position to start writing the encoded string at the beginning block,
     *        0 for the highest (most significant) 4 bits, 1 for the next 4 bits, etc.
     *        Encoding will write from the more significant bits.  
     * @param encodeLen length of the digit string (each digit uses 4 bit)
     * @return number of ADDITIONAL blocks required, excluding the first if partially used (i.e. startPos > 0)
     */
    private int lengthBitEncode (int startPos, int encodeLen) {
    	return lengthBitEncode (startPos, encodeLen, defaultBlockBitLength);
    }
    
    /**
     * Return the number of ADDITIONAL blocks needed to encode a digit string
     * of length encodeLen, with specified block length in bits 
     *   
     * @param startPos position to start writing the encoded string at the beginning block,
     *        0 for the highest (most significant) 4 bits, 1 for the next 4 bits, etc.
     *        Encoding will write from the more significant bits.  
     * @param encodeLen length of the digit string (each digit uses 4 bit)
     * @param blockBitLength length of a block in bits
     * @return number of ADDITIONAL blocks required, excluding the first if partially used (i.e. startPos > 0)
     */  
    private int lengthBitEncode (int startPos, int encodeLen, int blockBitLength) {
    	// total bits required plus start block
    	int totalbit = startPos * 4 + encodeLen * 4;
    	// Normally uses a termination symbol to mark the end of digit strings,
    	// But as frequency of strings falls off exponentially with length,  
    	// to optimise for short strings, use continuation character at
    	// the end of the first block if the string could not fit within it
    	// No termination symbol is needed if the string just fit in the first block
        if (totalbit < blockBitLength) {
        	// termination
        	totalbit += 4;
        } else if (totalbit > blockBitLength) {
        	totalbit += 8;  // continuation at first block + termination
        }
        // return blocks required, excluding the first if partially used
        return bitToBlock(totalbit, blockBitLength) - (startPos > 0? 1 : 0);
    }
 
    /**
     * Encode the digit string provided into the long buffer, starting at specified byte offset and position
     * using the default bit length for each block 
     *    
     * @param tmpbuf Target buffer to write (OR) to. tmpbuf[offset] may already has some bits set. 
     * @param offset offset into tmpbuf (i.e. writing starts at tmpbuf[offset])
     * @param position position to start writing the encoded string. See lengthBitEncode()
     * @param node node whose key is to be written
     * @param keypos offset into node's key to start writing
     */
    
    private void writeKeyToBuf (long[] tmpbuf, int offset, int position, int node, int keypos) {
    	writeKeyToBuf (tmpbuf, offset, position, node, keypos, defaultBlockBitLength);    	
    }
    
    /**
     * Encode the digit string provided into the long buffer, starting at specified byte offset and position
     * according to provided bit length for each block 
     *    
     * @param tmpbuf Target buffer to write (OR) to. tmpbuf[offset] may already has some bits set. 
     * @param offset offset into tmpbuf (i.e. writing starts at tmpbuf[offset])
     * @param position position to start writing the encoded string. See lengthBitEncode()
	 * @param node node whose key is to be written
     * @param keypos offset into node's key to start writing
     * @param blockBitLength length of a block in bits
     */
    
    private void writeKeyToBuf (long[] tmpbuf, int offset, int position, int node, int keypos, int blockBitLength) {
    	int keyLen = rt.getKeyLength(node) - keypos;  // start writing the key from keypos
    	int totalbit = (position + keyLen) * 4;
    	
    	int strBufIdx = rt.getKeyRef(node);
    	
    	if (totalbit <= blockBitLength) {
    		// pack in the offset byte 
    		tmpbuf[offset] = orDigits (tmpbuf[offset], position, strBufIdx, keypos, keyLen, blockBitLength);
    		// pack termination symbol 
    		if (!(totalbit == blockBitLength)) {
    			tmpbuf[offset] |= SYMBOL_TERM << (blockBitLength - totalbit - 4);
    		}
    	} else {
    		// pack first few digits in the offset byte
    		int digitPerBlock = blockBitLength/4;
    		int keyOffset = digitPerBlock - position - 1;
    		tmpbuf[offset] = orDigits (tmpbuf[offset], position, strBufIdx, keypos, keyOffset , blockBitLength);
    		tmpbuf[offset] |= SYMBOL_CONT;    		
    		for (; keyOffset < keyLen; keyOffset += digitPerBlock) {
    			offset ++;
    			tmpbuf[offset] = orDigits (0, 0, strBufIdx, keypos + keyOffset, 
    					(keyLen-keyOffset >= digitPerBlock)? digitPerBlock: keyLen-keyOffset, blockBitLength);
    		}
    		// add SYMBOL_TERM
    		if (keyOffset == keyLen) {
    			offset ++;
    			// write SYMBOL_TERM to new byte
    			tmpbuf[offset] |= SYMBOL_TERM << ((digitPerBlock - 1) * 4);    			
    		} else {
    			tmpbuf[offset] |= SYMBOL_TERM << ((keyOffset - keyLen - 1) * 4);
    		}
    	}
    }
    
    private long orDigits (long in, int pos, int strBufIdx, int offset, int len, int blockBitLength) {
    	long out;
    	int shift = blockBitLength - (pos+1) * 4;
    	
    	out = in;    	
    	for (int i = 0; i < len; i++) {
    		out |= (rt.getKeyCharAt(strBufIdx, offset + i) - (byte)'0') << shift;
    		shift -= 4;
    	}
    	return out;
    }
    
    /**
     * Calculate the number of blocks required for encoding bits, with default block length
     * @param bits number of bits to encode
     * @return number of blocks
     */
    private int bitToBlock (int bits) {
    	return bitToBlock(bits, defaultBlockBitLength);
    }
    
    /**
     * Calculate the number of blocks required for encoding bits, with specified block length
     * @param bits number of bits to encode
     * @return number of blocks
     */   
    private int bitToBlock (int bits, int blockBitLength) {
    	return (bits + (blockBitLength-1)) / blockBitLength;
    }
    

    /**
     * Write block to internal byte [] buffer, using defaultBlockLength
     * @param startBlock start block address, at defaultBlockBitLength
     * @param buffer buffer to write 
     * @param length length of buffer to write 
     */
    void writeBlock (int startBlock, long[] buffer, int length) {
    	writeBlock (startBlock, buffer, length, defaultBlockBitLength);
    }

    /**
     * Write block to internal byte [] buffer
     * @param startBlock start block address, at defaultBlockBitLength
     * @param buffer buffer to write 
     * @param length length of buffer to write
     * @param blockBitLength length of a block in bits for data to write
     */
    void writeBlock (int startBlock, long[] buffer, int length, int blockBitLength) {
    	// convert start block to bit / 4
    	long startBitBlock = startBlock * (defaultBlockBitLength / 4);
    	int bitBlockUnit = blockBitLength / 4;
    	for (int i = 0; i < length; i++) {
    		writeOneBlock (startBitBlock, buffer[i], blockBitLength);
    		startBitBlock += bitBlockUnit;
    	}
    }
    
    void writeOneBlock (long startBitBlock, long value, int blockBitLength) {
    	int byteOffset = (int) (startBitBlock / 2);    	
    	int shift = blockBitLength;
    	int mod = (int) (startBitBlock % 2);
    	
    	if (mod == 1) {
    		shift -= 4;
    		// Assume byteBuffer is cleared to 0    		
    		byteBuffer[byteOffset] |= (byte)((value >> shift) & 0xF);
    		if (DEBUG && ((value >> shift) > 0xF)) {
    			log.warn("Warning: value overflow: value = " + value);
			}
    		byteOffset += 1;
    	} 
    	while (shift >= 8) {	
    		shift -= 8;
    		byteBuffer[byteOffset] = (byte) ((value >> shift) & 0xFF);
    		byteOffset += 1;
    	}
    	if (shift > 0) {    		
    		// last 4 bit
    		byteBuffer[byteOffset] |= (byte) ((value & 0xF) << 4);
    		byteOffset += 1;
    	}
    	// assert last byte written within block allocation
    	if (DEBUG && !(byteOffset - 1 <= (block * defaultBlockBitLength + 7) / 8)) {
    		throw new AssertionError("last byte written within block allocation");
		}
    }
    
    /**
     * Write buffer to composite block
     * @param startBlock1 1st start block address (in defaultBlockBitLength)
     * @param block1Len length of 1st start block
     * @param startBlock2 2nd start block address (in defaultBlockBitLength)
     * @param defBlockOffset offset in number of defaultBlockBitLength
     * @param longBlockOffset offset in number of longBlockBitLength.
     *        The total offset is the sum of the bits represented by defBlockOffset 
     *        and longBlockOffset
     * @param buffer buffer to write
     * @param length length of buffer 
     * @param blockBitLength length of a block in buffer
     */
    void writeCompositeBlock (
			int startBlock1, int block1Len, int startBlock2,  // buffer config 
			int defBlockOffset, int longBlockOffset,
			long buffer[], int length, // long data to write 
			int blockBitLength) {
    	// convert start blocks to bit / 4
    	long startBitBlock1 = startBlock1 * (defaultBlockBitLength / 4);
    	long block1BitLen = block1Len * (defaultBlockBitLength / 4);
    	long startBitBlock2 = startBlock2 * (defaultBlockBitLength / 4);
    	
    	// calculate total offset in units of bit / 4
    	long currBitOffset = startBitBlock1 + (long) defBlockOffset * (defaultBlockBitLength / 4) +
    			(long) longBlockOffset * (longBlockBitLength / 4);
    	
    	// Split value buffer to write to the two blocks
    	    	
    	int bitBlockUnit = blockBitLength / 4;

    	int i = 0;
   		while (((currBitOffset + bitBlockUnit) <= (startBitBlock1 + block1BitLen))
   				&& (i < length)) {
    		writeOneBlock (currBitOffset, buffer[i], blockBitLength);
    		currBitOffset += bitBlockUnit;
    		i++;    		
    	}
    	if ((currBitOffset < (startBitBlock1 + block1BitLen)) && (i < length)) {
    		// current buffer[i] will straddle startBitBlock1 & 2
    		// write ((startBitBlock1 + block1BitLen)-currBitOffset) * 4 bit to 1st block
    		// and the remaining to the 2nd block
    		// buffer[i] needs to be split into 2 parts, with [bits1] & [bits2] bits respectively
    		int bits1 = (int) (((startBitBlock1 + block1BitLen) - currBitOffset) * 4);
    		int bits2 = blockBitLength - bits1;
    		writeOneBlock (currBitOffset, buffer[i] >> bits2, bits1);
    		currBitOffset = startBitBlock2;
    		writeOneBlock (startBitBlock2, buffer[i] & ((((long)1) << bits2) - 1), bits2);
    		i++;
    		currBitOffset += bits2 / 4;
    	} else if ((currBitOffset >= (startBitBlock1 + block1BitLen)) && (i < length)) {
    		currBitOffset = startBitBlock2 + currBitOffset - (startBitBlock1 + block1BitLen);
    	}
    	while (i < length) {
    		writeOneBlock (currBitOffset, buffer[i], blockBitLength);
    		currBitOffset += bitBlockUnit;
    		i++;    		
    	}
    }

	/**
	 * Get the size (in byte) of the radix tree buffer to be written
     * @return size of the buffer in bytes
	 */
	@Override
	public int getBufSize () {
		return (block * defaultBlockBitLength + 7) / 8;
	}


    /**
     * Save a single trie byte buffer to file path
     * @param filename file path to save
     * @throws IOException 
     */
    public void writeTrie (String filename) throws IOException {
    	File file = new File(filename);
    	// Create folder if not already exists
    	if (!file.exists()) {
    		file.getParentFile().mkdirs();
    		file.createNewFile();
    	}
        FileOutputStream os = null;
        try {
            os = new FileOutputStream(file);
            writeTrie(os);
        } finally {
            if (os != null) os.close();
        }
    }

    /**
     * Save a set of trie byte buffers to file path, with magic key / an index block
     * @param filename file path to save
     * @throws IOException
     */
    public static void writeMultiTrie (String filename, RadixTreeBufWritable... rtBufWriters) throws IOException {
        File file = new File(filename);
        // Create folder if not already exists
        if (!file.exists()) {
            file.getParentFile().mkdirs();
            file.createNewFile();
        }
        FileOutputStream os = null;

        try {
            os = new FileOutputStream(file);

            // Write 4 byte magic key
            byte[] magic = {(byte) 'T', (byte) 'R', (byte) 'I', (byte) 'E'};
            os.write(magic);

            // Write 4 byte version
            os.write(intToByteBuf(0x01));

            // Write Number of buffers (4 bytes)
            os.write(intToByteBuf(rtBufWriters.length));

            // Write Buffer sizes (4 bytes each)
            for (RadixTreeBufWritable rtBufWriter : rtBufWriters) {
                os.write(intToByteBuf(rtBufWriter.getBufSize()));
            }

            // Write the buffers
            for (RadixTreeBufWritable rtBufWriter : rtBufWriters) {
                rtBufWriter.writeTrie(os);
            }
        } finally {
            if (os != null) os.close();
        }
    }

    /**
     * Utility to convert an unsigned integer to a 4-byte byte buffer for writing to an output stream
     * @param v value of integer
     * @return byte buffer
     */
    private static byte[] intToByteBuf (int v) {
        byte[] byteBuf = new byte[] {
            (byte)(v >>> 24),
            (byte)(v >>> 16),
            (byte)(v >>> 8),
            (byte)v
        };
        return byteBuf;
    }

    /**
     * Write the trie byte buffer to the output stream
     * @param os output stream to write to
     * @throws IOException
     */
	@Override
    public void writeTrie (OutputStream os) throws IOException {
        os.write(byteBuffer, 0, getBufSize());
    }
    
    /**
     * For stat only after building trie
     */
    public void encodeTrieWithStat() {
    	// Encode the trie and keep stats
    	initStat ();
    	encodeTrie ();

		rt.printstat();
		log.info ("Next byte offset for buffer = {}", getBufSize());
    	log.info ("Block count (in 12 bit units) = {}", block);
    	log.info ("Block with far reference = {}", farcount);
    	log.info ("Total extra header block = {}", totalExtraBlock);

    	if (stat) {
	    	log.info ("Real nodes count by key length");
	    	for (Entry<Integer, Integer> e : countReal.entrySet()) {
	    		log.info("{} : {}", e.getKey(), e.getValue());
	    	}
	    	log.info ("Internal nodes count by key length");
	    	for (Entry<Integer, Integer> e : countOther.entrySet()) {
	    		log.info("{} : {}", e.getKey(), e.getValue());
	    	}
	    	log.info ("Children count by key length");
	    	for (Entry<Integer, Integer> e : countChild.entrySet()) {
	    		log.info("{} : {}", e.getKey(), e.getValue());	    		
	    	}
    	}

    }
}
