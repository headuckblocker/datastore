package com.headuck.datastore;

import com.headuck.datastore.DBImporter.IDownloadProgressHandler;
import com.headuck.datastore.data.DBFactory;
import com.headuck.datastore.data.HashFactory;
import com.headuck.datastore.data.Record;
import com.headuck.datastore.data.StoreFactory;
import com.headuck.datastore.utils.Utils;
import com.headuck.datastore.xml.XMLRule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;


public class JunkManager {
	private boolean init = false;

	public static int SOURCE_HKJUNKCALL_JUNK = 0;
	public static int SOURCE_HKJUNKCALL_ORG = 1;

	public HashFactory hashFactoryInfo;
	public HashFactory hashFactoryVer;
	public StoreFactory dbFactoryJunk;
	public StoreFactory dbFactoryCCat;
	public StoreFactory dbFactoryCat;
	public StoreFactory dbFactoryLevel;
    public StoreFactory dbFactoryOrg;

	// Parameters set 
	private String credential = null;
	private String inputFilePath = null;  // for use in import from file
	private InputStream inputStream = null;    // for use in import from ZIP Stream
	private int urlSource = SOURCE_HKJUNKCALL_JUNK;  // either SOURCE_HKJUNKCALL_JUNK or SOURCE_HKJUNKCALL_ORG
	private IDownloadProgressHandler junkHandler = null;
	private IDownloadProgressHandler orgHandler = null;

	// Data from info.xml
	private String urlInfo = null;
	private String urlMainz = null;
	private String urlWl2z = null;
	
	// Version info from header / HKG.xml
	private String verDate = null;
	private String verTime = null;
	private String verSerial = null;
	
	// final static String infoXMLUrl = "http://hkjunkcall.com/info.xml";

	// Note: use (( for base 64 encoded string padding instead of ==

    // Base64 encoded https://dls.hkjunkcall.com/HKG_info.xml
    final static String HKGInfoUrlEncoded = "aHR0cHM6Ly9kbHMuaGtqdW5rY2FsbC5jb20vSEtHX2luZm8ueG1s";
    // Base64 encoded https://dls.hkjunkcall.com/dls.asp?rf=HKG.ZIP
    final static String MainzUrlEncoded = "aHR0cHM6Ly9kbHMuaGtqdW5rY2FsbC5jb20vZGxzLmFzcD9yZj1IS0cuWklQ";
	// Base64 encoded https://dls.hkjunkcall.com/dls.asp?rf=HKG_WL2.ZIP
	final static String Wl2zUrlEncoded = "aHR0cHM6Ly9kbHMuaGtqdW5rY2FsbC5jb20vZGxzLmFzcD9yZj1IS0dfV0wyLlpJUA" + "((";
	// https pub key pin
	// pub key pin can be obtained by running pin.py for dls.hkjunkcall.com 443
	// and encode the PLAIN public key of the site by base64
	final static String pubKeyPinEncoded = "MzA4MjAxMjIzMDBkMDYwOTJhODY0ODg2ZjcwZDAxMDEwMTA1MDAwMzgyMDEwZjAwMzA4MjAxMGEwMjgyMDEwMTAwOWMzMDQ2YTY4OGNiMjQ3NzFjOTQ2NDhlOTMxODA0NDk0NmMxMDZjMjZhNjcwYTE4ODFjZGY1ZTkwZTVlMTVhM2NmMTUwY2QwNTFjZjYwZjE2MWE1ZjVhNTNkMWM3ZWVlZmY4MTk2YzQ5NWU2OTc2ODE2NGMyMjdjOTI4NTM1MDBhYzY1MjRmNzQ3ZjdjMTBlYzYwNzcxMTA3ZmIzNzk0NTBmZGI0MjM4MWIyY2U4YzI4ZDE0YzM5ZjcyOTlhYjI2OTMyNjI4NWQ5ZjI3ZGI1ZjUwYTEwYTdmNDEzN2RlZDdlNzhiNjczNTYwZjBiNGI1OTc2Yjc3YzU2Njc5NzA3ODQyMjhmNDViMWIwZmRjZTY1OWRkNmFkYjMzZDQ3N2M4OGZlZjYwZTVkMGFhY2Q5MDZhYTY0YTM1NWI1M2E2Mjk1OTYzMGZlOGYyY2ViNzJkNDU2ZjUwMzYxYzgzOTk0ZGRkYWY3NWZkMzIyNzA1YTU5YTJmNDRmYzcxMjcwYjk1MmU1YWJjMzk5M2MzYzc0YjFiMWIzNmQwMjk4MjExYjZlYTM1ZmE5OGYzZjcyMjA2ZGUxNDY3MTY0NGUyZjM1NTAxNTRhYmU3OTEwMTNhYWJjOWI1NmM5MjE0ZjYwYTI4NTFkOWNlMDJhNjNiZTIzMzE5MjM5MjA3NzYyMDQ4MzUwMjAzMDEwMDAx";
	// cert key pin can be obtained by obtaining SHA-256 of intermediate certs
	// separate with comma, and encode by base64
	// Amazon,Godaddy: 28689b30e4c306aab53b027b29e36ad6dd1dcf4b953994482ca84bdc1ecac996,3a2fbe92891e57fe05d57087f48e730f17e5a5f53ef403d618e5b74d7a7e6ecb
	final static String certPinsEncoded = "Mjg2ODliMzBlNGMzMDZhYWI1M2IwMjdiMjllMzZhZDZkZDFkY2Y0Yjk1Mzk5NDQ4MmNhODRiZGMxZWNhYzk5NiwzYTJmYmU5Mjg5MWU1N2ZlMDVkNTcwODdmNDhlNzMwZjE3ZTVhNWY1M2VmNDAzZDYxOGU1Yjc0ZDdhN2U2ZWNi";

	final static int IMPORT_FROM_HTTP = 1;
	final static int IMPORT_FROM_FILE = 2;		
	final static int IMPORT_FROM_ZIPSTREAM = 3;

	private String errMsg = null;

	final static Logger log = LoggerFactory.getLogger("JunkManager");

	static private JunkManager junkDbMgr = null;

	public static JunkManager getJunkManager (String pathHKJunkDB) {
		if (junkDbMgr == null) {
			junkDbMgr = new JunkManager().initFactories(pathHKJunkDB);
			log.info ("Initialized JunkManager");
		}
		return junkDbMgr;
	}

	/**
	 * Initialize the public db factory paths
	 * @param pathHKJunkDB path to the JunkDB database
	 * @return JunkManager instance
	 */
	public JunkManager initFactories(String pathHKJunkDB) {		
		dbFactoryJunk =  new DBFactory (pathHKJunkDB, "junk.db", "junkidx.db", "pn", false);		
		dbFactoryCCat =  new DBFactory (pathHKJunkDB, "ccat.db");		
		dbFactoryCat =  new DBFactory (pathHKJunkDB, "cat.db");	
		dbFactoryLevel =  new DBFactory (pathHKJunkDB, "level.db");		
		hashFactoryVer =  new HashFactory ("version.hash").setPrimaryField("region.HKG");		
		hashFactoryInfo =  new HashFactory ("info.hash");
        dbFactoryOrg = new DBFactory (pathHKJunkDB, "org.db", "orgidx.db", "pns", false);
		init = true;
		return this;
	}
	
	/**
	 * Sets the id and key for retrieving info from HKJunkCall
	 * @param credential id and key string, in form of "key=&lt;key&gt;&id=&lt;id&gt;"
	 * @return the same JunkManager instance
	 */
	public JunkManager setCredsdential(String credential) {
		this.credential = credential;
		return this;
	}
	
	public JunkManager setInputFilePath(String path) {
		this.inputFilePath = path;
		return this;
	}
	
	public JunkManager setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
		return this;
	}

	public JunkManager setInputUrl(int urlSource) {
		this.urlSource = urlSource;
		return this;
	}

	/**
	 * Sets the IDownloadProgressHandler progress handler for the junk importer
	 * @param junkHandler Download progress handler
	 * @return the same JunkManager instance
	 */
	public JunkManager setJunkProgressHandler(IDownloadProgressHandler junkHandler) {
		this.junkHandler = junkHandler;
		return this;
	}

	/**
	 * Sets the IDownloadProgressHandler progress handler for the junk importer
	 * @param orgHandler Download progress handler
	 * @return the same JunkManager instance
	 */
	public JunkManager setOrgProgressHandler(IDownloadProgressHandler orgHandler) {
		this.orgHandler = orgHandler;
		return this;
	}

	/**
	 * Get the file locations on the HKJunkCall server 
	 * @return true if success, false if failure
	 */
	public boolean getXMLInfo () {
		/*
		boolean ret;
		Record record = null;
		String key = null;
		*/

		if (!init) {
			log.error ("Not yet initialized");
			return false;
		}

        // Since HKJunkcall security upgrade, uses hardcoded URL Info and Mainz location
        /*
		ret = getMeta (infoXMLUrl, 
				JunkXMLDef.xmlRuleInfo,
				hashFactoryInfo);			
		if (!ret) return false;
		
		for (Map.Entry<String, Record> entry : hashFactoryInfo.storageMap.entrySet()) {
	        record = entry.getValue();
	        key = entry.getKey();		         
	        if ("HKG".equals (key)) break;
	    }
		
		if ("HKG".equals (key) && (record != null)) {
			urlInfo = record.map.getString("data.info");
			urlMainz = record.map.getString("data.main_z");
			if ((urlInfo != null) && (urlMainz != null)) {
				return true;
			}
		}
		log.error ("Cannot find HKG info in {}", infoXMLUrl);		
		return false;
		*/
        urlInfo = Utils.decode(HKGInfoUrlEncoded);
        urlMainz = Utils.decode(MainzUrlEncoded);
		urlWl2z = Utils.decode(Wl2zUrlEncoded);
        return true;
	}
	
	/**
	 * Get the version info of the current database in the HKJunkCall server
	 * This should be called after initialization. This will call getXMLInfo if needed
	 *  
	 * @return version string in form of "DATE TIME REVISION" if success, null if failure
	 */
	public String getDataInfo () {
		boolean ret;
		
		if (!init) {
			log.error ("Not yet initialized");
            setError ("Junk manager not initialized");
			return null;
		}
		if (urlInfo == null) {
			if (!getXMLInfo()) { 
				log.error ("Failed to initialize data info url for hkjunkcall");
				setError ("Failed to get data info url for hkjunkcall");
				return null;
			}
		}
		ret = getMeta (urlInfo, 
				JunkXMLDef.xmlRuleVer,
				hashFactoryVer);			
		if (!ret) return null;
		String verInfo = extractVerInfo();
		if (verInfo != null) {
			return verInfo;
		}		
		log.error ("Cannot find Version info in {}", urlInfo);
		setError ("Failed to get version information");
		return null;		
	}
	
	private boolean getMeta(String url, XMLRule xmlRule, HashFactory hashFactory) {
		DBImporter dbImporterMeta;
		String pubKeyPin = Utils.decode(pubKeyPinEncoded);
		String[] certPin = Utils.decodeMultiple(certPinsEncoded);
		dbImporterMeta = new DBImporter().addRule(xmlRule, hashFactory).setPin(pubKeyPin, certPin);
		
		if (dbImporterMeta != null) {
			try {
				dbImporterMeta.importFromHTTP(url);
					
				if (dbImporterMeta.error) {
					log.error ("Error reading url {}: {}", url, dbImporterMeta.errorMsg);
					return false;
				}
				return true;
				
			} catch (IOException e)	
			{
				if (Thread.interrupted()) {
					log.warn ("Thread interrupted!", e);
				} else {
					log.error ("Exception reading url " + url, e);
				}
			}
			catch (SAXException e) 
			{
			    log.error ("Parse exception reading url "+ url, e);
			}						
		}
		return false;
	}
	
	private String extractVerInfo () 
	{				
		Record record = null; 
		
		for (Map.Entry<String, Record> entry : hashFactoryVer.storageMap.entrySet()) {
	        record = entry.getValue();
	        break;
	    }		
		if (record != null) {
			verDate = record.map.getString("update");
			verTime = record.map.getString("time");
			verSerial = record.map.getString("version");
		}
		if ((verDate != null) && (verTime != null) && (verSerial != null)) {			
			return verDate + " " + verTime + " " + verSerial;
		}
		return null;
	}

	public DBImporter getDBImporterJunk() {
		String pubKeyPin = Utils.decode(pubKeyPinEncoded);
		String[] certPin = Utils.decodeMultiple(certPinsEncoded);
		DBImporter dbImporterJunk = new DBImporter(junkHandler)
				.addRule(JunkXMLDef.xmlRuleJunk, dbFactoryJunk)
				.addRule(JunkXMLDef.xmlRuleCCat, dbFactoryCCat)
				.addRule(JunkXMLDef.xmlRuleCat, dbFactoryCat)
				.addRule(JunkXMLDef.xmlRuleLevel, dbFactoryLevel)
				.addRule(JunkXMLDef.xmlRuleVer, hashFactoryVer)
				.setPin(pubKeyPin, certPin);
		return dbImporterJunk;
	}

	public DBImporter getDBImporterOrg() {
		String pubKeyPin = Utils.decode(pubKeyPinEncoded);
		String[] certPin = Utils.decodeMultiple(certPinsEncoded);
		DBImporter dbImporterOrg = new DBImporter(orgHandler)
				.addRule(JunkXMLDef.xmlRuleOrg, dbFactoryOrg)
				//.addRule(JunkXMLDef.xmlRuleCCat, dbFactoryCCat)
				//.addRule(JunkXMLDef.xmlRuleCat, dbFactoryCat)
				.addRule(JunkXMLDef.xmlRuleVer, hashFactoryVer)
				.setPin(pubKeyPin, certPin);
		return dbImporterOrg;
	}

	/**
	 * Import junk call database from HKJunkCall site by HTTP
	 * This should be called after initialization as well as setCredsdential
	 * This will call getXMLInfo if needed
	 *  
	 * @return version string in form of "DATE TIME REVISION" if success, null if failure
	 */

	public String importFullDBfromHTTP(int source, DBImporter dbImporter)  {
		setInputUrl(source);
		return importFullDB(IMPORT_FROM_HTTP, dbImporter);
	}

    /**
     * Import junk call database from HKJunkCall site by HTTP, using previous result of getDataInfo()
     * This should be called after initialization as well as setCredsdential,
     * and recent getDataInfo()
     *
     * @return version string in form of "DATE TIME REVISION" if success, null if failure
     */

    public String importFullDBfromHTTPwithPrevVerInfo(int source, DBImporter dbImporter)  {
		setInputUrl(source);
        return importFullDB(IMPORT_FROM_HTTP, dbImporter, false, true);
    }

	/**
	 * Import junk call database from file
	 * This should be called after initialization and setInputFilePath
	 *  
	 * @return version string in form of "DATE TIME REVISION" if success, null if failure
	 */
	public String importFullDBfromFile(DBImporter dbImporter)  {
		return importFullDB(IMPORT_FROM_FILE, dbImporter);
	}

	/**
	 * Import junk call database from file
	 * This should be called after initialization
	 * @param path file path to import  
	 * @return version string in form of "DATE TIME REVISION" if success, null if failure
	 */
	public String importFullDBfromFile(String path, DBImporter dbImporter)  {
		setInputFilePath(path);
		return importFullDB(IMPORT_FROM_FILE, dbImporter);
	}
	
	/**
	 * Import junk call database from zipped stream
	 * This should be called after initialization and setInputStream
	 *  
	 * @return version string in form of "DATE TIME REVISION"  if success, null if failure
	 */
	public String importFullDBfromZipStream(DBImporter dbImporter)  {
		return importFullDB(IMPORT_FROM_ZIPSTREAM, dbImporter);
	}
	
	/**
	 * Import junk call database from zipped stream
	 * This should be called after initialization
	 *  
	 * @param inputStream input zip stream for import
	 * @param allowOnlyMeta true if missing version string is allowed
	 * @return version string in form of "DATE TIME REVISION"  if success,
	 *         "" if allowOnlyMeta is true and no version string is found
	 *         null if failure
	 */
	public String importFullDBfromZipStream(InputStream inputStream, DBImporter dbImporter, boolean allowOnlyMeta)  {
		setInputStream(inputStream);
		return importFullDB(IMPORT_FROM_ZIPSTREAM, dbImporter, allowOnlyMeta);
	}

	/**
	 * Import junk call database
	 * @param importSource input source
	 * @return version string in form of "DATE TIME REVISION" if success
	 *         null if failure
	 */

	private String importFullDB(int importSource, DBImporter dbImporter) {
		return importFullDB(importSource, dbImporter, false);
	}

	/**
	 * Import junk call database
	 * @param importSource input source
	 * @param allowOnlyMeta true if missing version string is allowed
	 * @return version string in form of "DATE TIME REVISION" if success,
	 *         "" if allowOnlyMeta is true and no version string is found
	 *         null if failure
	 */
	private String importFullDB(int importSource, DBImporter dbImporter, boolean allowOnlyMeta) {
        return importFullDB(importSource, dbImporter, allowOnlyMeta, false);
    }

    /**
     * Import hkjunkcall database
     * @param importSource input source
     * @param allowOnlyMeta true if missing version string is allowed
     * @param usePrevVerInfo true to use the result of the previous call to getDataInfo()
     *                       (for import from HTTP only)
     * @return version string in form of "DATE TIME REVISION" if success,
     *         "" if allowOnlyMeta is true and no version string is found
     *         null if failure
     */
    private String importFullDB(int importSource, DBImporter dbImporter, boolean allowOnlyMeta, boolean usePrevVerInfo)  {

		setError (null);
		if (!init) {
			log.error ("Not yet initialized");
			return null;
		}
		
		String sourceText = "";
		try {
			switch (importSource) {
			case IMPORT_FROM_HTTP:
				String importUrl = null;
				if (urlSource == SOURCE_HKJUNKCALL_JUNK) {
					importUrl = urlMainz;
				} else if (urlSource == SOURCE_HKJUNKCALL_ORG) {
					importUrl = urlWl2z;
				}
				// Update version info
				sourceText = "url: " + importUrl;
                if (!usePrevVerInfo) {
                    if (getDataInfo() == null) {
                        log.error("Failed to initialize junk list info from server");
                        setError("Failed to get information from server");
                        return null;
                    }
                }
                if ((importUrl == null) || (verDate == null) || (verTime == null)) {
                    log.error ("Failed to get version information from server");
                    setError ("Failed to get version information from server");
                    return null;
				}
				dbImporter.importFromHTTPWithToken(importUrl, credential + dateStrip(verDate) + verTime);
				break;
			case IMPORT_FROM_FILE:
				sourceText = "file: " + inputFilePath;
				dbImporter.importFromFile(inputFilePath);
				break;
			case IMPORT_FROM_ZIPSTREAM:
				sourceText = "ZIP Stream";
				dbImporter.importFromZipStream(inputStream);
			}
			
			if (dbImporter.error) {
				log.error ("Error reading {}: {}", sourceText, dbImporter.errorMsg);
				setError (dbImporter.errorMsg);
                return null;
            }


			String verInfo = extractVerInfo();
			if (verInfo != null) {
				return verInfo;
			}

			if (allowOnlyMeta) {
				// Allow failure in extracting version info
				return "";
			}

			log.error ("Cannot find Version info from {}", sourceText);
			setError ("Failed to get version information");
			return null;
		}
		catch (IOException e)	
		{
			if (Thread.interrupted()) {
				log.warn ("Thread interrupted!", e);
			} else {
				log.error ("Exception importing list from " + sourceText, e);
			}
		}
		catch (SAXException e) 
		{
		    log.error ("Parse exception importing list from "+ sourceText, e);
		}			
		return null;

	}

    /**
     * Strips hyphens from the date string
     * @param dateVer in format yyyy-mm-dd
     * @return string yyyymmdd
     */
    private String dateStrip (String dateVer) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < dateVer.length(); i++) {
            char c = dateVer.charAt(i);
            if (c <= '9' && c >= '0') {
                sb.append(c);
            }
        }
        return sb.toString();
    }

	private void setError (String errMsg) {
		this.errMsg = errMsg;
	}
	
	public String getError () {
		return errMsg;
	}
	
	
}
	
