package com.headuck.datastore;

import com.headuck.datastore.xml.XMLRule;

public class JunkXMLDef {

	public static final XMLRule xmlRuleInfo = new XMLRule(1, new String[] {"definition","DB", "record"},
			new String[] {"data.info", "data.main_z"},
			new String[] {"id", "type"});
	
	public static final XMLRule xmlRuleJunk = new XMLRule(2, new String[] {"definition","md", "rc"},
			new String[] {"cn","pn","cat","ud","lvl","ccat"},
			new String[] {"id"});

	public static final XMLRule xmlRuleCCat = new XMLRule(3, new String[] {"definition","CompanyCat2", "record"},
			new String[] {"name.en_us","desc.en_us","name.zh_hk","desc.zh_hk","name.zh_cn","desc.zh_cn"},
			new String[] {"id", "lang"});
	
	public static final XMLRule xmlRuleCat = new XMLRule(4, new String[] {"definition","catmaster2", "record"},
			new String[] {"name.en_us","desc.en_us","name.zh_hk","desc.zh_hk","name.zh_cn","desc.zh_cn"},
			new String[] {"id", "lang"});

	public static final XMLRule xmlRuleLevel = new XMLRule(5, new String[] {"definition","cLevel2", "record"},
			new String[] {"name.en_us","desc.en_us","name.zh_hk","desc.zh_hk","name.zh_cn","desc.zh_cn"},
			new String[] {"id", "lang"});
	
	public static final XMLRule xmlRuleVer = new XMLRule(6, new String[] {"definition","properties"},
			new String[] {"update", "time", "version", "region.HKG"},
			new String[] {"code"});

	public static final XMLRule xmlRuleOrg = new XMLRule(7, new String[] {"definition","wd", "rc"},
			new String[] {"cn", "pns", "pne", "ud", "ct"},
			new String[] {"id"});
}
