package com.headuck.datastore.net;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Arrays;

import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;
import java.security.MessageDigest;
// import javax.xml.bind.DatatypeConverter;
/**
 * Created by headuck on 2/8/16.
 * Reference:
 * http://stackoverflow.com/questions/19005318/implementing-x509trustmanager-passing-on-part-of-the-verification-to-existing
 * https://github.com/Flowdalic/java-pinning/blob/master/java-pinning-aar/src/main/java/eu/geekplace/javapinning/PinningTrustManager.java
 */
public class PinningTrustManager implements X509TrustManager {

    private final byte[] pinLeafBytes;
    private final byte[][] pinCABytes;
    final X509TrustManager defaultTm;
    final static Logger log = LoggerFactory.getLogger("PinningTrustManager");

    PinningTrustManager(String pinLeaf, String[] pinCA) {
        if (pinLeaf != null) {
            pinLeafBytes = hexToBytes(pinLeaf);
        } else {
            pinLeafBytes = null;
        }
        if (pinCA != null) {
            int len = pinCA.length;
            if (len > 0) {
                pinCABytes = new byte[len][];
                for (int i = 0; i < len; i++) {
                    pinCABytes[i] = hexToBytes(pinCA[i]);
                }
            } else {
                pinCABytes = null;
            }
        } else {
            pinCABytes = null;
        }
        // Get the default trust manager
        X509TrustManager x509Tm = null;
        TrustManagerFactory tmf = null;
        String errMsg = null;
        try {
            tmf = TrustManagerFactory
                    .getInstance(TrustManagerFactory.getDefaultAlgorithm());
            // Using null here initialises the TMF with the default trust store.
            tmf.init((KeyStore) null);
        } catch (NoSuchAlgorithmException e) {
            errMsg = "No such algorithm exception: " + e.getMessage();
        } catch (KeyStoreException e) {
            errMsg = "Key Store exception: " + e.getMessage();
            tmf = null;
        }
        if (tmf != null) {
            for (TrustManager tm : tmf.getTrustManagers()) {
                if (tm instanceof X509TrustManager) {
                    x509Tm = (X509TrustManager) tm;
                    break;
                }
            }
        }
        defaultTm = x509Tm;  // null if error
        if (errMsg != null) {
            log.warn("Error in init default trust manager: "+errMsg);
        }
    }

    private static byte[] hexToBytes(String pinHexString) {
        byte[] pinBytes;

        // Convert pinHexString to lower case. Note that we don't need to use
        // the locale argument version of toLowerCase() as we will throw an
        // exception later anyway when a character ^[a-f0-9] is found
        pinHexString = pinHexString.toLowerCase();

        final char[] pinHexChars = pinHexString.toCharArray();
        // Check that pinHexChars only contains chars [a-f0-9]
        for (char c : pinHexChars) {
            // Throw exception if char is ^[a-f0-9]
            if (! ((c >= 'a' && c <= 'f') || (c >= '0' && c <= '9'))) {
                throw new IllegalArgumentException(
                        "Pin String must only contain ASCII letters [a-fA-F] and numbers [0-9], found offending char: '"
                                + c + "'");
            }
        }

        // Convert the pinHexString to bytes
        final int len = pinHexChars.length;
        pinBytes = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            pinBytes[i / 2] = (byte) ((Character.digit(pinHexString.charAt(i), 16) << 4) + Character
                    .digit(pinHexString.charAt(i + 1), 16));
        }
        return pinBytes;
    }

    @Override
    public void checkClientTrusted(X509Certificate[] chain, String authType)
            throws CertificateException {
        if (defaultTm != null) {
            defaultTm.checkClientTrusted(chain, authType);
        } else {
            throw new UnsupportedOperationException();
        }
    }

    @Override
    public void checkServerTrusted(X509Certificate[] chain, String authType)
            throws CertificateException {
        final X509Certificate leafCertificate = chain[0];
        boolean validated = false;
        // printCert(chain[0]);
        // Additional check that the public key is the pinned one
        if (isPubKeyPinned(leafCertificate, pinLeafBytes)) {
            if (defaultTm != null) {
                defaultTm.checkServerTrusted(chain, authType);
            }
            validated = true;
        }

        // If not direct pin, check parent

        if (!validated) {
            log.info ("Leaf cert not pinned, checking intermediates");
            for (int i = 1; i < chain.length; i++) {
                //printCert(chain[i]);
                if (isCertPinned(chain[i], pinCABytes)) {
                    if (defaultTm != null) {
                        defaultTm.checkServerTrusted(chain, authType);
                    }
                    validated = true;
                    log.info ("Intermediate cert validated");
                    break;
                }
            }
        }

        if (validated) return;
        // Throw a CertificateException with a meaningful message. Note that we
        // use CERTPLAIN, which tends to be long, so colons as separator are of
        // no use and most other software UIs show the "public key" without
        // colons (and using lowercase letters).
        // final StringBuilder pinHexString = JavaPinningUtil.toHex(leafCertificate.getEncoded(), false, false);
        throw new CertificateException("Certificate not pinned.");
    }

    @Override
    public X509Certificate[] getAcceptedIssuers() {
        if (defaultTm != null) {
            return defaultTm.getAcceptedIssuers();
        } else {
            return new X509Certificate[0];
        }
    }
/*
    private void printCert (X509Certificate x509certificate) {
        try {
            //byte[] cer = x509certificate.getEncoded();
            //byte[] sig = x509certificate.getSignature();
            System.out.println("CERT: " + x509certificate.getSubjectDN().toString());
            for (byte b: cer) {
                System.out.format("%02X", b);
            }
            System.out.println("\nSIGN: ");
            byte[] thumb = getThumbprint(x509certificate);
            for (byte b: thumb) {
                String s = String.format("%02X ", b);
                System.out.print(s);
            }
            System.out.println("");

        } catch (CertificateEncodingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

    }
*/
    private static byte[] getThumbprint(X509Certificate cert)
            throws NoSuchAlgorithmException, CertificateEncodingException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        byte[] der = cert.getEncoded();
        md.update(der);
        byte[] digest = md.digest();
        return digest;
    }

    private boolean isPubKeyPinned(X509Certificate x509certificate, byte[] pinBytes) throws CertificateEncodingException {
        // Check equality of leaf public key
        if ((pinBytes == null) || !Arrays.equals(pinBytes, x509certificate.getPublicKey().getEncoded())) {
            return false;
        }
        return true;
    }

    private boolean isCertPinned(X509Certificate x509certificate, byte[][] pinBytesArray) throws CertificateEncodingException {
        // Check equality of leaf public key
        if (pinBytesArray == null) return false;
        try {
            byte[] certThumb = getThumbprint(x509certificate);
            for (byte[] pinBytes : pinBytesArray) {
                if (Arrays.equals(pinBytes, certThumb)) {
                    return true;
                }
            }
        } catch (NoSuchAlgorithmException e) {
            log.warn("Error checking cert pin",  e);
        } catch (CertificateEncodingException e) {
            log.warn("Error checking cert pin",  e);
        }
        return false;
    }
}