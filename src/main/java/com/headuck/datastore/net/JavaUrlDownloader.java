/**
 * License: GPL v3. Copyright 2015 Headuck 
 */
package com.headuck.datastore.net;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;

/**
 * 
 *
 */
public class JavaUrlDownloader extends UrlDownloader {

	protected HttpURLConnection conn;
	protected URL url;
	protected final String token;
    protected SSLContext sslContext = null;

	public JavaUrlDownloader (final String urlString, final IHttpHandler handler) throws MalformedURLException
	{
		super(urlString, handler);
		conn = null;
		token = null;
		url = new URL(urlString);
	}

	public JavaUrlDownloader (final String urlString, final String token, final IHttpHandler handler) throws MalformedURLException
	{
		super(urlString, handler);
		conn = null;
		this.token = token;
		url = new URL(urlString);
	}

    public void setSSLContext (String pubKey, String[] caCert) {
        SSLContext sslContext;
        try {
            sslContext = SSLContext.getInstance("TLS");
        } catch (NoSuchAlgorithmException e) {
            log.error ("No Such Algorithm Exception during init of SSL Context", e);
            return;
        }
        try {
            sslContext.init(null, new TrustManager[]{new PinningTrustManager(pubKey, caCert)}, null);
            this.sslContext = sslContext;
        } catch (KeyManagementException e) {
            log.error ("Key Management Exception in setting SSL Context: ", e);
        } catch (IllegalArgumentException e) {
            log.error ("Exception in setting key for SSL Context: ", e);
        }
    }

	public void setDefaultSSLContext () {
		SSLContext sslContext;
		try {
			sslContext = SSLContext.getInstance("TLS");
		} catch (NoSuchAlgorithmException e) {
			log.error ("No Such Algorithm Exception during init of SSL Context", e);
			return;
		}
		try {
			sslContext.init(null, null, null);
			this.sslContext = sslContext;
		} catch (KeyManagementException e) {
			log.error ("Key Management Exception in setting SSL Context: ", e);
		} catch (IllegalArgumentException e) {
			log.error ("Exception in setting key for SSL Context: ", e);
		}
	}
	
	/**
	 * Network implementation specific function for opening connection
	 * Gets contents of URL from urlString and reports to handler
	 * 
	 * @return true if success; false if error
	 * Should call closeConnection() to clean up if return true
	 */
	@Override
	public boolean openConnection()
	{
		return openConnection(false);
	}

	/**
	 * Network implementation specific function for opening connection
	 * Gets contents of URL from urlString and reports to handler
	 * @param allowMinusContentLength allows minus content length returned in header
	 * @return true if success; false if error
	 * Should call closeConnection() to clean up if return true
	 */
	@Override
	public boolean openConnection(boolean allowMinusContentLength)
	{
		try {
			while (true) {
				conn = (HttpURLConnection) url.openConnection();
				conn.setRequestProperty("User-Agent", USER_AGENT);
				if (token != null) {
					conn.setRequestProperty("ACCESSTOKEN", token);
				}
				if (conn instanceof HttpsURLConnection) {
					if (sslContext != null) {
						((HttpsURLConnection) conn).setSSLSocketFactory(sslContext.getSocketFactory());
					} else {
						throw new SSLContextMissingException("HTTPS connection requires SSLContext");
					}
				}
				conn.setRequestProperty("Accept-Charset", CHARSET);
				conn.setRequestProperty("Accept", ACCEPT);
				conn.setRequestProperty("Accept-Encoding", ACCEPT_ENCODING);
				conn.setInstanceFollowRedirects(false);
				conn.setConnectTimeout(CONN_TIMEOUT);
				conn.setReadTimeout(READ_TIMEOUT);
				conn.connect();
				statusCode = conn.getResponseCode();

				switch (statusCode) {
					case HttpURLConnection.HTTP_MOVED_PERM:
					case HttpURLConnection.HTTP_MOVED_TEMP:
						String location = conn.getHeaderField("Location");
						// location = URLDecoder.decode(location, "UTF-8");
						URL base = new URL(url.toString());
						url = new URL(base, location);  // Deal with relative URLs
                        netInputStream = conn.getInputStream();
                        netInputStream.close();
						conn.disconnect();
						continue;
				}
				break;
			}

			// At this level, only handles http status 200
 			if (statusCode != 200) {
				// Report failure from server
				throw new IOContentException("HTTP " + statusCode + " error: " + conn.getResponseMessage());
			}
			//contentLength = conn.getContentLengthLong();  /* Not for Android builds */
 			contentLength = conn.getContentLength();
			contentType = conn.getContentType();
			contentEncoding = conn.getContentEncoding();
			netInputStream = conn.getInputStream();
			if (contentLength > MAXCONTENT) {
				// Report failure
				throw new IOContentException("Content length (" + contentLength + ") from server exceeded maximum");
			}			
			if (!allowMinusContentLength && (contentLength < 0)) {
				// Report failure
				throw new IOContentException("Content length (" + contentLength + ") from server invalid");
			}

		} catch (IOException e) {
			handleException(e);
			statusCode = -1;
			//if (e instanceof IOContentException) {
			//	conn.disconnect();
			//	conn = null;
			//}
		}
		return (statusCode == 200);
	}

	public URL getUrl() {
		return url;
	}

	@Override
	public void closeConnection() {
		if (conn != null) {
			conn.disconnect();
			conn = null;
		}
		super.closeConnection();
	}

}	
