/**
 * License: GPL v3. Copyright 2015 Headuck 
 */

package com.headuck.datastore.net;

import com.headuck.datastore.DataStore;
import com.headuck.datastore.instream.CountingInputStream;
import com.headuck.datastore.instream.ICountProgressHandler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedInputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;

public abstract class UrlDownloader implements ICountProgressHandler {

	final String USER_AGENT = DataStore.NAME + "/" + DataStore.VERSION + " " + System.getProperty("http.agent");
	final String ACCEPT = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
	final String ACCEPT_ENCODING = "gzip";
	final String CHARSET = "ISO-8859-1,utf-8";
	final int CONN_TIMEOUT = 10000;
	final int READ_TIMEOUT = 20000;
	final int MAXCONTENT = 1000000;
	final int MAXREAD = 10000000;
	final IHttpHandler handler;
	final String urlString; 
	boolean reported = false;
	public long contentLength;
	public String contentType;
	public int statusCode;
	protected String contentEncoding;
	protected InputStream netInputStream;
	protected CountingInputStream cs = null;
	static final Logger log = LoggerFactory.getLogger("UrlDownloader");
	
	public UrlDownloader (final String urlString, final IHttpHandler handler) 
	{
		this.handler = handler;	
		this.urlString = urlString;
	}

	protected class IOContentException extends IOException { 	

		private static final long serialVersionUID = 1L;
		protected IOContentException (final String message)
		{
			super(message);
		}
	}

	protected class SSLContextMissingException extends IOException {

		private static final long serialVersionUID = 2L;
		protected SSLContextMissingException (final String message)
		{
			super(message);
		}
	}
	
	abstract public boolean openConnection();
	abstract public boolean openConnection(boolean allowMinusContentLength);
	abstract public void setSSLContext (String pubKey, String[] CACert);


	public InputStream getInputStream()
	{
		InputStream is = null;
		InputStream zips = null;		

		if (reported) return null;
		cs = new CountingInputStream(netInputStream, this); 
		try {
			if ("gzip".equals(contentEncoding)) {
				zips = new GZIPInputStream(cs);
			    is = new BufferedInputStream(zips);
			} else {
				is = new BufferedInputStream(cs);
			}
		} catch (IOException e) {
			handleException(e);
		}
		return is;
	}
	
	public long getByteCount ()
	{
		if (cs == null) return (-1);
		return cs.getByteCount();
	}
	
	public void onProgress (long byteRead) throws IOException, InterruptedException {
		if (contentLength != -1) {
			if (byteRead > contentLength) {
				throw new IOException("HTTP read error: Content length too long");
			}
		}
		handler.onProgress(byteRead, contentLength);
	}
		
	protected void handleException(Exception e) {
		// e.printStackTrace();
		handler.onFailure(/* e.getClass().getSimpleName() + ":" + */ e.getMessage());
		reported = true;
	}

	protected void closeConnection() {
		if (!reported) {
			handler.onSuccess();
		}
	}
	
	protected static void close(Closeable c) {
		if (c == null) return; 
		try {
			c.close();
		} catch (IOException e) {
			log.warn("Error in closing", e);
			//e.printStackTrace();
		}
	}

}
