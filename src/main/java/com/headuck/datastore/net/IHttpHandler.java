/**
 * License: GPL v3. Copyright 2015 Headuck 
 */


package com.headuck.datastore.net;

/**
 * Interface for callback handlers of Url Downloader
 *
 */
public interface IHttpHandler {
		void onSuccess ();
		void onFailure (String message);
		void onProgress (long progress, long total) throws InterruptedException;
}
