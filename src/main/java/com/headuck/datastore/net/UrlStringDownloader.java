package com.headuck.datastore.net;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class to submit an url string and download contents in a String
 * The String length is limited to 60000 bytes
 */
public class UrlStringDownloader {
    static final Logger log = LoggerFactory.getLogger("UrlStringDL");
    public String errorMsg = null;
    public URL url = null;
    static final int READ_LIMIT = 60000;
    /**
     * Handler for http downloader
     *
     */
    private class LocalHttpHandler implements IHttpHandler {

        public void onSuccess() {
            log.info ("HTTP connection reported success");
        }

        public void onFailure(String message) {
            log.info ("HTTP Failure: {}", message);
            errorMsg = message;
        }

        public void onProgress(long progress, long total) throws InterruptedException {
            log.debug ("Progress {} of {}", progress, total);
        }
    }

    /**
     * Send an http request with the url request, and return the String read from network
     * @param httpUrl url to be sent
     * @return string read
     * @throws IOException
     */
    public String getHTTPString(final String httpUrl) throws IOException {

        JavaUrlDownloader downloader = null;
        InputStreamReader reader = null;
        InputStream nets = null;
        int total = 0;

        try {
            LocalHttpHandler localHandler = new LocalHttpHandler();
            errorMsg = null;
            StringBuilder sb = null;
            downloader = new JavaUrlDownloader(httpUrl, localHandler);
            downloader.setDefaultSSLContext();
            if (downloader.openConnection(true)) {
                nets = downloader.getInputStream();
                // Save the input stream to a String
                reader = new InputStreamReader(nets, "UTF-8");
                sb = new StringBuilder();
                char[] buffer = new char[1024];
                int readLen;
                while ((readLen = reader.read(buffer)) >= 0) {
                    sb.append(buffer, 0, readLen);
                    total += readLen;
                    if (total > READ_LIMIT) break;
                }
                url = downloader.getUrl();
                return sb.toString();
            }
        }
        finally
        {
            if (downloader != null) {
                downloader.closeConnection();

            }
            if (reader != null) {
                reader.close();
            }
            if (nets != null) {
                nets.close();
            }
        }
        return null;
    }
}