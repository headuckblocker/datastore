package com.headuck.datastore.data;

import com.headuck.datastore.data.lmdb.LMDBTable;

public class DBFactory extends StoreFactory {
	
	protected String path;					// path of db 
	protected String idxName = null;		// db name for index
	protected String idxField = null;      // field name of record for use as index
	protected boolean dupSort = false;     // whether duplicate secondary index is allowed  

    public DBFactory (String path, String dbName) {
    	super(dbName);
    	this.path = path;
    }
          
    public DBFactory (String path, String dbName, String idxName, String idxField, boolean dupSort) {
    	this(path, dbName);
    	this.idxName = idxName;
    	this.idxField = idxField;
    	this.dupSort = dupSort;
    }

	public String getIndexField() {
		return idxField;
	}

    @Override
	public DBTable openStorage(String context, boolean readonly, boolean create) {
		// TODO: use reflection to allow configurable DB class
		return new LMDBTable(context, path, dbName, primaryField, idxName, idxField, readonly, create, dupSort);
	}
}

