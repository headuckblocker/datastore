package com.headuck.datastore.data;

import java.util.TreeMap;

public class HashStorage extends Storage {

	TreeMap<String, Record> storageMap = null;
	
	public HashStorage(String dbName, String primaryField, TreeMap<String, Record> storageMap) {
		super(dbName, primaryField, false);
		this.storageMap = storageMap;
	}

	@Override
	public Record get(final byte[] key, boolean isIdx) {
		assert (!isIdx);
		return storageMap.get(new String(key));
	}

	@Override
	public void put(final byte[] key, final Record object) {		
		super.put(key, object);
		// Remove primary field. For consistency with db semantics but not strictly necessary.
		byte[] objectbuf = object.toBytes(primaryFieldBuf);
		Record newobj = new Record(objectbuf);
		storageMap.put(new String(key), newobj);
	}
	
	@Override
	public boolean delete(final byte[] key, final boolean isIdx) {
		assert (!isIdx);
		super.delete(key, isIdx);
		if (storageMap.remove(key) == null) {
			return false;
		}
		return true;
	}
	
	@Override
	public long count() {
		if (storageMap == null) return 0;
		return storageMap.size();
	}
	
	@Override
	public void close() {
		storageMap = null;
	}
	
	@Override
	public void close(boolean abort) {
		close();
	}

}
