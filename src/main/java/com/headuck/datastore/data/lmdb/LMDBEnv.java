package com.headuck.datastore.data.lmdb;

import com.headuck.datastore.data.DBException;
import com.headuck.datastore.utils.Utils;

import org.fusesource.lmdbjni.Constants;
import org.fusesource.lmdbjni.Database;
import org.fusesource.lmdbjni.Env;
import org.fusesource.lmdbjni.LMDBException;
import org.fusesource.lmdbjni.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map.Entry;


public class LMDBEnv {
	static final Logger log = LoggerFactory.getLogger("LMDBEnv");

	static final int MAX_DB = 16;				// Max number of DB per Environment
	static final long MAP_SIZE = 10485760*10;   // Map Size for each environment

	
	// Class variables
	// Access to the HashMap should be synchronized by lock 
	static HashMap <String, EnvStruct> envMap = new HashMap <String, EnvStruct>();
	static final Object lock = new Object();  // global lock to assess env map
		
	// Instance variables
	String path;
	String context;
	// Modification to envStruct which will be a shared reference needs to be synchronized by the instance object 
	EnvStruct envStruct = null;
	// For checking
	Transaction writeTxnAcquired = null;


	protected static class EnvStruct {
		private Env env;
		private volatile int count;
		private volatile int writeTxn = 0;
		private HashMap<String, TransactionStruct> contextTxn = new HashMap<String, TransactionStruct>();
		private HashMap<String, Database> envDatabase = new HashMap<String, Database>();
		private Database metaDb = null; 
				
		public EnvStruct (Env env, int count) {
			this.env = env;
			this.count = count;   // ref count
		}
		
		public synchronized void incCount() {			
			count ++;
		}
		
		public synchronized int decCount() {
			--count;
			if (count == 0) {
				envCleanUp();
			}
			return count;
		}

		void envCleanUp() {
			// warns and closes all remaining transaction (commit) if any
			log.info("Cleaning up & closing DB environment");
			for (Entry<String, TransactionStruct> entry : contextTxn.entrySet()) {
				log.warn("Transaction not closed under context : {}", entry.getKey());
				while (entry.getValue().decCount(false) > 0) {
					;// loop until 0
				}
			}
			if (env != null) {
				env.close();
				env = null;
			}
			// clean up
			contextTxn = null;
			envDatabase = null;
		}

		/**
		 * Open the database dbName
		 * prevent different threads opening database at the same time
		 * @param dbName database name to open
		 * @param create true to create and drop the old database
		 * @return LMDB handle to Database
		 */
		public synchronized Database openDatabase(final String dbName, final boolean create, final boolean dupSort) {
			Database db;
			log.info("==> Open db: {}", dbName);

			db = envDatabase.get(dbName);
			if (db != null) {
				log.info("==> return opened db: {}", dbName);
				return db;
			}
			try {
				int flags = 0;
				// Create if not found. 
				if (create) flags |= Constants.CREATE;
				// Dupsort as specified
				if (dupSort) flags |= Constants.DUPSORT;
				// Not using the env version of openDatabase w/o transaction, to avoid deadlock
				// caused by attempt to open write tx while another write tx is not yet closed
				//db = env.openDatabase(dbName, flags);
				db = envOpenDatabase(dbName, flags);
			} catch (LMDBException e) {
				throw new DBException("Database open error, dbName=" + dbName, e);
			}
			if (db != null) {
				envDatabase.put(dbName, db);
			} else {
				throw new DBException("Database open returned null, dbName=");
			}
			log.info("==> return new opened db: {}", dbName);
			return db;
		}

		private synchronized Database envOpenDatabase(final String dbName, int flags) {
			Database db = null;
			LMDBEnv.TransactionStruct txStruct = null;
			boolean openSuccess = false;
			try {
				if ((flags & Constants.CREATE) != 0) {
					txStruct = getWriteTransactionInternal("_openDb", 0);

				} else {
					txStruct = getReadTransactionInternal("_openDb");
				}
				if (txStruct != null) {
					txStruct.incCount();
					db = env.openDatabase(txStruct.tx, dbName, flags);
					openSuccess = true;
				}
			} finally {
				if(txStruct != null) {
                    try {
                        txStruct.decCount(!openSuccess);
                    } finally {
                        releaseWriteTransactionInternal(txStruct);
                    }
				}
			}
			return db;
		}

		public void openMetaDatabase() {			
			metaDb = openDatabase("meta", true, false);
		}
		
		public synchronized Transaction getReadTransaction(final String context)
		{
			log.info("==> Get Read transaction: context = {}", context);
			TransactionStruct txStruct = null;
			
			if (context != null) {
				// check if tx for the context already exists
				txStruct = contextTxn.get(context);
			}
			// if no context / existing context, create tx
			if (txStruct == null) {
				txStruct = getReadTransactionInternal(context);
				contextTxn.put(context, txStruct);
			}
			if (context != null) {
				txStruct.incCount();
			}
			log.info("==> Return read transaction: context = {}", context);
			return txStruct.tx;			
		}

		private synchronized LMDBEnv.TransactionStruct getReadTransactionInternal(String context) {
			LMDBEnv.TransactionStruct txStruct;
			// need to create a new write transaction
			Transaction tx = env.createReadTransaction();
			txStruct = new TransactionStruct (tx, 0, false);  // count will be increased after return
			log.info("Create read transaction in context {}", context);
			return txStruct;
		}

		public synchronized Transaction getWriteTransaction (final String context, int waitTime) 
		{
			log.info("==> Get write transaction: context = {}", context);
			TransactionStruct txStruct = null;
			
			if (context != null) {
				// check if tx for the context already exists
				txStruct = contextTxn.get(context);
				if (txStruct != null) {
					// if there is already a context transaction but it is not writable,  
					// throws exception. 
					if (writeTxn == 0) {
						throw new DBException("Cannot create write transaction in the current context " + context);
					}
				}
			}
			// if no context / existing context, create tx
			if (txStruct == null) {
				txStruct = getWriteTransactionInternal(context, waitTime);
			}
			if (context != null && txStruct != null) {
				txStruct.incCount();
				contextTxn.put(context, txStruct);
			}
			log.info("==> Return write transaction: context = {}", context);
			return (txStruct == null) ? null : txStruct.tx;		// could be null if interrupted
		}	

		private synchronized LMDBEnv.TransactionStruct getWriteTransactionInternal(String context, int waitTime) {
			LMDBEnv.TransactionStruct txStruct = null;
			// need to create a new write transaction
			while (true) {
				if (writeTxn == 0) {
					Transaction tx = env.createWriteTransaction();
					txStruct = new TransactionStruct (tx, 0, true);
					log.info("Create write transaction in context {}", context);
					writeTxn ++;
					break;
				}
				else {
					try {
						log.info("==> Wait write transaction: context = {}", context);
						this.wait(waitTime);
					} catch (InterruptedException e) {
						Thread.currentThread().interrupt();
						break;
					}
					if (writeTxn > 0) break;
				}
			}
			return txStruct;
		}
		/**
		 * 
		 * Get data from meta database of the environment. Should only be called within a transaction
		 * @param context transaction context
		 * @param key key for meta
		 * @return value from the meta database
		 */
		 
		public String getMeta (final String context, final String key) throws DBException {
			TransactionStruct txStruct = null;
			String ret;

			if (context != null) {
				// get the tx for the transaction
				txStruct = contextTxn.get(context);
				
				if ((txStruct != null) && (metaDb != null)) {
					byte[] byteKey = Utils.stringToBytes(key);
					byte[] byteVal = metaDb.get(txStruct.tx, byteKey);
					if (byteVal == null) return null;
					try {
						ret = new String (byteVal, "UTF-8");
					} catch (UnsupportedEncodingException e) {
						log.error("Unsupported encoding");
						ret = null;
					} 					
					return ret; 
				} else {
					if (txStruct == null) {
						throw new DBException("No transaction in context while accessing meta info");
					} else {
						throw new DBException("metaDb not initialised in context={} while accessing meta info");
					}
				}
			} else {
				throw new DBException("Null context in getMeta");
			}			
		}

		/**
		 * Put data in meta database of the environment.  Should only be called within a transaction
		 * @param context transaction context
		 * @param key key for meta
		 * @param value value for meta
		 * 
		 */
		public void putMeta (final String context, final String key, final String value) throws DBException {
			TransactionStruct txStruct = null;
			
			if (context != null) {
				// get the tx for the transaction
				txStruct = contextTxn.get(context);
				
				if ((txStruct != null) && (metaDb != null)) {
					if (txStruct.isWriteTxn) {
						byte[] byteKey = Utils.stringToBytes(key);
						byte[] byteVal = Utils.stringToBytes(value);
						metaDb.put(txStruct.tx, byteKey, byteVal);	
					} else {
						throw new DBException("Attempt to write to meta in a read only transaction");
					}
				} else {
					if (txStruct == null ) {
						throw new DBException("No transaction in attempt to write meta info");
					} else {
						throw new DBException ("metaDb not initialised in putMeta");
					}
				}
			} else {
				throw new DBException ("Null context in putMeta");
			}
		}
				
		/*
		 *  close transaction if there is no more references in this context 
		 */
		public synchronized void close(final String context, Transaction tx, boolean abort)   
		{
			TransactionStruct txStruct = contextTxn.get(context);
			if (txStruct == null) {
				throw new DBException("Context not found in closing transaction");
			}
			if (txStruct.tx != tx) {
				throw new DBException("Closing a different transaction than the transaction in context");
			}
			if (txStruct.decCount(abort) == 0) {
				contextTxn.remove(context);
				releaseWriteTransactionInternal(txStruct);
			}
		}

		private synchronized void releaseWriteTransactionInternal(LMDBEnv.TransactionStruct txStruct) {
			if (txStruct.isWriteTxn()) {
				writeTxn--;
				this.notify();
			}
		}

	}
	
	/** 
	 *  Class used by EnvStruct
	 *   
	 *  "count" in the class instance is used within synchronized block of EnvStruct and this 
	 *  should be no further need to synchronize
	 * 
	 */
	protected static class TransactionStruct {
		public Transaction tx;
		private volatile int count;
		private final boolean isWriteTxn;
		
		public TransactionStruct (Transaction tx, int count, boolean isWriteTxn) {
			this.tx = tx;
			this.count = count;   // ref count
			this.isWriteTxn = isWriteTxn;
		}
	
		public synchronized void incCount() {
			count ++;
		}

		private synchronized void decCountSync() {
			--count;
		}

		public int decCount(boolean abort) {
			decCountSync();
			if (count == 0) {
				// closes transaction normally. 
				if (tx != null) {
					if (!isWriteTxn || !abort) {
						tx.commit();
					} else {
						tx.abort();
					}						
				}
				tx = null;
			}
			return count;
		}
		
		public boolean isWriteTxn() {
			return isWriteTxn;
		}
	}

	/**
	 * Return an LMDBEnv structure representing the environment.  Reuse the underlying env
	 * if the already exists
	 * Each LMDBEnv should not be used across threads
	 * 
	 * @param path full directory of the database location. Will create path if not exists
	 */
	public LMDBEnv(final String context, final String path) {
		this.path = path;
		this.context = context;
		synchronized (lock) {
			envStruct = envMap.get(path);
			
			if (envStruct == null) {
				boolean succeed = false;
				try {
					// creates new LMDB Env
					Env env = new Env();					
					// create if null
					env.setMaxDbs(MAX_DB);  // Max number of databases
					env.setMapSize(MAP_SIZE); // Max size
					final File file = new File (path);
					if (!file.exists()) {
						// create path if not exists
						if (!file.mkdirs()) {
							throw new DBException("Cannot create directory for database, path=" + path);
						} else {
							log.info("Created directory {}", path);
						}
					} else if (!file.isDirectory()) {
						throw new DBException("Cannot create directory, file with the same name existss, path=" + path);
					} 
					try {
						env.open(path, Constants.NOTLS);
					} catch (LMDBException e) {
						throw new DBException("Database environment init error, path=" + path, e);
					}
					envStruct = new EnvStruct(env,1);

					log.info("New DB environment created, path = {}", path);
					
					envStruct.openMetaDatabase();
					succeed = true;
					envMap.put(path, envStruct);
				} finally {
					if (!succeed) {
						if (envStruct != null) {
							envStruct.envCleanUp();
							envStruct = null;
						}
					}
				}
			} else {
				envStruct.incCount();
			}
		}
	}

    /**
     * Open database
     * @param dbName database name
     * @param create true to allow creation of new db
     * @param dupSort allow duplicate keys
     * @return database opened
     */
	public Database openDb (String dbName, boolean create, boolean dupSort) {
		if (envStruct == null || envStruct.env == null) {
			throw new DBException("Open database: environment error, dbName=" + dbName);
		}
		return envStruct.openDatabase(dbName, create, dupSort);
	}
	
	/**
	 * Creates read transaction
	 * @return created transaction
	 */
	public Transaction getReadTransaction() {
		if (envStruct == null || envStruct.env == null) {
			throw new DBException("Create read txn: environment error");
		}
		return envStruct.getReadTransaction(context);
	}

	/**
	 * Creates write transaction, blocking
	 * @return created transaction, null if block interrupted
	 */
	public Transaction getWriteTransaction() {		
		return getWriteTransaction(0); 
	}

	/**
	 * Creates write transaction, blocking until timeout
	 * @param waitTime max wait time in ms for lock 
	 * @return created transaction, null if fail (interrupted or timeout)
	 */
	public Transaction getWriteTransaction(int waitTime) {
		if (envStruct == null || envStruct.env == null) {
			throw new DBException("Create write txn: environment error");
		}
		return envStruct.getWriteTransaction(context, waitTime);
	}
	
	public String getMeta (final String context, final String key) {
		if (envStruct == null || envStruct.env == null) {
			throw new DBException("getMeta: environment error");
		}
		return envStruct.getMeta(context, key);
	}
	
	public void putMeta (final String context, final String key, final String value) {
		if (envStruct == null || envStruct.env == null) {
			throw new DBException("putMeta: environment error");
		}
		envStruct.putMeta(context, key, value);
	}

	/**
	 * Close the environment
	 * Decrement count and close the LMDB Environment (env) under the LMDBEnv if this is the last 
	 * reference. Should be called before destruction of an LMDBEnv instance
	 */
	public void close() {
		if (envStruct == null) {
			throw new DBException("Close environment : null envStruct encountered");
		}
		cleanup();
	}
	
	protected void cleanup() {
		// clean up for this instance of LMDBEnv
		synchronized (lock) 
		{
			// cleanup before destruction of instance, if necessary
			if ((envStruct != null) && (envStruct.decCount() == 0)) {					
					envMap.remove(path); 
			}
		}
		envStruct = null;
	}
	
	/**
	 * Close the transaction, commit if it is the write transaction acquired 
	 * @param tx transaction to close
	 */
	public void close(Transaction tx) {
		close(tx, false); 	
	}

	/**
	 * Close the transaction
	 * @param tx transaction to close
	 * @param abort for write transaction only, true for abort, false for commit.
	 *        abort has no effect for read transaction
	 */
	
	public void close(Transaction tx, boolean abort) {
		if (tx == null) {
			throw new DBException("Attempt to close a null transaction");
		}
		log.info("Close Transaction in context {}", this.context);
		envStruct.close(this.context, tx, abort);
	}
	

	public void close(Database db) {
		if (db == null) {
			throw new DBException("Attempt to close a null database"); 
		}
		// No bookkeeping of db (yet). Note that this is not called if transaction was aborted
		// Closing of database is not recommended under LMDB
		// db.close();   
	}
}
