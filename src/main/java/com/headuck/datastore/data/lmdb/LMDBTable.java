package com.headuck.datastore.data.lmdb;

import com.headuck.datastore.data.DBException;
import com.headuck.datastore.data.DBTable;
import com.headuck.datastore.data.Record;
import com.headuck.datastore.utils.Utils;

import org.fusesource.lmdbjni.Constants;
import org.fusesource.lmdbjni.Cursor;
import org.fusesource.lmdbjni.Database;
import org.fusesource.lmdbjni.Entry;
import org.fusesource.lmdbjni.GetOp;
import org.fusesource.lmdbjni.LMDBException;
import org.fusesource.lmdbjni.NativeBuffer;
import org.fusesource.lmdbjni.SeekOp;
import org.fusesource.lmdbjni.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.NoSuchElementException;

public class LMDBTable extends DBTable {

	final Logger log = LoggerFactory.getLogger("LMDBTable");
	LMDBEnv env = null;
	Database db = null;
	Database dbIdx = null;
	Transaction tx = null;
	long recordCount = -1;
	// for writing
	final static int MAX_KEY_LENGTH = 511;   // from MDB_MAXKEYSIZE of lmdb
	final static int MAX_VALUE_LENGTH = 1000; 
	NativeBuffer nativeKey = null;
	NativeBuffer nativeIdx = null;
	NativeBuffer nativeValue = null;
	
	public LMDBTable(String context, String path, String dbName, String primaryField, 
			String idxName, String idxField, boolean readonly, boolean create) {
		this (context, path, dbName, primaryField, 
				 idxName, idxField, readonly, create, false);
	}
	
	public LMDBTable(String context, String path, String dbName, String primaryField, 
			String idxName, String idxField, boolean readonly, boolean create, boolean dupSort) {
		
		super(context, path, dbName, primaryField, idxName, idxField, readonly, create, dupSort);
		boolean success = false;
		try {
			env = new LMDBEnv(context, path);
			// When opening table, create if not found unless read only. 
			// (The create parameter for this method is for dropping the existing db)
			db = env.openDb(dbName, !readonly, false);   // DupSort flag only supported for index 
			log.info("Open LMDB {}", dbName);
			if (idxName != null) {
				dbIdx = env.openDb(idxName, !readonly, dupSort);
				log.info("Open Index {}", idxName);
			}
			success = true;
			
		} catch (LMDBException e) {
			throw new DBException("Database environment creation error", e);
		}
		finally {
			if (!success) {
				cleanupAbort();	
			}
		}
	}
	
	private void initTransaction () {
		boolean success = false;
		try {
			if (readonly) {
				tx = env.getReadTransaction();
			} else {
				tx = env.getWriteTransaction();	
				nativeKey = NativeBuffer.create(MAX_KEY_LENGTH);
				nativeIdx = NativeBuffer.create(MAX_KEY_LENGTH);
				nativeValue = NativeBuffer.create(MAX_VALUE_LENGTH);
			}
			
			if (!readonly && create) {
				db.drop(tx, false);
				log.info("Drop Database {}", dbName);
				recordCount = 0;
			} else {
				String recordCountStr = env.getMeta(context, dbName + ".count");
				if (recordCountStr != null) {
					recordCount = Long.parseLong(recordCountStr);
					log.info("Database {}: record count = {}", dbName, recordCount);
				} else {
					log.warn ("Cannot read record count, reset to 0");
					recordCount = 0;
				}
				
			}
			if (idxName != null) {
				if (!readonly && create) {
					dbIdx.drop(tx, false);
					log.info("Drop Index {}", idxName);
				}
			}
			success = true;
		} catch (LMDBException e) {
			throw new DBException("Database " + dbName + " init transaction error", e);
		} finally {
			if (!success) 
				cleanupAbort();
		}
	} 	
	
	
	/**
	 * Get information of type Record from opened database matching the key.
	 * Key is used for matching primary key or indexed key   
	 * For database with dupSort index and isIdx = true, return the first record matching the indexed key
	 * Use cursor instead to traverse database with dupSort index 
	 * 
	 * @return record, or null if not found
	 * @param keybuf Primary key if isIdx = false, indexed key if isIdx = true. In format of byte[]. 
	 */
	@Override
	public Record get(final byte[] keybuf, boolean isIdx) {
		
		Record record = null;
		byte[] byteKey = null;
		byte[] byteRecord = null;
		
		try {
			if (tx == null) {
				initTransaction ();
			}
			if (isIdx) {
				byteKey = dbIdx.get(tx, keybuf);
			} else {
				byteKey = keybuf;
			}
			if (byteKey != null) {
				byteRecord = db.get(tx, byteKey);
				if (byteRecord != null) {
					record = new Record(byteRecord);
				} else {
					if (isIdx) {
						throw new DBException("Indexed entry in index " + idxName 
								+ " not found in database " + dbName + ": Key = " + new String(keybuf));				
					}
				}
			}
		} 
		catch (LMDBException e) {
			throw new DBException("Error in reading record from " + dbName + ": Key = " + new String (keybuf), e);
		}
		return record;
	}

		
	@Override
	public void put(final byte[] keybuf, final Record record) {
		
		final int keylen;
		byte[] idxbuf = null;
		int idxlen=0;
		final byte[] objbuf;
		final int objlen;

		
		// exception if this LMDBTable is opened for read only
		super.put(keybuf, record);
		
		try {
			// Performs the equivalence of    
			//   db.put(tx, key.getBytes(), object.toBytes(primaryField));
			// with native buffer for efficiency
			// Uses the [primaryFieldBuf] field of Record as the primary key
			if (tx == null) {
				initTransaction ();
			}
			try {				
				//keybuf = Utils.stringToBytes(key);
				keylen = keybuf.length;
				nativeKey.write (0, keybuf, 0, keylen);
				
				objbuf = record.toBytes(primaryFieldBuf);
				objlen = objbuf.length;
				nativeValue.write (0, objbuf, 0, objlen);				
			} catch (IllegalArgumentException e) {
				log.warn("Key field / record too long in writing database, omitted : Key = {}", new String(keybuf));
				return;
			}
			
			if (idxName != null) {
				idxbuf = record.map.get(idxFieldBuf);
				if (idxbuf != null) {  // permits records w/o index field
					// Use native buffer to put secondary index for efficiency  
					try {				
						// idxbuf = Utils.stringToBytes(idx);
						idxlen = idxbuf.length;
						nativeIdx.write (0, idxbuf, 0, idxlen);
					} catch (IllegalArgumentException e) {
						log.warn("Index field too long in writing database, omitted : Key = " + new String(keybuf) 
								+ " Index = " + new String (idxbuf));
						return;
					}
				}
			}

			// Set the no-overwrite flag for accurate counting of primary database
			// This does not work for dup_sort primary db (the db.put did not check for NODUPDATA
			// but dupsort primary is not supported.
			byte[] dup = db.put (tx, nativeKey, keylen, nativeValue, objlen, Constants.NOOVERWRITE);
			if (dup != null) {
				// overwrite this time
				db.put (tx, nativeKey, keylen, nativeValue, objlen);
			} else {
				recordCount ++;
			}
			if (idxbuf != null) {
				// Uses the default behaviour: replacing previously existing key 
				// if not dupSort, or adding duplicate if dupSort
				boolean idxChanged;
				if (dup != null) {
					Record tmpRecord = new Record(dup);
					byte[] oldIdx = tmpRecord.map.get(idxFieldBuf);
					byte[] oldKey = keybuf;
					idxChanged = removeFromIdx(oldKey, oldIdx, keybuf, idxbuf);
				} else {
					idxChanged = true;
				}
				if (idxChanged) {
					// Replacing previously existing key
					// if not dupSort, or adding duplicate if dupSort
					dbIdx.put(tx, nativeIdx, idxlen, nativeKey, keylen);
				}
			}
		} catch (LMDBException e) {
			throw new DBException("Error in writing record from " + dbName + ": Key = " + new String(keybuf), e);			
		}
	}
	
	@Override
	public boolean delete(final byte[] keybuf, final boolean isIdx) {
		byte[] byteKey = null;
		byte[] idxbuf = null;
		boolean result;
		
		// exception if this LMDBTable is opened for read only
		super.delete(keybuf, isIdx);
		
		try {
			if (tx == null) {
				initTransaction ();
			}
			if (isIdx) {
				idxbuf = keybuf;
				byteKey = dbIdx.get(tx, idxbuf);				
			} else {
				byteKey = keybuf;
				if ((idxName != null) && (byteKey != null)) {
					byte[] byteRecord = db.get(tx, byteKey);
					if (byteRecord != null) {
						// TODO : This is inefficient although mass delete should be rare
						Record record = new Record(byteRecord);
						byte[] idxKey = record.map.get(idxFieldBuf);
						if (idxKey != null) {
							idxbuf = idxKey;    // permits records without idx field
						}
					} else {
						log.warn("Delete: Unable to find record in db {}, key={}", dbName, new String(keybuf));						
					}					
				}
			}
			if (byteKey != null) {			
				result = db.delete(tx, byteKey);	
				if (result) {
                    recordCount --;
                }
			} else {
				log.warn("Delete: Unable to find record in index {}, key = {}", dbIdx, new String(keybuf));
				result = false;
			}
			if (idxbuf != null) {
				boolean idxResult;
				if (!dupSort) {
					idxResult = dbIdx.delete(tx, idxbuf);
				} else {
					idxResult = dbIdx.delete(tx, idxbuf, byteKey);
				}
				if (!idxResult) {
					log.warn("Delete: Unable to find index for deletion: index {}, key = {}", dbIdx, new String(idxbuf));
				}
			}			
		} 
		catch (LMDBException e) {
			throw new DBException("Error in deleting record from " + dbName + ": Key = " + new String(keybuf), e);
		}
		return result;
	}
	
	@Override
	public long count() {
		if (recordCount == -1) {			
			if (tx == null) {
				// init transaction to read record count
				initTransaction ();
			}						
		}
		return recordCount;
	}
	
	@Override
	public DBCursor getCursor(boolean isIdx) {
		if (isIdx && (idxName == null)) {
			throw new DBException("getCursor: isIdx must be false for table without index for " + dbName);
		}
		try {
			if (tx == null) {
				initTransaction ();
			}
			if (!isIdx) {
				return new LMDBCursor(db.openCursor(tx), false);
			} else {
				//if (!dupSort) {
					return new LMDBCursor(dbIdx.openCursor(tx), true);
				//} else {
				//	return new LMDBCursorDup(dbIdx.openCursor(tx));
				//}
			}
		} catch (LMDBException e) {
			throw new DBException(e.getMessage(), e);
		}
	}

	public DBCursor getCursorDup(boolean isIdx) {
		if (isIdx && (idxName == null)) {
			throw new DBException("getCursor: isIdx must be false for table without index for " + dbName);
		}
		try {
			if (tx == null) {
				initTransaction ();
			}
			return new LMDBCursorDup(dbIdx.openCursor(tx));
		} catch (LMDBException e) {
			throw new DBException(e.getMessage(), e);
		}
	}

	@Override
	public void close() {
		super.close();
	}
	
	@Override
	public void close(boolean abort) {
		super.close(abort);
	}

	/**
	 * Clean up for failure with write transaction opened
	 * Write transaction will be aborted
	 */
	@Override 
	protected void cleanupAbort() {
		cleanup(true);
	}
		
	@Override
	protected void cleanup() {
		cleanup(false);
	}
	
	protected void cleanup(boolean abort) {
		if (!abort && !readonly) {
			if (env != null) {
				if (tx == null) {
					initTransaction ();
				}
				env.putMeta(context, dbName + ".count", Long.toString(recordCount));
				log.info("Saving count for {} = {}", dbName, recordCount);
			} else {
				log.info("Unable to saving count for {} ({}): env is null", dbName, recordCount);
			}			
		}
		if (nativeValue != null) nativeValue.delete();
		if (nativeIdx != null) nativeIdx.delete();
		if (nativeKey != null) nativeKey.delete();
		if (env != null) {
			try {				
				if (tx != null) {		
					log.info("Close Transaction for {}", this.dbName);
					env.close(tx, abort);					
					tx = null;
				}
				if (dbIdx != null) {
					if (!abort) {
						log.info("Close Index {}", this.idxName);
						env.close(dbIdx);						
					}
					dbIdx = null;
				}
				if (db != null) {
					if (!abort) {
						log.info("Close database {}", this.dbName);
						env.close(db);						
					}
					db = null;
				}
				log.info("Close success for {}", this.dbName); 
			} catch (LMDBException e) {
				log.error("DB Exception in closing", e);
				throw new DBException("Exception in closing database", e);
			} finally {
				env.close();		// close the env itself
				env = null;
			}
		} else {
			log.warn("Environment is null when closing database {}", this.dbName);
		}
	}

	private boolean removeFromIdx (byte[] oldKey, byte[] oldIdx , byte[] newKey, byte[] newIdx) {
		boolean idxRemoved = false;
		if (oldIdx != null) {  // permits records without idx field
			boolean idxResult;
			if (!dupSort) {
				if (newIdx == null || (Utils.compare(oldIdx, newIdx) != 0)) {
					idxResult = dbIdx.delete(tx, oldIdx);
					if (!idxResult) {
						log.warn("removeFromIdx: Unable to find index for deletion: index {}, key = {}", dbIdx, new String(oldIdx));
					}
					idxRemoved = true;
				}

			} else {
				if (newKey == null || newIdx == null ||
						(Utils.compare(oldIdx, newIdx) != 0) || (Utils.compare(oldKey, newKey) != 0)) {
					idxResult = dbIdx.delete(tx, oldIdx, oldKey);
					if (!idxResult) {
						log.warn("removeFromIdx: Unable to find index for deletion: index {}, key = {}", dbIdx, new String(oldIdx));
					}
					idxRemoved = true;
				}
			}

		}
		return idxRemoved;
	}

	/**
	 * Cursor class for LMDB Collection
	 * 
	 */
		
     class LMDBCursor extends DBCursor {

        protected final Cursor cursor;
        protected boolean deferredGetPrev = false;
		protected boolean modeIsForward = true;  // current lmdb cursor = next
        
		// Simple tuple class
		protected class LMDBKeyValue extends KeyValue<byte[]> {
			// store retrieved primary key when idx is true, since this is excluded in the record
			public byte[] primaryKey = null; 
			 
			public LMDBKeyValue(byte[] keybuf, byte[] value) {
				super(keybuf, value);
			}
			public LMDBKeyValue(byte[] keybuf, byte[] value, byte[] primaryKey) {
				super(keybuf, value);
				this.primaryKey = primaryKey;
			}
			@Override
			public Record toRecord() {
				Record r = new Record (value);
				if (primaryKey != null) {
					r.map.put(primaryFieldBuf, primaryKey);
				}
				return r;
			}
		}
		
        private LMDBCursor(Cursor cursor, boolean isIdx) {
            this.cursor = cursor;
            this.isIdx = isIdx;
        }

        @Override
        public void close() throws IOException {
            cursor.close();
        }


		private boolean removeFromIdx (LMDBKeyValue oldValue, byte[] newKey, byte[] newIdx) {
			Record tmpRecord = oldValue.toRecord();
			byte[] oldIdx = tmpRecord.map.get(idxFieldBuf);
			byte[] oldKey = oldValue.key;
			return LMDBTable.this.removeFromIdx(oldKey, oldIdx, newKey, newIdx);
		}

        protected LMDBKeyValue cursorGet (final GetOp op) {
        	Entry entry = cursor.get(op);
        	if (entry == null) return null;
			byte[] byteKey;
			byteKey = entry.getKey();
			// NOTE: key is null for GetOp.LAST_DUP and GetOp.FIRST_DUP. Reread using
			// GetOp.GET_CURRENT
			if (byteKey == null) {
				entry = cursor.get(GetOp.GET_CURRENT);
				byteKey = entry.getKey();
			}
        	final byte[] value = entry.getValue();
        	if (isIdx) {
        		final byte[] value1 = db.get(tx, value);
        		if (value1 == null) {
					throw new DBException("Indexed entry in index " + idxName
								+ " not found in database " + dbName + ": Key = "  + ((byteKey == null)? "null" : new String (byteKey)));
        		}
        		return new LMDBKeyValue (byteKey, value1, value);
        	} else 
        		return new LMDBKeyValue (byteKey, value);
        }
        
        protected LMDBKeyValue cursorSeek (SeekOp op, byte[] bytes) {
        	Entry entry = cursor.seek(op, bytes);
        	if (entry == null) return null;
        	byte[] byteKey = entry.getKey();        	
        	byte[] value = entry.getValue();
        	if (isIdx) {
        		byte[] value1 = db.get(tx, value);
        		if (value1 == null) {
					throw new DBException("Indexed entry in index " + idxName 
							+ " not found in database " + dbName + ": Key = " + ((byteKey == null)? "null" : new String (byteKey)));
        		}
        		return new LMDBKeyValue (byteKey, value1, value);
        	} else
        		return new LMDBKeyValue (byteKey, value);
        }

        protected void cursorPut(byte[] keybuf, Record record) throws LMDBException {

            int keylen = keybuf.length;
            // System.out.println("cursorPut: keybuf = " + new String(keybuf) + " len = " + keylen);
            byte[] objbuf = null;
            int objlen = 0;
            byte[] idxbuf = null;
            int idxlen = 0;
            try {
                objbuf = record.toBytes(primaryFieldBuf);
                objlen = objbuf.length;
                // System.out.println("cursorPut: objbuf = " + new String(objbuf) + " len = " + objlen);
				nativeKey.write (0, keybuf, 0, keylen);
				nativeValue.write (0, objbuf, 0, objlen);
			} catch (IllegalArgumentException e) {
				log.warn("Key field / record too long in writing database, omitted : Key = {}", new String(keybuf));
				return;
			}

			if (idxName != null) {
				idxbuf = record.map.get(idxFieldBuf);
				if (idxbuf != null) {  // permits records w/o index field
					// Use native buffer to put secondary index for efficiency
					try {
						idxlen = idxbuf.length;
						nativeIdx.write (0, idxbuf, 0, idxlen);
					} catch (IllegalArgumentException e) {
						log.warn("Index field too long in writing database, omitted : Key = " + new String(keybuf)
								+ " Index = " + new String (idxbuf));
						return;
					}
				}
			}

			if (isIdx) {
                if (idxbuf != null) {
                    // Cursor is on index field. Verify the key and index is the same
                    Entry entry = cursor.get(GetOp.GET_CURRENT);
                    if (Utils.compare(entry.getKey(), idxbuf) != 0) {
                        throw new LMDBException("Index mismatch in cursor put of index cursor");
                    }
                    if (Utils.compare(entry.getValue(), keybuf) != 0) {
                        throw new LMDBException("Primary key field mismatch in cursor put of index cursor");
                    }
                    // This is ensured: cursor.put(nativeIdx, nativeKey, 0);
                    // Update the main table.  Given that the key does not change!
                    db.put(tx, nativeKey, keylen, nativeValue, objlen);
                } else {
                    log.warn("Index field is null when putting index cursor. Not supported");
                    return;
                }
            } else {
                Entry entry = cursor.get(GetOp.GET_CURRENT);
                if (Utils.compare(entry.getKey(), keybuf) != 0) {
                    throw new LMDBException("Key mismatch in cursor put");
                }
                cursor.put(nativeKey, keylen, nativeValue, objlen, 0);
                // Update index
                if (idxbuf != null) {
                    // Check if index has changed. If so, replace the previous index entry
                    LMDBKeyValue tmpKeyValue = new LMDBKeyValue(entry.getKey(), entry.getValue());
                    boolean idxChanged = removeFromIdx(tmpKeyValue, keybuf, idxbuf);
                    if (idxChanged) {
                        // Replacing previously existing key
                        // if not dupSort, or adding duplicate if dupSort
                        dbIdx.put(tx, nativeIdx, idxlen, nativeKey, keylen);
                    }
                }
			}
		}

        /**
         * Internal method for Delete record
         * Note: the cursor must already be positioned at the same record as the retrieved keyValue
         * @param keyValue LMDBKeyValue for deletion
         */
        protected void cursorDelete(LMDBKeyValue keyValue) {
            if (isIdx) {
                // Get the primary key
                byte[] byteKey = keyValue.primaryKey;
                // delete the primary db record
                boolean result = db.delete(byteKey);
                if (result) {
                    recordCount --;
                } else {
                    log.warn("Delete: Unable to find primary db for deletion: index {}, key = {}", dbIdx, new String(byteKey));
                }
            } else {
                if (idxName != null) {
                    // Need to remove the index
                    removeFromIdx(keyValue, null, null);
                }
            }
            // delete the cursor record
            cursor.delete();
            if (!isIdx) {
                recordCount --;
            }
        }

		@Override
		public DBCursor seekPrev() {
			if (!hasPrev()) {
                throw new NoSuchElementException();
            }
            try {
            	if (deferredGetPrev) {
            		getDeferredPrev();
            	}
                curr = prev;
                next = prev;
            	prev = cursorGet(GetOp.PREV);
                if (modeIsForward) {
                    prev = cursorGet(GetOp.PREV);
                    modeIsForward = false;
                }
                return this;
            } catch (LMDBException e) {
                throw new DBException(e.getMessage(), e);
            }		
		}
		
		@Override
		public Record prev() {
			seekPrev();
			return curr.toRecord();
		}

		@Override
		public DBCursor seekNext() {
			if (!hasNext()) {
                throw new NoSuchElementException();
            }
            try {
                curr = next; 
                prev = next;
                next = cursorGet(GetOp.NEXT);
                if (!modeIsForward) {
                    next = cursorGet(GetOp.NEXT);
                    modeIsForward = true;
                }
                deferredGetPrev = false;
                return this;
            } catch (LMDBException e) {
                throw new DBException(e.getMessage(), e);
            }
		}
		
		@Override
		public Record next() {
			seekNext();
			return curr.toRecord();
		}
		
		@Override
        public DBCursor seekToFirst() {
            try {
            	curr = null;
                prev = null;
                next = cursorGet(GetOp.FIRST);
                deferredGetPrev = false;
                modeIsForward = true;
                return this;
            } catch (LMDBException e) {
                throw new DBException(e.getMessage(), e);
            }
        }

		@Override
        public DBCursor seekToLast() {
            try {
            	curr = null;
                next = null;
                prev = cursorGet(GetOp.LAST);
                deferredGetPrev = false;
                modeIsForward = false;
                return this;
            } catch (LMDBException e) {
                throw new DBException(e.getMessage(), e);
            }
        }

		@Override
		public DBCursor seek(final byte[] keybuf) {
          try {
                next = cursorSeek(SeekOp.RANGE, keybuf);
                deferredGetPrev = true;
                curr = null;
                modeIsForward = true;
                return this;
            } catch (LMDBException e) {
                throw new DBException(e.getMessage(), e);
            }			
		}

        @Override
        public void deleteNext() {
            if (next != null) {
                if (!modeIsForward) {
                    // the cursor is now at prev.
                    next = cursorGet(GetOp.NEXT);
                    modeIsForward = true;
                }
                cursorDelete((LMDBKeyValue)next);
                curr = null;
                next = cursorGet(GetOp.NEXT);
            } else {
                throw new NoSuchElementException("deleteNext called without next element");
            }
        }

        @Override
        public void deletePrev() {
            if (deferredGetPrev) {
                getDeferredPrev();
            }
            if (prev != null) {
                if (modeIsForward) {
                    // the cursor is now at next.
                    prev = cursorGet(GetOp.PREV);
                    modeIsForward = false;
                }
                cursorDelete((LMDBKeyValue)prev);
                curr = null;
                prev = cursorGet(GetOp.PREV);
            } else {
                throw new NoSuchElementException("deletePrev called without prev element");
            }
        }

		@Override
		public void putNext(Record record) {
            try {
                if (next != null) {
                    if (!modeIsForward) {
                        // the cursor is now at prev.
                        next = cursorGet(GetOp.NEXT);
                        modeIsForward = true;
                    }
                    cursorPut(next.key, record);
                    next = cursorGet(GetOp.GET_CURRENT);
                } else {
                    throw new NoSuchElementException("putNext called without next element");
                }
            } catch (LMDBException e) {
                throw new DBException("Cannot execute put Next for cursor", e);
            }
        }

		@Override
		public void putPrev(Record record) {
            if (deferredGetPrev) {
                getDeferredPrev();
            }
            try {
                if (prev != null) {
                    if (modeIsForward) {
                        // the cursor is now at next.
                        prev = cursorGet(GetOp.PREV);
                        modeIsForward = false;
                    }
                    cursorPut(prev.key, record);
                    prev = cursorGet(GetOp.GET_CURRENT);;
                } else {
                    throw new NoSuchElementException("putPrev called without prev element");
                }
            } catch (LMDBException e) {
                throw new DBException("Cannot execute put Prev for cursor", e);
            }
		}

		// Implements deferred getting of "prev" Record
		// To get previous record only on demand after seek
        // deferredGetPrev implies modeIsForward
		
		private void getDeferredPrev() {
	        prev = cursorGet(GetOp.PREV);
	        modeIsForward = false;
	        deferredGetPrev = false;
		}
		
		@Override
        public boolean hasPrev() {
        	if (deferredGetPrev) {
        		getDeferredPrev();
        	}
        	return super.hasPrev();
        }

		@Override
        public Record peekPrev() {
        	if (deferredGetPrev) {
        		getDeferredPrev();
        	}
        	return super.peekPrev();
        }
    }

    /**
	 * Cursor class for LMDB Collection with DupSort index
	 * 
	 */
    
    class LMDBCursorDup extends LMDBCursor {
        
        private LMDBCursorDup(Cursor cursor) {
        	// Dup supports index only
        	super(cursor, true);
        }
        
        /*
         *  Version of cursorSeek with value specified.
         */
        
        protected LMDBKeyValue cursorSeek (SeekOp op, byte[] bytes, byte[] valueBytes) {
        	Entry entry = cursor.seek(op, bytes, valueBytes);
        	if (entry == null) return null;
        	byte[] byteKey = entry.getKey(); 
        	byte[] value = entry.getValue();
        	if (isIdx) {
        		value = db.get(tx, value);
        		if (value == null) {
					throw new DBException("Indexed entry in index " + idxName 
							+ " not found in database " + dbName + ": Key = " + ((byteKey == null)? "null" : new String (byteKey)));
        		}
        	}        	
        	return new LMDBKeyValue (byteKey, value);
        }
        
        /* 
         * The seek Prev / Next operations are the same except that
         * The DUP variants are used for the lmdb seek operations
         *  
         */
        
		@Override
		public DBCursor seekPrev() {
			if (!hasPrev()) {
                throw new NoSuchElementException();
            }
            try {
            	if (deferredGetPrev) {
            		getDeferredPrev();
            	}
                curr = prev;
                next = prev;
            	prev = cursorGet(GetOp.PREV_DUP);
                if (modeIsForward) {
                    prev = cursorGet(GetOp.PREV_DUP);
                    modeIsForward = false;
                }
                return this;
            } catch (LMDBException e) {
                throw new DBException(e.getMessage(), e);
            }		
		}

		@Override
		public DBCursor seekNext() {
			if (!hasNext()) {
                throw new NoSuchElementException();
            }
            try {
                curr = next; 
                prev = next;
                next = cursorGet(GetOp.NEXT_DUP);
                if (!modeIsForward) {
                    next = cursorGet(GetOp.NEXT_DUP);
                    modeIsForward = true;
                }
                deferredGetPrev = false;
                return this;
            } catch (LMDBException e) {
                throw new DBException(e.getMessage(), e);
            }
		}

		/**
		 *  Seek to the first data item of the current key 
		 */
		@Override
        public DBCursor seekToFirst() {
            try {
            	curr = null;
                prev = null;
                next = cursorGet(GetOp.FIRST_DUP);
                deferredGetPrev = false;
                modeIsForward = true;
                return this;
            } catch (LMDBException e) {
                throw new DBException(e.getMessage(), e);
            }
        }

		/**
		 *  Seek to the last data item of the current key 
		 */
		@Override
        public DBCursor seekToLast() {
            try {
            	curr = null;
                next = null;
                prev = cursorGet(GetOp.LAST_DUP);
                deferredGetPrev = false;
                modeIsForward = false;
                return this;
            } catch (LMDBException e) {
                throw new DBException(e.getMessage(), e);
            }
        }

		
		/**
		 *  Seek to the first data item of the next key 
		 */
        public DBCursor seekToNextKeyFirst() {
            try {
            	curr = null;
                prev = null;
                next = cursorGet(GetOp.NEXT_NODUP);
                deferredGetPrev = false;
                modeIsForward = true;
                return this;
            } catch (LMDBException e) {
                throw new DBException(e.getMessage(), e);
            }
        }

		/**
		 *  Seek to the last data item of the prev key 
		 */
        public DBCursor seekToPrevKeyLast() {
            try {
            	curr = null;
                next = null;
                prev = cursorGet(GetOp.PREV_NODUP);
                deferredGetPrev = false;
                modeIsForward = false;
                return this;
            } catch (LMDBException e) {
                throw new DBException(e.getMessage(), e);
            }
        }

        /**
		 *  Seek to the specified key. First record for key with multiple values
		 *  Note that this use SeekOp.KEY, i.e. no record would be returned if key does not match
		 */        
        @Override
		public DBCursor seek(final byte[] keybuf) {
          try {
                next = cursorSeek(SeekOp.KEY, keybuf);
                deferredGetPrev = true;
                curr = null;
                modeIsForward = true;
                return this;
            } catch (LMDBException e) {
                throw new DBException(e.getMessage(), e);
            }			
		}
        
        /**
		 *  Seek to the specified key and value 
		 */
		public DBCursor seekToDupValue(final byte[] keybuf, final byte[] valbuf) {
          try {
                next = cursorSeek(SeekOp.BOTH_RANGE, keybuf, valbuf);
                deferredGetPrev = true;
                curr = null;
                modeIsForward = true;
                return this;
            } catch (LMDBException e) {
                throw new DBException(e.getMessage(), e);
            }			
		}

        @Override
        public void deleteNext() {
            throw new DBException("Not yet implemented");
        }

        @Override
        public void deletePrev() {
            throw new DBException("Not yet implemented");
        }

		// Implements deferred getting of "prev" Record 
		// To get previous record only on demand after seek
		
		private void getDeferredPrev() {
	        prev = cursorGet(GetOp.PREV_DUP);
            modeIsForward = false;
            deferredGetPrev = false;
		}

    }
}
