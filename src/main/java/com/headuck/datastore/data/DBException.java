package com.headuck.datastore.data;


public class DBException extends RuntimeException {
    private static final long serialVersionUID = 2;
    private Throwable cause;

    /**
     * Constructs a DBException with an explanatory message.
     *
     * @param message
     *            Detail about the reason for the exception.
     */
    public DBException(String message) {
        super(message);
    }

    /**
     * Constructs a new DBException with the specified cause.
     * @param cause The cause.
     */
    public DBException(Throwable cause) {
        super(cause.getMessage());
        this.cause = cause;
    }

    /**
     * Constructs a new DBException with a message and a specified cause.
     * @param cause The cause.
     */
    public DBException(String message, Throwable cause) {
        super(message);
        this.cause = cause;
    }

    
    /**
     * Returns the cause of this exception or null if the cause is nonexistent
     * or unknown.
     *
     * @return the cause of this exception or null if the cause is nonexistent
     *          or unknown.
     */
    @Override
    public Throwable getCause() {
        return this.cause;
    }
}