package com.headuck.datastore.data;

import com.headuck.datastore.json.JSONException;
import com.headuck.datastore.json.JSONTokener;
import com.headuck.datastore.utils.Utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Class for internal representation of a database record (key-value pairs). <p/>
 * 
 * Format returned by XMLRuleParser, with 
 * reader / writer from / to byte[] for database storage.
 * 
 * Unless {@link String Record.Data#pushHash(String, String)} is called to put key-value attribute pairs,
 * a byte buffer is used to store the raw byte data in linear format
 * [key bytes in UTF8] [0] [value bytes in UTF8] [0] ... <p/>
 * 
 * This is to avoid repeated conversion UTF8 <--> 
 * internal representation, as well as creation of a large number of
 * String objects, for space and time efficiency.
 * 
 * Methods are NOT thread safe.
 */

public class Record {
	final static Logger log = LoggerFactory.getLogger("Record");

	protected final static int DEFAULT_SIZE = 100;
	
	// class for the data
	public class Data  {
		
		HashMap <String, String> hashMap = null; 
		// ByteArrayOutputStream outputStream;
		ByteArrayOutputStream tmpOutputStream; 

		byte[] buf;
		int bufidx;
		int cursize;
		int hashcache = 0;
		boolean bufInvalid = false;  // hash is in use and is dirty 
		
		public Data() {
			//outputStream = new ByteArrayOutputStream(DEFAULT_SIZE);
			buf = new byte[DEFAULT_SIZE];
			bufidx = 0;  // len & next byte to write
			cursize = DEFAULT_SIZE;
		}
		
		private void dataWrite (byte b) {
			if (bufidx < cursize) {
				buf[bufidx] = b;
			} else {
				realloc(DEFAULT_SIZE);
				buf[bufidx] = b;
			}
			bufidx ++;
		}
		
		private void dataWrite (byte[] b) {
			if (bufidx + b.length < cursize) {
				System.arraycopy(b, 0, buf, bufidx, b.length);
			} else {
				realloc (DEFAULT_SIZE + bufidx + b.length - cursize);
				System.arraycopy(b, 0, buf, bufidx, b.length);
			}
			bufidx += b.length;
		}
		
		private void realloc (int size) {
			cursize += size;		
			byte[] buf1 = new byte[cursize];
			System.arraycopy(buf, 0, buf1, 0, buf.length);
			buf = buf1;
		}
		
		public Data(byte[] byteBuf) {
			this ();
			dataWrite(byteBuf);
		}
		
		/**
		 * Add key-value pair to internal hash. Create the hash 
		 * and convert existing buffer to hash if necessary.
		 *
		 * @param key Key
		 * @param val Value
		 */
		public String putHash (String key, String val) {	

			if (hashMap == null) {
				hashMap = new HashMap<String, String>();
				bufToHash();
			}
			bufInvalid = true;
			return hashMap.put (key, val);
		}

		/**
		 * Remove key-value pair from internal hash. Create the hash
		 * and convert existing buffer to hash if necessary.
		 * Return replaced value
		 *
		 * @param key Key
		 */
		public String removeHash (String key) {

			if (hashMap == null) {
				hashMap = new HashMap<String, String>();
				bufToHash();
			}
			bufInvalid = true;
			return hashMap.remove (key);
		}

		/**
		 * Directly add key-value to internal buffer, WITHOUT checking for duplicate key
		 * @param key Key
		 * @param val Value
		 * @throws DBException if hash is being used instead of the internal buffer
		 */
		public void put (String key, String val) {
			put (key, val, true);					
		}

		/**
		 * Directly add key-value to internal buffer, WITHOUT checking for duplicate key
		 * @param key Key
		 * @param val Value
		 * @throws DBException if hash is being used instead of the internal buffer
		 */
		public void put (byte[] key, byte[] val) {
			put (key, val, true);					
		}
		
		/**
		 * Directly add key-value to internal buffer, WITHOUT checking for duplicate key
		 * @param key Key
		 * @param val Value
		 * @param check check for invalid buffer if true
		 * @throws DBException if hash is being used instead of the internal buffer 
		 *         in case check is true
		 */
		
		public void put (String key, String val, boolean check) {
			if (check && bufInvalid) {
				throw new DBException("Attempt to put to dirty record");
			}
			dataWrite(Utils.stringToBytes(key));				
			dataWrite((byte)0);
			dataWrite(Utils.stringToBytes(val));
			dataWrite((byte)0);					
		}
		
		/**
		 * Directly add key-value to internal buffer, WITHOUT checking for duplicate key
		 * @param key Key
		 * @param val Value
		 * @param check check for invalid buffer if true
		 * @throws DBException if hash is being used instead of the internal buffer 
		 *         in case check is true
		 */		
		public void put (byte[] key, byte[] val, boolean check) {
			if (check && bufInvalid) {
				throw new DBException("Attempt to put to dirty record");
			}
			dataWrite(key);				
			dataWrite((byte)0);
			dataWrite(val);
			dataWrite((byte)0);					
		}

		/**
		 * Save data from outputStream buffer to hash
		 */
		protected void bufToHash() {
			
			int len = bufidx;
			
			int lastpos = -1;
			String key1 = "";
			String val1;
			boolean nextIsKey = true;
			
			if (bufInvalid) {
				throw new DBException("bufToHash called while record buffer is invalid");
			}
			
			for (int i=0; i<len; i++) {
				if (buf[i] == 0) {
					if (nextIsKey) {					
						key1 = new String (buf, lastpos + 1, i - lastpos - 1);
					} else {					
						val1 = new String (buf, lastpos + 1, i - lastpos - 1);
						hashMap.put(key1, val1);
					}
					lastpos = i;
					nextIsKey = !nextIsKey;
				}
			}
			assert (nextIsKey);			
		}
		
		protected void hashToBuf() {
			if (hashMap == null) {
				throw new DBException("hashToBuf called while hash is invalid");
			}
			
			bufidx = 0;

			for (Map.Entry<String, String> entry : hashMap.entrySet()) {
			    String key = entry.getKey();
			    if (key != null) {
				    String value = entry.getValue();
				    if (value != null) {
						put (key, value, false);
				    }
			    }
			}

			bufInvalid = false;

		}
		
		/**
		 * Get the value for the 'key' attribute in the record. Input in form of a keybuf
		 * Use the internal hashMap if it is created, else get from the internal buffer 
		 * @param keybuf byte buffer for the key
		 * @return value in byte[] format
		 */
		public byte[] get(byte[] keybuf) {
			if (hashMap == null) {
				// get value from internal buffer				
				return getBufBytes(keybuf);
			}  
			String strVal = hashMap.get(new String(keybuf));
			if (strVal == null) return null;
			return Utils.stringToBytes(strVal);		
		}

		/**
		 * Get the value for the 'key' attribute in the record. Input in form of a String
		 * Use the internal hashMap if it is created, else get from the internal buffer 
		 * @param key key String
		 * @return value in byte[] format
		 */
		public byte[] get(String key) {
			if (hashMap == null) {
				// get value from internal buffer
				byte[] keybuf = Utils.stringToBytes(key);
				return getBufBytes(keybuf);
			} 
			String strVal = hashMap.get(key);
			if (strVal == null) return null;
			return Utils.stringToBytes(strVal);
		}
		
		/**
		 * String version of {@link #get(byte[] keybuf)}
		 * @param keybuf key
		 * @return String value
		 */
		public String getString(byte[] keybuf) {
			byte [] valbuf = get(keybuf);
			if (valbuf == null) return null;
			return new String(valbuf);
		}
		
		/**
		 * String version of {@link #get(String key)}
		 * @param key key
		 * @return String value
		 */
		public String getString(String key) {
			byte [] valbuf = get(key);
			if (valbuf == null) return null;
			return new String(valbuf);
		}
		
		
		/**
		 * Implementation of 'get' for the internal buffer, return the value for the provided key
		 * 
		 * @param keybuf key in byte[] format
		 * @return value in byte[] format
		 */
		protected byte[] getBufBytes (byte[] keybuf) {
			int len = bufidx;
			
			int lastpos = -1;
			boolean nextIsKey = true;
			boolean keymatch = false;
			
			for (int i=0; i<len; i++) {
				if (buf[i] == 0) {
					if (nextIsKey) {
						keymatch = true;
						// check if keybuf[] equals buf [offset = lastpos + 1; len=i-lastpos-1]
						if (keybuf.length == i - lastpos - 1) {
							for (int k = keybuf.length - 1 , j = i - 1; k >= 0; j--, k--) {
								if (keybuf[k] != buf[j]) {
									keymatch = false;
									break;
								}
							}
						} else {
							keymatch = false;
						}

					} else {
						if (keymatch) {
							int valLen = i - lastpos - 1;
							byte[] val1 = new byte[valLen]; 
							System.arraycopy(buf, lastpos + 1, val1, 0, valLen);
							return val1;
						}
					}
					lastpos = i;
					nextIsKey = !nextIsKey;
				}
			}
			assert (nextIsKey);
			return null;
		}
		
		/**
		 * Convert to raw byte format for saving to db
		 * @param omitKeybuf key-value pair with name omitField will be omitted
		 * @return byte array
		 */
		public byte[] toBytes(final byte[] omitKeybuf) {
			
			
			//if (tmpOutputStream == null) {
			//	tmpOutputStream =  new ByteArrayOutputStream(DEFAULT_SIZE);
			//} else {
			//	tmpOutputStream.reset();
			//}
			
			// If Hash Map is not null, convert it to outputStream first
			if (hashMap != null) {
				hashToBuf();
			}
			
			//updateBuf();
			if (omitKeybuf != null)
			{
				//byte[] omitKeybuf = Utils.stringToBytes(omitField);
				int len = bufidx;					
				int lastpos = -1;
				int lastkeypos = -1;
				// String key1 = "";
				//String val1;
				boolean nextIsKey = true;
				boolean keymatch = false;
					
				for (int i=0; i<len; i++) {
					if (buf[i] == 0) {
						if (nextIsKey) {
							keymatch = true;
							// check if omitField = buf [pos= lastpos + 1 len= i - lastpos - 1]
							if (omitKeybuf.length == i - lastpos - 1) {
								for (int k = omitKeybuf.length - 1 , j = i - 1; k >= 0; j--, k--) {
									if (omitKeybuf[k] == buf[j]) {
										continue;
									} else {
										keymatch = false;
										break;
									}
								}
							} else {
								keymatch = false;
							}
							// key1 = new String (buf, lastpos + 1, i - lastpos - 1);
							lastkeypos = lastpos + 1;
						} else {
							if (keymatch) {
								// exclude buf [pos = lastkeypos, len = i - lastkeypos + 1] from saved string
								// (including the trailing 0s)
								int newbuflen = len - (i - lastkeypos + 1); 
								// two blocks from buf to be copied: 
								// 0 to lastkeypos - 1;  and i + 1 to len - 1, either may be null
								byte[] outBuf = new byte[newbuflen];
								if (lastkeypos > 0) {
									System.arraycopy(buf, 0, outBuf, 0, lastkeypos);
								}
								if ((len - i) > 2) {
									System.arraycopy(buf,i+1, outBuf, lastkeypos, len - i - 2);
								}
								return outBuf;
								//return (new String (buf, lastpos + 1, i - lastpos - 1));							
							}
						}
						lastpos = i;
						nextIsKey = !nextIsKey;
					}
				}
				assert (nextIsKey);
			}
			byte[] outBuf = new byte[bufidx];
			System.arraycopy(buf,0, outBuf, 0, bufidx);
	        return outBuf;
		}
		
		@Override
		public String toString () {
			if (hashMap == null) {
				hashMap = new HashMap<String, String>();
				bufToHash();
			}
			
			boolean first = true;
			sb = new StringBuilder("{");		

			for (Map.Entry<String, String> entry : hashMap.entrySet()) {
			    String key = entry.getKey();
			    if ((key != null) /* && !key.equals(omitField) */) {
				    String value = entry.getValue();
				    if (value != null) {
				    	if (first) {	    		
				    		first = false;
				    	} else {
				    		sb.append(",");
				    	}
				    	// unquoted key
				    	sb.append(key);
				    	sb.append(":");
				    	appendQuotedString(value);
				    }
			    }
			}
	    	sb.append("}");
	    	
	    	// bytes = Utils.stringToBytes(sb.toString());
	        return sb.toString();
		}
		
		/**
		 * For the hash code in recycler view, for identity purpose 
		 * 
		 * @return hash
		 */
		
		public long getHash () {
			if (hashcache != 0) return hashcache;
			if (hashMap != null) {
				hashcache = hashMap.hashCode();				
			} else {				
				hashcache = Arrays.hashCode(buf);
			}
			return hashcache;
		}
		
	}
	
	
	//public HashMap<String, String> map = new HashMap<String, String>();
	public Data map;
	private StringBuilder sb = null;
	
	/* 
	 * Uninitialized record
	 */
	public Record() {
		map = new Data ();
	}
	
	/* 
	 * Create Record from raw byte[] format from DB
	 */
	public Record(byte[] bytes) {
		//this.readFromJSON(new String (bytes));
		map = new Data (bytes);
	}

	public Record fromJson(String JSONString) throws JSONException {
		readFromJSON(JSONString);
		return this;
	}
	
	public long getHash() {
		return map.getHash();
	}
	
	
	public byte[] toBytes(final byte[] omitKeybuf) {
		return map.toBytes(omitKeybuf);
	}
	
	public String toString() {
		return map.toString();		
	}
	
	
	/*
	 * 
	 * Helper for toBytes for writing strings in quoted form in compliance with JSON format
	 * Escapes special chars
	 * 
	 */
	// Adopted from JSON.org 

	protected void appendQuotedString(final String string) {

        char c = 0;
        int i;
        final int len;
        String t;
        int beginAppend = -1;
        
		if (string == null) {
            sb.append ("\"\"");
            return;
        } else {
        	len = string.length();
        }

        sb.append('"');
        for (i = 0; i < len; i ++) {
            c = string.charAt(i);
            switch (c) {           
            case '\\':
            case '"':
            	if (beginAppend >= 0) {
            		sb.append(string.substring(beginAppend, i));
            	}
                sb.append('\\');
        		beginAppend = i;
                break;
            case '/':
            	if (beginAppend >= 0) {
            		sb.append(string.substring(beginAppend, i));
            	}
                sb.append('\\');
        		beginAppend = i;
                break;
            default:
            	if (c >= ' ') {
                    if (beginAppend == -1) beginAppend = i;
            	    continue;
            	} else {  // Control
                	if (beginAppend >= 0) {
                		sb.append(string.substring(beginAppend, i));
                	}
                    switch (c) {
			            case '\b':
			                sb.append("\\b");
			                break;
			            case '\t':
			                sb.append("\\t");
			                break;
			            case '\n':
			                sb.append("\\n");
			                break;
			            case '\f':
			                sb.append("\\f");
			                break;
			            case '\r':
			                sb.append("\\r");
			                break;
			            default:
                            t = "000" + Integer.toHexString(c);
                            sb.append("\\u");
							sb.append(t.substring(t.length() - 4));
                    }
                    beginAppend = -1;
                }
            }
        }
    	if (beginAppend >= 0) {
    		sb.append(string.substring(beginAppend, i));
    	}
        sb.append('"');
    }
	
	/*
	 * 
	 * Read Record from a JSON text string (typically retrieved from database)
	 * @param input JSON string for input into Record
	 * 
	 */
	// Adopted from JSON.org 
	
	protected void readFromJSON(String input) throws JSONException {
		
		JSONTokener x = new JSONTokener (input);         
        char c;
        String key;

        if (x.nextClean() != '{') {
            throw x.syntaxError("'{' expected at the beginning of JSON record");
        }
        for (;;) {
            c = x.nextClean();
            switch (c) {
            case 0:
                throw x.syntaxError("'}' expected at the end of JSON record");
            case '}':
                return;
            default:
                x.back();
                key = x.nextValue().toString();
            }

            // The key is followed by ':'.
            c = x.nextClean();
            if (c != ':') {
                throw x.syntaxError("':' expected after JSON key in record");
            }
            map.put(key, x.nextValue().toString());

            // Pairs are separated by ','.
            switch (x.nextClean()) {
            case ';':
            case ',':
                if (x.nextClean() == '}') {
                    return;
                }
                x.back();
                break;
            case '}':
                return;
            default:
                throw x.syntaxError("',' or '}' expected in JSON record");
            }
        }
    }
}

