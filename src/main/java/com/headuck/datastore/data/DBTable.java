package com.headuck.datastore.data;

import com.headuck.datastore.utils.Utils;

import java.io.IOException;
import java.util.NoSuchElementException;

abstract public class DBTable extends Storage {
	final public String context;
	final public String idxName;
	final public String idxField;
	final protected boolean readonly;
	final protected boolean create;
	final protected boolean dupSort;
	protected byte[] idxFieldBuf = null; 
	
	/**
	 * Constructor for opening DBTable. Duplicate index key not allowed
	 * @param context Custom name given to the transaction for logging
	 * @param path Path of the db
	 * @param dbName Name of the db
	 * @param primaryField Primary key for the db
	 * @param idxName Name of the index db
	 * @param idxField Index field for the db
	 * @param readonly True if open for read only
	 * @param create True for creating a new db
	 */
	public DBTable (String context, String path, String dbName, String primaryField, 
			String idxName, String idxField, boolean readonly, boolean create) {
		this (context, path, dbName, primaryField, 
				idxName, idxField, readonly, create, false);
	}
	
	/**
	 * Constructor for opening DBTable. Duplicate keys not allowed
	 * @param context Custom name given to the transaction for logging
	 * @param path Path of the db
	 * @param dbName Name of the db
	 * @param primaryField Primary key for the db
	 * @param idxName Name of the index db
	 * @param idxField Index field for the db
	 * @param readonly True if open for read only
	 * @param create True for creating a new db
	 * @param dupSort True for allowing duplicate index key
	 */
	public DBTable (String context, String path, String dbName, String primaryField, 
			String idxName, String idxField, boolean readonly, boolean create, boolean dupSort) {
		super(dbName, primaryField, readonly);
		this.context = context;		
		this.idxName = idxName;
		this.idxField = idxField;
		if (idxField != null) {
			this.idxFieldBuf = Utils.stringToBytes(idxField);
		}
		this.readonly = readonly;
		this.create = create;
		this.dupSort = dupSort;
	}
		
	abstract public DBCursor getCursor(final boolean isIdx);		

	abstract protected void cleanup();
	
	abstract protected void cleanupAbort(); 		

	@Override
	public void close() {		
		cleanup();
	}
	
	@Override
	public void close(boolean abort) {
		if (abort) {
			cleanupAbort();
		} else {
			cleanup();
		}			
	}
	
	
	abstract public class DBCursor {
		
		// Simple tuple class 
		public abstract class KeyValue <V> { 
			public final byte[] key; 
			public final V value; 			
			public KeyValue(final byte[] key, final V value) { 
			    this.key = key; 
			    this.value = value;  
			} 
			abstract public Record toRecord();
		}
		
		
        protected boolean isIdx=false;
        protected KeyValue<?> next = null;
        protected KeyValue<?> prev = null;
        protected KeyValue<?> curr = null;
               
        abstract public void close() throws IOException; 

        /**
         * Check if the cursor points to anything
         * @return true if the cursor points to nothing currently
         */
        public boolean isEmpty() {
            return curr == null;
        }
        
        public boolean hasNext() {
            return next != null;
        }

        public boolean hasPrev() {
            return prev != null;
        }

        public Record peekNext() {
        	if (next != null) {
        		return next.toRecord();
        	} else {
                throw new NoSuchElementException("peekNext called without next element");
        	}
        }

        public Record peekPrev() {
        	if (prev != null) {
        		return prev.toRecord();
        	} else {
                throw new NoSuchElementException("peekPrev called without previous element");
        	}     
        }

        
        abstract public DBCursor seekToFirst();

        abstract public DBCursor seekToLast();

        public DBCursor seek(final String key) {
        	return seek (Utils.stringToBytes(key));
        }
        
        abstract public DBCursor seek(final byte[] keybuf); 

        abstract public DBCursor seekNext();
        
        abstract public DBCursor seekPrev();
        
        abstract public Record prev();
        
        abstract public Record next();

		abstract public void deleteNext();

		abstract public void deletePrev();

		abstract public void putNext(Record record);

		abstract public void putPrev(Record record);

        public byte[] getKey() {
        	if (curr != null) {
        		return curr.key;
        	} else {
                throw new NoSuchElementException("getKey called without current element");
        	}            	
        }
        
        public byte[] peekNextKey() {
        	if (next != null) {
        		return next.key;
        	} else {
                throw new NoSuchElementException("peekNextKey called without next element");
        	}
        }

        public byte[] peekPrevKey() {
        	if (prev != null) {
        		return prev.key;
        	} else {
                throw new NoSuchElementException("peekPrevKey called without previous element");
        	}     
        }
        
	}
}
