package com.headuck.datastore.data;

import com.headuck.datastore.utils.Utils;

public abstract class Storage {

	public String primaryField;
	protected byte[] primaryFieldBuf;
	public boolean readonly = false;
	public String dbName;
	
	public Storage(String dbName, String primaryField, boolean readonly) {
		this.dbName = dbName;
		this.primaryField = primaryField;
		if (primaryField != null) {
			this.primaryFieldBuf = Utils.stringToBytes(primaryField);
		}
		this.readonly = readonly;
	}

	public Record get(final String key, final boolean isIdx) {
		return get (Utils.stringToBytes(key), isIdx);
	}
	
	public abstract Record get(final byte[] key, final boolean isIdx);

	public void put(final String key, final Record object) {
		final byte[] keybuf = Utils.stringToBytes(key);
		put(keybuf, object);
	}
	
	public void put(final byte[] key, final Record object) {
		if (readonly) throw new DBException("Attempting to write on a readonly storage:" + dbName);
	}

	public boolean delete(final String key, final boolean isIdx) {
		final byte[] keybuf = Utils.stringToBytes(key);
		return delete(keybuf, isIdx);
	}
	
	public boolean delete(final byte[] keybuf, final boolean isIdx) {
		if (readonly) throw new DBException("Attempting to delete from a readonly storage:" + dbName);
		return false;
	}
	
	public abstract void close();		
	
	public abstract void close(boolean abort);

	public abstract long count();
	
}