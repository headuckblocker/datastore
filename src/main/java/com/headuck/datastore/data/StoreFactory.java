package com.headuck.datastore.data;

public abstract class StoreFactory {

	protected String dbName;				
	protected String primaryField = "id";

	public StoreFactory(String dbName) {
		this.dbName = dbName;
	}

	public StoreFactory setPrimaryField(String primaryField) {
		this.primaryField = primaryField;
		return this;
	}

	public String getPrimaryField() {
		return primaryField;
	}
	
	abstract public Storage openStorage(String context, boolean readonly, boolean create);
}