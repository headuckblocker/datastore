package com.headuck.datastore.data;

import java.util.TreeMap;

public class HashFactory extends StoreFactory {
	
	public TreeMap <String, Record> storageMap = new TreeMap<String, Record>();
	
	public HashFactory(String dbName) {
		super(dbName);
	}

	public HashFactory setPrimaryField(String primaryField) {
		this.primaryField = primaryField;
		return this;
	}
	
	public HashStorage openStorage(String context, boolean readonly, boolean create) {
		if (create) {
			storageMap.clear();
		}
		return new HashStorage(dbName, primaryField, storageMap);
	}
}
