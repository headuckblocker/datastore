package com.headuck.datastore;

import com.headuck.datastore.data.DBException;
import com.headuck.datastore.data.Record;
import com.headuck.datastore.data.Storage;
import com.headuck.datastore.data.StoreFactory;
import com.headuck.datastore.instream.CountingInputStream;
import com.headuck.datastore.instream.ICountProgressHandler;
import com.headuck.datastore.instream.SmartEncodingInputStream;
import com.headuck.datastore.instream.ZipEntryInputStream;
import com.headuck.datastore.net.IHttpHandler;
import com.headuck.datastore.net.JavaUrlDownloader;
import com.headuck.datastore.xml.IJunkRecordHandler;
import com.headuck.datastore.xml.XMLRule;
import com.headuck.datastore.xml.XMLRuleParser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import java.io.BufferedInputStream;
import java.io.Closeable;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;


public class DBImporter {

	final static Logger log = LoggerFactory.getLogger("DBImporter");
	// Values of errorMsg 
	final static String ERROR_MESSAGE_EXCEPTION = "Exception occurred";
	final static String ERROR_MESSAGE_CLOSING_DB = "Database error, failed to close database";
	final static String ERROR_MESSAGE_NETWORK = "Download failure";
	final static String ERROR_MESSAGE_XML = "XML parsing failure";
	final static String ERROR_MESSAGE_DBERROR = "Database error";
	
	// MIME Types to recognize zip file
	final String [] zipMime = { "application/zip", "application/x-zip-compressed",
								"application/zip-compressed", "multipart/x-zip"}; 
	final List <String> zipMineList = Arrays.asList(zipMime);
	
	public interface IDownloadProgressHandler {
		void onProgress (long total, long progress) throws InterruptedException;
	}

	private class RuleDBPair {
		public StoreFactory db;
		public XMLRule xmlRule;
		
		public RuleDBPair (StoreFactory db, XMLRule xmlRule) {
			this.db = db;
			this.xmlRule = xmlRule;
		}
	}

	ArrayList <RuleDBPair> ruleDBPairs = new ArrayList <RuleDBPair> ();
	
	// error state for this importer
	public boolean error = false;
	public String errorMsg;
	
	long startTime;
	IDownloadProgressHandler handler = null;

	// Pin for https
	String pubKeyPin = null;
	String[] certPin = null;
	
	public DBImporter () {
	}
		
	public DBImporter (IDownloadProgressHandler handler) {
		this.handler = handler;
	}
	
	public DBImporter addRule (XMLRule xmlRule, StoreFactory db) {
		ruleDBPairs.add(new RuleDBPair(db, xmlRule));
		return this;
	}

	public DBImporter setPin (String pubKeyPin, String[] certPin) {
		this.pubKeyPin = pubKeyPin;
		this.certPin = certPin;
		return this;
	}

	/**
	 * Import DB from local xml or zip file
	 * @param filename - filename to import.  Files ending with .zip will be regarded as zip file
	 * @throws IOException
	 * @throws SAXException
	 */
	public void importFromFile(final String filename) throws IOException, SAXException 
	{		
		ZipEntryInputStream zips = null;
		FileInputStream fis = null;
		CountingInputStream cs = null; 

    	log.info("Import from file {}", filename);

		try {			
			fis = new FileInputStream(filename);
			cs = new CountingInputStream(fis, new CountHandler (handler, fis.available()));
			if (filename.toLowerCase().endsWith(".zip")) {
				zips = new ZipEntryInputStream(cs, fis.available()/2);
			}						
			importFromStream ((zips == null)? cs:zips);
		}
		finally
		{
			silentClose (zips, fis, cs);
		}
	}
	
	/**
	 * Import DB from HTTP url with access token
	 * @param httpUrl - url of the http document to import.
	 *        Supports gzip compression and "application/zip" compression (handled at different levels)
	 * @param token - access token for the header, or null if none
	 * @throws IOException
	 * @throws SAXException
	 */
	public void importFromHTTPWithToken(final String httpUrl, final String token) throws IOException, SAXException {
	
		JavaUrlDownloader downloader = null;
		ZipEntryInputStream zips = null;
		InputStream nets = null;
	
    	log.info("Import from http {}", httpUrl);

    	
		try {
			downloader = new JavaUrlDownloader(httpUrl, token, new LocalHttpHandler(handler));
			downloader.setSSLContext(pubKeyPin, certPin);
			if (downloader.openConnection()) {
				nets = downloader.getInputStream();
				if (zipMineList.contains(downloader.contentType) || ZipEntryInputStream.isZipStream(nets)) {
					zips = new ZipEntryInputStream(nets, downloader.contentLength/2);
				}
				importFromStream ((zips == null)? nets:zips);
			}
		}
		finally
		{
			if (downloader != null) {
				downloader.closeConnection();
			}
			silentClose (zips, nets);
		}
	}

	/**
	 * Import DB from HTTP url
	 * @param httpUrl - url of the http document to import.
	 *        Supports gzip compression and "application/zip" compression (handled at different levels)
	 * @throws IOException
	 * @throws SAXException
	 */
	public void importFromHTTP(final String httpUrl) throws IOException, SAXException {
		importFromHTTPWithToken(httpUrl, null);
	}
	
	public void importFromZipStream (final InputStream is) throws IOException, SAXException 
	{		
		CountingInputStream cs = null;
	    ZipEntryInputStream zips = null;
	    		
	    log.info("Import from zip stream");
	    
	    try {
			cs = new CountingInputStream(is, new CountHandler(handler ,is.available()));
			zips = new ZipEntryInputStream(cs, is.available()/2);
			importFromStream (zips);
	    }
		finally
		{
			silentClose (zips, cs);
		}
	}

	
	/**
	 * Import DB from a Java stream.  Also deal with Byte Order Mark and recognize the 
	 * 	different utf encoding of the stream
	 * @param ins - import stream
	 * @throws IOException
	 * @throws SAXException
	 */
	protected void importFromStream(final InputStream ins) throws IOException, SAXException 
	{ 
		final int BUFFER_INPUT_SIZE = 65536;
		final int BUFFER_TEST_SIZE = 1024;
		
		BufferedInputStream is = null;
		SmartEncodingInputStream sis = null;
		// InputStreamReader reader = null;
		ArrayList<XMLRule> xmlRules = new ArrayList<XMLRule>();
		TreeMap<Integer, Storage> dbMap = new TreeMap<Integer, Storage>();
		
		boolean completed = false;
		boolean dbException = false;
		
		try {
			is = new BufferedInputStream(ins, BUFFER_INPUT_SIZE);
			assert (is.markSupported());
			sis = new SmartEncodingInputStream (is, BUFFER_TEST_SIZE, Charset.forName("UTF-8"), true, true);
			// reader = (InputStreamReader) sis.getReader();

			for (RuleDBPair entry : ruleDBPairs) {			
				XMLRule xmlRule = entry.xmlRule;
				assert (xmlRule != null);
				xmlRules.add(xmlRule);
				Storage targetStore = entry.db.openStorage("DBImporter", false, true);
				assert (targetStore != null);
				dbMap.put(xmlRule.id, targetStore);
			}
			XMLRuleParser xmlParser = new XMLRuleParser (xmlRules, sis, new localRecordHandler(dbMap));
			// execute with timing info
			startTime = System.currentTimeMillis();
			xmlParser.read();
			if (!error) completed = true;
		} catch (DBException e) {
			dbException = true;
			log.error("DBException in DBImporter", e);
			throw e;			
		} finally {
		    if (!completed) {
		    	if (!error) {
		    		error = true;
		    		if (dbException) {
		    			errorMsg = ERROR_MESSAGE_DBERROR;
					} else {
						errorMsg = ERROR_MESSAGE_EXCEPTION;
						log.error("Exception in DBImporter");
					}
		    	}
		    }
		    silentClose (is, sis/*, reader*/);
			// iterate through Storage in TreeMap and close them
		    Iterator<Storage> itr = dbMap.values().iterator();		   	   
		    while(itr.hasNext()) {
		    	try {
		    		itr.next().close(!completed);   // abort if not success
		    	} catch (DBException e) {
		    		log.error("Error in closing database", e);
		    		if (!error) {
			    		error = true;
			    		errorMsg = ERROR_MESSAGE_CLOSING_DB;
		    		}
		    	}
		  	}    
		    if (!error) {
		    	log.info("Import succeeded");
		    }
		}
	}
	
	/**
	 * Handler for Counting Input Stream
	 * Progress is forwarded to IDownloadProgressHandler handler if not null  
	 *
	 */
	public class CountHandler implements ICountProgressHandler {

		long total;
		IDownloadProgressHandler handler;
		
		public CountHandler (IDownloadProgressHandler handler, long total) {
			this.handler = handler;
			this.total = total;
		}
		
		//@Override
		public void onProgress(long byteRead) throws IOException, InterruptedException {
			if (handler != null)
				handler.onProgress(total, byteRead);
		}
		
	}
	
	/**
	 * Handler for http downloader
	 * Progress is forwarded to IDownloadProgressHandler handler if not null  
	 *
	 */
	public class LocalHttpHandler implements IHttpHandler {
		
		IDownloadProgressHandler handler;
		
		public LocalHttpHandler (IDownloadProgressHandler handler) {
			this.handler = handler;			
		}
		
		public void onSuccess() {
			log.info ("HTTP connection reported success");
		}
	
		public void onFailure(String message) {
			log.info ("HTTP Failure: {}", message);			
			error = true;
			errorMsg = ERROR_MESSAGE_NETWORK + " - " + message;
		}
	
		public void onProgress(long progress, long total) throws InterruptedException {
			if (handler != null) {
				handler.onProgress(total, progress);
			}
			log.debug ("Progress {} of {}", progress, total);	
			//System.out.format ("Elapsed time %d ms%n",System.currentTimeMillis()-startTime);
		}
	}

	/**
	 * Handler for parsing records
	 * Progress is forwarded to IDownloadProgressHandler handler if not null  
	 *
	 */
	public class localRecordHandler implements IJunkRecordHandler {
		
		// int lineCount = 0;
		TreeMap<Integer, Storage> dbMap = null;
		
		public localRecordHandler (TreeMap<Integer, Storage> dbMap) {
			this.dbMap = dbMap;
		}		

		public void onNewRecord(int ruleId, Record record) {
			Storage db = dbMap.get(ruleId);
					
			// Get the primary field from the record 
			byte[] key = record.map.get(db.primaryField);
			
			if (key != null) {			
				db.put(key, record);									
			} else {
				log.warn("Record has no primary key: {}", record.toString());
			}
			// System.out.println("->"+record.toString());
		}
		
		public void onFailure(String message) {
			log.error("XML parsing failure: {}" + message);
			dbMap = null;
			error = true;
			errorMsg = ERROR_MESSAGE_XML;		
		}
		
		public void onSuccess() {
			log.info("XML parsing completed");
			log.info("Elapsed time: {} ms",System.currentTimeMillis()-startTime);
			dbMap = null;			
		}
		
	}
	
	private static void silentClose (Closeable ... closables) {
		for (Closeable c : closables) {
			if (c != null) {
				try {
					c.close();
				} catch (IOException e) {
					log.warn ("IO Exception during closing", e);
					// Suppress
				}				
			}
		}
	}
	
}

