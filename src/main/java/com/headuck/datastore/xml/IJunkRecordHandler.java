package com.headuck.datastore.xml;

import com.headuck.datastore.data.Record;

public interface IJunkRecordHandler {
		void onNewRecord (int ruleId, Record record);
		void onFailure (String message); 
		void onSuccess ();
}
