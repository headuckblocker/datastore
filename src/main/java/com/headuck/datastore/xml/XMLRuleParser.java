package com.headuck.datastore.xml;

import com.headuck.datastore.data.Record;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

public class XMLRuleParser {

	// private InputStreamReader reader=null;
	private InputStream is=null;
	private final IJunkRecordHandler outputHandler;
	private final ArrayList<XMLRule> ruleList;
	private final int ruleListSize;
	private HashMap<String, String> tagSet = new HashMap<String, String>();
	private HashMap<String, String> attribSet = new HashMap<String, String>();  
	
	public XMLRuleParser(ArrayList<XMLRule> ruleList, InputStream is, IJunkRecordHandler handler) throws UnsupportedEncodingException
	{
/*		this (ruleList, new InputStreamReader(is, "UTF-8"), handler);
	}
	
	public XMLRuleParser(ArrayList<XMLRule> ruleList, InputStreamReader reader, IJunkRecordHandler handler) 
	{
	    this.reader = reader;
	*/
		this.is = is;
		this.outputHandler = handler;
		this.ruleList = ruleList;
		ruleListSize = ruleList.size();
		// Add tags involved to HashMap for efficiency in further operations
		for (XMLRule rule: ruleList) {
			for (String tag: rule.parentList) {
				tagSet.put(tag/*.toLowerCase()*/, tag);
			}
			for (String tag: rule.fieldList) {
				tagSet.put(tag/*.toLowerCase()*/, tag);
				int dot = tag.indexOf(".");
				if (dot >= 0) {
					String substr = tag.substring(0, dot);
					tagSet.put(substr/*.toLowerCase()*/, substr);
				}
			}
			for (String attrib: rule.attribList) {
				attribSet.put(attrib/*.toLowerCase()*/, attrib);
			}
		}		
	}
	
	public void read() throws IOException, SAXException {
		
		try {
		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser saxParser = factory.newSAXParser();
		// XMLReader xmlReader = saxParser.getXMLReader();
		DefaultHandler handler = new DefaultHandler() {
			// Element contexts above leaf elements
			ArrayList<String> parsedParentList = new ArrayList<String>(XMLRule.MAXDEPTH);
			int parsedParentDepth = 0;
			ArrayList<String> parsedFieldList = new ArrayList<String>(XMLRule.MAXDEPTH);
			int nullCount = 0;
			Record recordHash;
			XMLRule currRule = null;
			int curri = -1;
			String currTag = null;
			String currValue = null;
			int [] matchedTag = new int[ruleListSize]; 
		 
			public void startDocument() throws SAXException {
				for (int i = 0; i < ruleListSize; i++) {
					matchedTag[i] = 0;
				}					
			}
			
			public void startElement(final String uri, final String localName, final String qName, 
		                final Attributes attributes) throws SAXException {
		 
				// System.out.println("Start Element :" + qName);				
				
				final String tag = tagSet.get(qName/*.toLowerCase()*/);  // null if not in any of the rule set
				String attrib;
				String field;
				String str;
				int i;
				int l;
				XMLRule rule;
				
				if (currRule == null) {  
					// Not in any record				
					parsedParentList.add(tag);
					parsedParentDepth ++;
					if (tag == null) {
						nullCount ++;
					}
					//Compare with rules					
					if (nullCount == 0) // Shortcut, any null implies won't match 
					{
						for (i = 0; i < ruleListSize; i++) {
							if (matchedTag[i] != parsedParentDepth - 1) continue;
							rule = ruleList.get(i);
							if ((rule.parentList.size()>=parsedParentDepth) && 
								(parsedParentList.get(parsedParentDepth-1).equals(rule.parentList.get(parsedParentDepth-1)))) {
								// rule partially matched
								matchedTag[i]++;
								// in case of full match
								if (matchedTag[i] == rule.parentList.size()) { 
									// Beginning of a new record type id							
									currRule = rule;
									curri = i;
									recordHash = new Record();								
									// Add all matching attributes of current tag
									for (i = 0, l=attributes.getLength(); i < l; i++) {
										attrib = attribSet.get(attributes.getQName(i)/*.toLowerCase()*/);
										if (attrib != null) {
											recordHash.map.put(attrib,attributes.getValue(i));
										}
									}
									// Only process first fully matched rule
									break;
								}
							}
						}
					}
				} else {
					// the current tag
					parsedFieldList.add(tag);
					// Only process as field for immediate level under parent 
					if ((tag != null) && (currTag == null) && (parsedFieldList.size()==1)) {						
						for (i = 0, l=attributes.getLength(); i < l; i++) {
							attrib = attribSet.get(attributes.getQName(i)/*.toLowerCase()*/);							
							if ((attrib != null) && (currRule.attribList.contains(attrib))) {
								str = new StringBuilder(tag).append(".").append(attributes.getValue(i)).toString();
								// Check if in field list of current rule
								// Accepts "name.zh_hk" as field name matching
								// <name lang="zh_hk"> where "lang" is in attribSet
								for (i = 0, l = currRule.fieldList.size(); i < l; i++) {
									field = currRule.fieldList.get(i);
									if (field.equals(str)) {
										currTag = field;
										currValue = null;
										break;
									}
								}
							}
							if (currTag != null) break;
						}
						if (currTag == null) {
							// process simple field w/o matching attributes
							for (i = 0, l = currRule.fieldList.size(); i < l; i++) {
								field = currRule.fieldList.get(i);
								if (field.equals(tag)) {
									currTag = field;
									currValue = null;
									break;
								}
							}
						}
					}
				}

			}
		 
			public void endElement(final String uri, final String localName,
				final String qName) throws SAXException {
		 							
				// System.out.println("End Element :" + qName);
				final String tag = tagSet.get(qName/*.toLowerCase()*/);  // null if not in any of the rule set
				String topTag;

				if (currRule != null) {
					final int fieldListSize = parsedFieldList.size();
					// ends current record if nothing to pop from parsedFieldList
					if (fieldListSize == 0) {
						outputHandler.onNewRecord(currRule.id, recordHash);
						currRule = null;
						recordHash = null;
						matchedTag[curri]--;
						curri = -1;
						// pop from parent list						
						topTag = parsedParentList.remove(parsedParentDepth-1);
						parsedParentDepth --;
						if (tag != null) {
							if (!tag.equals(topTag)) {
								throw (new SAXException(String.format("Unmatched tag: <%s> closed with <%s>", topTag, tag)));
							}
						}
					} else {
						// pop field from parsedFieldList
						topTag = parsedFieldList.remove(fieldListSize-1);
						if (tag != null) {
							if (!tag.equals(topTag)) {
								throw (new SAXException(String.format("Unmatched tag: <%s> closed with <%s>", topTag, tag)));
							}
						}
						// write field to record 
						if ((currTag != null) && (fieldListSize == 1)) {
							if (currValue != null) {
								recordHash.map.put(currTag,currValue);
							}
							currTag = null;
							currValue = null;
						}
					}
				} else {
					// not in an record
					if (parsedParentDepth > 0) {
						topTag = parsedParentList.remove(parsedParentDepth-1);
						parsedParentDepth --;
						for (int i = 0; i < ruleListSize; i++) {
							if (matchedTag[i] > parsedParentDepth) matchedTag[i]--;
						}
						if (tag != null) {
							if (!tag.equals(topTag)) {
								throw (new SAXException(String.format("Unmatched tag: <%s> closed with <%s>", topTag, tag)));
							}
						} else {
							nullCount --;
						}							
					} else {
						throw (new SAXException(String.format("Unexpected closing tag: <%s>", tag)));
					}
				}
								
			}
		 
			public void characters(char ch[], int start, int length) throws SAXException {
				
				if (currTag != null) {
					if (currValue == null) {
						currValue = new String(ch, start, length); 
					} else {
						// not efficient but should be rare normally
						currValue = currValue + new String(ch, start, length);
					}
				}		 
			}
			
			public void endDocument() {
				outputHandler.onSuccess();
			}
		};

	    //xmlReader.setContentHandler(handler);
	    //xmlReader.parse(new InputSource(reader));
		saxParser.parse(is, handler);
		handler = null;
		// xmlReader.setContentHandler(null);
		// xmlReader = null;
		saxParser = null;
		System.gc();
     } catch (ParserConfigurationException e) {
    	 throw new SAXException ("Parser configuration error", e);
	     //  e.printStackTrace();
	     //  outputHandler.onFailure(e.getClass().getSimpleName() + ":" + e.getMessage());
     } finally {
 	    // reader=null;
		is = null;
     }
		
   }
 
}
