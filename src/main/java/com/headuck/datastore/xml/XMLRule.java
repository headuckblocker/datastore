package com.headuck.datastore.xml;

import java.util.ArrayList;
import java.util.Arrays;

/*
 * Defines a rule for converting XML input stream to Record
 * 
 */

public class XMLRule {
	static int MAXDEPTH = 10;  // max depth of XML rule
	public int id;
	public ArrayList<String> parentList;  // Array of parent tags to trigger record
	public ArrayList<String> fieldList;   // Array of fields
	public ArrayList<String> attribList;  // Array of recognised attributes

	public XMLRule (int id,  String[] parentList,  
					String[] fieldList, String[] attribList) {
		this.id = id;
		this.parentList = new ArrayList<String>(Arrays.asList(parentList));
		this.fieldList = new ArrayList<String>(Arrays.asList(fieldList));
		this.attribList = new ArrayList<String>(Arrays.asList(attribList));
	};
}